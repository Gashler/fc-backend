<!doctype html>
<html>
    <head>
        @if (isset($object))
            <title>{{ $object->title }}</title>
            <meta property="og:title" content="{{ $object->title }}">
            <meta property="og:url" content="{{ config('site.app_url')}}/c/{{ $object->id }}">
            @if (isset($object->image))
                <meta property="og:image" content="{{ $object->image->url }}">
            @endif
            <meta name="description" content="{{ $object->short_description }}">
            @if (isset($object->user))
                <meta name="author" content="{{ $object->user->name }}">
            @elseif (isset($object->campaign->user))
                <meta name="author" content="{{ $object->campaign->user->name }}">
            @endif
        @else
            <title>{{ config('site.app_name') }}</title>
        @endif
        <base href="/">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta property="og:type" content="website">
        <link href="{{ config('site.frontend_url') }}/styles.1b5bc4905e5b9c5a48c0.bundle.css" rel="stylesheet">
        <meta name="keyword" content="crowdfunding, fundraising, platform, money">
        <link rel="shortcut icon" href="{{ config('site.frontend_url') }}/assets/img/favicon.png">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5b1ad07143e7c9001185412c&product=social-ab' async='async'></script>
        <link rel="stylesheet" href="/css/default.css">
        <script charset="utf-8" src="{{ config('site.frontend_url') }}/views-dashboard-dashboard-module.js"></script>
    </head>
    <body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden" ng-version="5.2.11"><ng-progress _nghost-c0="">
        <ng-progress-bar _ngcontent-c0="" _nghost-c2="" ng-reflect-state="[object Object]" ng-reflect-position-using="margin" ng-reflect-ease="linear" ng-reflect-speed="200" ng-reflect-show-spinner="true" ng-reflect-direction="leftToRightIncreased" ng-reflect-thick="false" ng-reflect-color="#CC181E">
            <!--bindings={
            "ng-reflect-ng-if": "[object Object]"
        }--><div _ngcontent-c2="" class="ng-progress">
        <div _ngcontent-c2="" class="bar" ng-reflect-ng-style="[object Object]" style="transition: all 200ms linear; background-color: rgb(204, 24, 30); margin-left: -100%;">
            <div _ngcontent-c2="" class="bar-shadow" ng-reflect-ng-style="[object Object]" style="box-shadow: rgb(204, 24, 30) 0px 0px 10px, rgb(204, 24, 30) 0px 0px 5px;"></div>
        </div>
        <!--bindings={
        "ng-reflect-ng-if": "true"
    }--><div _ngcontent-c2="" class="spinner clockwise" ng-reflect-klass="spinner" ng-reflect-ng-class="clockwise">
    <div _ngcontent-c2="" class="spinner-icon" style="border-top-color: rgb(204, 24, 30); border-left-color: rgb(204, 24, 30);"></div>
</div>
</div></ng-progress-bar></ng-progress>
<simple-notifications ng-reflect-options="[object Object]"><div class="simple-notification-wrapper top right" ng-reflect-klass="simple-notification-wrapper" ng-reflect-ng-class="top,right"> <!--bindings={
    "ng-reflect-ng-for-of": ""
}--> </div></simple-notifications>
<router-outlet></router-outlet><si-dashboard><header class="app-header navbar">
    <button appmobilesidebartoggler="" class="navbar-toggler d-lg-none" type="button">☰</button>
    <a class="navbar-brand" href="{{ config('site.frontend_url') }}"></a>

    <!--bindings={
    "ng-reflect-ng-if": "[object Object]"
}-->
@if (isset($object))
    <span>
        <span>
            <a class="navbar-toggler" href="{{ config('site.frontend_url') }}/#/campaigns/{{ $object->id }}/dashboard/0"><i class="icon-speedometer"></i></a>
        </span>
        <span>
            <a class="navbar-toggler" href="{{ config('site.frontend_url') }}/#/campaigns/{{ $object->id }}/edit"><i class="icon-pencil"></i></a>
        </span>
        <span>
            <a class="navbar-toggler" href="{{ config('site.frontend_url') }}/#/campaigns"><i class="icon-user"></i></a>
        </span>
    </span>
@endif
<!--bindings={
"ng-reflect-ng-if": "false"
}-->
</header>


<div class="app-body">
    <div class="sidebar">




        <nav class="sidebar-nav">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link" routerlinkactive="active" ng-reflect-router-link="/" ng-reflect-router-link-active-options="[object Object]" ng-reflect-router-link-active="active" href="{{ config('site.frontend_url') }}/#/">
                        <i class="icon-home"></i> Home
                    </a>
                </li>
                <!--bindings={
                "ng-reflect-ng-if": "[object Object]"
            }-->
            @if (auth()->check())
                <li class="nav-item">
                    <a class="nav-link active" href="{{ config('site.frontend_url') }}/#/campaigns">
                        <i class="icon-map"></i>
                        <!--bindings={
                        "ng-reflect-ng-if": "false"
                    }-->
                    <!--bindings={
                    "ng-reflect-ng-if": "true"
                }-->    <span>My Campaigns</span>
            </a>
        </li>
    @endif
    <!--bindings={
    "ng-reflect-ng-if": "false"
}-->
<li class="nav-item">
    <a class="nav-link" ng-reflect-router-link="/campaigns/new" href="{{ config('site.frontend_url') }}/#/campaigns/new">
        <i class="fa icon-plus"></i> Create Campaign
    </a>
</li>
</ul>
</nav>
<div class="sidebar-footer">

</div>

</div>


<main class="main">
    <div class="container-fluid">
        @section('content')
        @show
    </div>
</main>

<!--bindings={
"ng-reflect-ng-if": "[object Object]"
}--><aside class="aside-menu">
<nav class="nav-tabs">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" routerlink="/user/edit" ng-reflect-router-link="/user/edit" href="{{ config('site.frontend_url') }}/#/user/edit"><i class="icon-user"></i> My Account</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" routerlink="/user/financial" ng-reflect-router-link="/user/financial" href="{{ config('site.frontend_url') }}/#/user/financial"><i class="icon-wallet"></i> My Financial Information</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" ng-reflect-router-link="/login" href="{{ config('site.frontend_url') }}/#/login"><i class="icon-logout"></i> Logout</a>
        </li>
    </ul>
</nav>
</aside>

</div>

<footer class="app-footer">
    <a routerlink="/" ng-reflect-router-link="/" href="{{ config('site.frontend_url') }}/#/">Home</a> &nbsp;|&nbsp;
    <a routerlink="/about" ng-reflect-router-link="/about" href="{{ config('site.frontend_url') }}/#/about">About</a> &nbsp;|&nbsp;
    <a routerlink="/contact" ng-reflect-router-link="/contact" href="{{ config('site.frontend_url') }}/#/contact">Contact</a>
    <small class="pull-right">© FunderCat 2018</small>
</footer>

</si-dashboard>


<span style="border-radius: 3px; text-indent: 20px; width: auto; padding: 0px 4px 0px 0px; text-align: center; font-style: normal; font-variant: normal; font-weight: bold; font-stretch: normal; font-size: 11px; line-height: 20px; font-family: &quot;Helvetica Neue&quot;, Helvetica, sans-serif; color: rgb(255, 255, 255); background: url(&quot;data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGhlaWdodD0iMzBweCIgd2lkdGg9IjMwcHgiIHZpZXdCb3g9Ii0xIC0xIDMxIDMxIj48Zz48cGF0aCBkPSJNMjkuNDQ5LDE0LjY2MiBDMjkuNDQ5LDIyLjcyMiAyMi44NjgsMjkuMjU2IDE0Ljc1LDI5LjI1NiBDNi42MzIsMjkuMjU2IDAuMDUxLDIyLjcyMiAwLjA1MSwxNC42NjIgQzAuMDUxLDYuNjAxIDYuNjMyLDAuMDY3IDE0Ljc1LDAuMDY3IEMyMi44NjgsMC4wNjcgMjkuNDQ5LDYuNjAxIDI5LjQ0OSwxNC42NjIiIGZpbGw9IiNmZmYiIHN0cm9rZT0iI2ZmZiIgc3Ryb2tlLXdpZHRoPSIxIj48L3BhdGg+PHBhdGggZD0iTTE0LjczMywxLjY4NiBDNy41MTYsMS42ODYgMS42NjUsNy40OTUgMS42NjUsMTQuNjYyIEMxLjY2NSwyMC4xNTkgNS4xMDksMjQuODU0IDkuOTcsMjYuNzQ0IEM5Ljg1NiwyNS43MTggOS43NTMsMjQuMTQzIDEwLjAxNiwyMy4wMjIgQzEwLjI1MywyMi4wMSAxMS41NDgsMTYuNTcyIDExLjU0OCwxNi41NzIgQzExLjU0OCwxNi41NzIgMTEuMTU3LDE1Ljc5NSAxMS4xNTcsMTQuNjQ2IEMxMS4xNTcsMTIuODQyIDEyLjIxMSwxMS40OTUgMTMuNTIyLDExLjQ5NSBDMTQuNjM3LDExLjQ5NSAxNS4xNzUsMTIuMzI2IDE1LjE3NSwxMy4zMjMgQzE1LjE3NSwxNC40MzYgMTQuNDYyLDE2LjEgMTQuMDkzLDE3LjY0MyBDMTMuNzg1LDE4LjkzNSAxNC43NDUsMTkuOTg4IDE2LjAyOCwxOS45ODggQzE4LjM1MSwxOS45ODggMjAuMTM2LDE3LjU1NiAyMC4xMzYsMTQuMDQ2IEMyMC4xMzYsMTAuOTM5IDE3Ljg4OCw4Ljc2NyAxNC42NzgsOC43NjcgQzEwLjk1OSw4Ljc2NyA4Ljc3NywxMS41MzYgOC43NzcsMTQuMzk4IEM4Ljc3NywxNS41MTMgOS4yMSwxNi43MDkgOS43NDksMTcuMzU5IEM5Ljg1NiwxNy40ODggOS44NzIsMTcuNiA5Ljg0LDE3LjczMSBDOS43NDEsMTguMTQxIDkuNTIsMTkuMDIzIDkuNDc3LDE5LjIwMyBDOS40MiwxOS40NCA5LjI4OCwxOS40OTEgOS4wNCwxOS4zNzYgQzcuNDA4LDE4LjYyMiA2LjM4NywxNi4yNTIgNi4zODcsMTQuMzQ5IEM2LjM4NywxMC4yNTYgOS4zODMsNi40OTcgMTUuMDIyLDYuNDk3IEMxOS41NTUsNi40OTcgMjMuMDc4LDkuNzA1IDIzLjA3OCwxMy45OTEgQzIzLjA3OCwxOC40NjMgMjAuMjM5LDIyLjA2MiAxNi4yOTcsMjIuMDYyIEMxNC45NzMsMjIuMDYyIDEzLjcyOCwyMS4zNzkgMTMuMzAyLDIwLjU3MiBDMTMuMzAyLDIwLjU3MiAxMi42NDcsMjMuMDUgMTIuNDg4LDIzLjY1NyBDMTIuMTkzLDI0Ljc4NCAxMS4zOTYsMjYuMTk2IDEwLjg2MywyNy4wNTggQzEyLjA4NiwyNy40MzQgMTMuMzg2LDI3LjYzNyAxNC43MzMsMjcuNjM3IEMyMS45NSwyNy42MzcgMjcuODAxLDIxLjgyOCAyNy44MDEsMTQuNjYyIEMyNy44MDEsNy40OTUgMjEuOTUsMS42ODYgMTQuNzMzLDEuNjg2IiBmaWxsPSIjYmQwODFjIj48L3BhdGg+PC9nPjwvc3ZnPg==&quot;) 3px 50% / 14px 14px no-repeat rgb(189, 8, 28); position: absolute; opacity: 1; z-index: 8675309; display: none; cursor: pointer; border: none; -webkit-font-smoothing: antialiased; top: 210px; left: 40px;">Save</span><span style="width: 24px; height: 24px; background: url(&quot;data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pjxzdmcgd2lkdGg9IjI0cHgiIGhlaWdodD0iMjRweCIgdmlld0JveD0iMCAwIDI0IDI0IiB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxkZWZzPjxtYXNrIGlkPSJtIj48cmVjdCBmaWxsPSIjZmZmIiB4PSIwIiB5PSIwIiB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHJ4PSI2IiByeT0iNiIvPjxyZWN0IGZpbGw9IiMwMDAiIHg9IjUiIHk9IjUiIHdpZHRoPSIxNCIgaGVpZ2h0PSIxNCIgcng9IjEiIHJ5PSIxIi8+PHJlY3QgZmlsbD0iIzAwMCIgeD0iMTAiIHk9IjAiIHdpZHRoPSI0IiBoZWlnaHQ9IjI0Ii8+PHJlY3QgZmlsbD0iIzAwMCIgeD0iMCIgeT0iMTAiIHdpZHRoPSIyNCIgaGVpZ2h0PSI0Ii8+PC9tYXNrPjwvZGVmcz48cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIGZpbGw9IiNmZmYiIG1hc2s9InVybCgjbSkiLz48L3N2Zz4=&quot;) 50% 50% / 14px 14px no-repeat rgba(0, 0, 0, 0.4); position: absolute; opacity: 1; z-index: 8675309; display: none; cursor: pointer; border: none; border-radius: 12px; top: 208px; left: 470px;"></span><div id="ClearPlayFilterRoot"><!-- react-empty: 1 --></div><div id="ClearPlayFilterStatusBar"></div>



</body>
</html>
