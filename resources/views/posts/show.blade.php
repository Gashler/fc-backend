@extends('layouts.default')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <p>
                <a href="{{ config('site.app_url') }}/c/{{ $object->campaign->id }}">
                    <i class="fa fa-angle-left"></i> {{ $object->campaign->title }}
                </a>
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-8">
            <div class="card">
                <div class="card-body">
                    <h1>{{ $object->title }}</h1>
                    <p><em>{{ $object->formatted_created_at }}</em></p>
                    <br>
                    {!! $object->body !!}
                </div>
            </div>
        </div>
    </div>
@stop
