<!-- Sidebar -->
<div>

    <!-- Pay -->
    <p>
        <a
            href="{{ config('site.frontend_url') }}/#/payments/new/{{ $object->id }}"
            class="btn btn-primary full-width"
        >Support this Campaign <i class="fa fa-angle-right"></i></a>
    </p>
    <!-- End Pay -->

    <!-- Statistics -->
    <div class="card text-white bg-info align-center">
        <div class="card-body">
            <div class="progress">
                <div
                    class="progress-bar"
                    role="progressbar"
                    aria-valuenow="60"
                    aria-valuemin="0"
                    aria-valuemax="100"
                    style="width: {{ $object->percent_raised }}%"
                ></div>
            </div>
            <div>
                <div class="inline-block">
                    <div class="h1 mt-4 mb-0">${{ $object->monetary_total }}</div>
                    <small class="text-muted text-uppercase font-weight-bold">Raised</small>
                </div>
                @if (isset($object->monetary_goal))
                    <div class="inline-block text-muted text-uppercase font-weight-bold" style="margin-top:33px;">&nbsp;of&nbsp;</div>
                    <div class="inline-block">
                        <div class="h1 mt-4 mb-0">${{ $object->monetary_goal }}</div>
                        <small class="text-muted text-uppercase font-weight-bold">Goal</small>
                    </div>
                @endif
            </div>
            <div>
                @if (isset($object->monetary_goal))
                    <div class="inline-block">
                        <div class="h1 mt-4 mb-0">{{ $object->percent_raised }}%</div>
                        <small class="text-muted text-uppercase font-weight-bold">Funded</small>
                    </div>
                    <div class="inline-block text-muted text-uppercase font-weight-bold" style="margin-top:33px;">&nbsp;by&nbsp;</div>
                @endif
                <div class="inline-block">
                    <div class="h1 mt-4 mb-0">{{ $object->backers_count }}</div>
                    <small class="text-muted text-uppercase font-weight-bold">Backers</small>
                </div>
            </div>
        </div>
    </div>
    <!-- End Statistics -->

    <!-- Rewards -->
    @if (count($object->rewards) > 0)
        <h2>Backer Rewards</h2>
        @foreach ($object->rewards as $index => $reward)
            <div class="card">
                <div class="card-header">
                    <strong>Contribute ${{ $reward->amount }} or more</strong>
                </div>
                <div class="card-body">
                    <h4>{{ $reward->title }}</h4>
                    <p>{{ $reward->description }}</p>
                    <a
                        href="{{ config('site.frontend_url') }}/#/payments/new/{{ $object->id }}/{{ $index }}"
                        class="btn btn-primary"
                    ><i class="fa fa-check"></i> Select Reward</a>
                </div>
            </div>
        @endforeach
    @endif
    <!-- End Rewards -->

</div>
<!-- End Sidebar -->
