@extends('layouts.default')
@section('content')
    <div class="row">

        <!-- Campaign Detail -->
        <div class="col-sm-12 col-md-8">

            <!-- links -->
            @foreach ($object->links as $link)
                @if (count($object->links) > 0)
                    <div class="pull-right">
                        @if ($link->key !== 'youtube')
                            <a
                                class="btn btn-default"
                                href="{{ $link->value }}"
                                target="_blank"
                            ><i class="fa fa-{{ $link->key }}"></i></a>
                        @endif
                    </div>
                @endif
            @endforeach
            <!-- links -->

            <h1>{{ $object->title }}</h1>
            <h3>{{ $object->time_left }} Left</h3>
            <p><strong>Hosted by {{ $object->user->name }}</strong></p>
            <p>{{ $object->short_description }}</p>
            @if ($object->video)
                <div class="videoWrapper">
                    <iframe
                        width="560"
                        height="315"
                        src="{{ $object->video->embed_url }}"
                        frameborder="0"
                        allow="autoplay encrypted-media"
                        allowfullscreen
                    ></iframe>
                </div>
            @endif
            @if (!isset($object->video) && isset($object->image))
                <img
                    src="{{ $object->image->web_url }}?width=960&height=540"
                    class="img-fluid full-width"
                >
            @endif

            <!-- Images -->
            @if (isset($object->video) || count($object->images) > 1)
                @foreach ($object->images as $image)
                    <div class="inline-block">
                        <a href="{{ $image->web_url }}" target="_blank">
                            <img
                                src="{{ $image->web_url }}?width=154&height=154"
                                class="img-fluid img-thumbnail"
                            >
                        </a>
                    </div>
                @endforeach
            @endif
            <!-- End Images -->

            <br>
            <br>

            <!-- long-description -->
            {!! $object->long_description !!}
            <!-- end long-description -->

            <br>

            <!-- videos -->
            @foreach ($object->links as $link)
                @if ($link->key == 'youtube' && $link->id !== $object->video_id)
                    <div class="videoWrapper">
                        <iframe
                            width="560"
                            height="315"
                            src="{{ $link->safe_url }}"
                            frameborder="0"
                            allow="autoplay encrypted-media"
                            allowfullscreen
                        ></iframe>
                    </div>
                    <br>
                @endif
            @endforeach
            <!-- videos -->

            <div class="hide-md">
                @include('campaigns.sidebar')
            </div>

            <div class="btn-group tabs">
                <a class="active btn btn-default" data-tab="backers">
                    <i class="icon-people"></i> Backers
                    &nbsp;&nbsp;
                    <span class="badge badge-secondary">
                        {{ $object->backers_count }}
                    </span>
                </a>
                <a class="btn btn-default" data-tab="comments">
                    <i class="icon-speech"></i> Comments
                    &nbsp;&nbsp;
                    <span class="badge badge-secondary">
                        {{ count($object->comments) }}
                    </span>
                </a>
                <a class="btn btn-default" data-tab="updates">
                    <i class="icon-pin"></i> Updates
                    &nbsp;&nbsp;
                    <span class="badge badge-secondary">
                        {{ count($object->posts) }}
                    </span>
                </a>
                <a class="btn btn-default" data-tab="statistics">
                    <i class="icon-graph"></i> Statistics
                </a>
            </div>

            <!-- backers -->
            @if (count($object->payments) > 0)
                <div class="card tab-content active" id="backers">
                    <table class="table table-striped">
                        <tbody>
                            @foreach ($object->payments as $payment)
                                <tr>
                                    <td>{{ $payment->formatted_created_at }}</td>
                                    <td>{{ $payment->user->name }}</td>
                                    <td>${{ $payment->amount }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="card-footer" style="overflow: hidden;">
                        <a
                            href="{{ config('site.frontend_url') }}/#/payments/new/{{ $object->id }}"
                            class="btn btn-primary pull-right"
                        >Become a Backer <i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
                <br>
            @endif
            <!-- end backers -->

            <!-- Comments -->
            <div class="card tab-content" id="comments">
                <div class="table table-striped">
                    @foreach ($object->comments as $comment)
                        <div class="tr">
                            <div class="th">
                                {{ $comment->user->username }}<br>
                                <img style="max-width: 50px;" src="{{ $comment->user->gravatar }}">
                            </div>
                            <div class="td">
                                <p>{{ $comment->body }}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="card-footer">
                    <a
                        class="btn btn-primary"
                        href="{{ config('site.frontend_url') }}/#/campaigns/{{ $object->id }}#leaveComment"
                    >Leave a Comment <i class="fa fa-angle-right"></i></a>
                </div>
            </div>
            <!-- End Comments -->

            <!-- Posts -->
            @if (count($object->posts) > 0)
                <div class="card tab-content" id="updates">
                    <div class="table table-striped">
                        @foreach ($object->posts as $post)
                            <div class="tr">
                                <div class="td">{{ $post->formatted_created_at }}</div>
                                <div class="td">
                                    <a href="{{ config('site.app_url') }}/p/{{ $post->id }}">
                                        {{ $post->title }}
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
            <!-- End Posts -->

            <!-- Statistics -->
            <div id="statistics" class="card tab-content">
                <table class="table table-striped">
                    <tbody>
                        @if ($object->monetary_goal)
                            <tr>
                                <th>Fundraising Goal</th>
                                <td>
                                    {{ $object->monetary_goal }}
                                </td>
                            </tr>
                        @endif
                        <tr>
                            <th>Amount Raised</th>
                            <td>{{ $object->monetary_total }}</td>
                        </tr>
                        <tr>
                            <th>Percent Raised</th>
                            <td>{{ $object->percent_raised }}%</td>
                        </tr>
                        <tr>
                            <th>Backers</th>
                            <td>{{ $object->backers_count }}</td>
                        </tr>
                        <tr>
                            <th>Campaign Duration</th>
                            <td>{{ $object->days }} days</td>
                        </tr>
                        <tr>
                            <th>Time Left</th>
                            <td>{{ $object->time_left }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- End Statistics -->

        </div>
        <!-- End Campaign Detail -->

        <div class="col-md-4 show-md">
            @include('campaigns.sidebar')
        </div>
    </div>
    <script>
        $('.tabs a').click(function() {
            $('.tabs a, .tab-content').removeClass('active');
            $(this).addClass('active');
            $('.tab-content#' + $(this).attr('data-tab')).addClass('active');
        });
    </script>
@stop
