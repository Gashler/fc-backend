<h2>Ready to Get Paid?</h2>
<p>Merchants, create or use an existing Stripe account and connect it to FunderCat so that you can start receiving scheduled payments for your sales. Please be aware of the following:</p>
<ul>
	<li>You'll need to maintain your own account with Stripe.</li>
	<li>You'll need to handle chargebacks and all customer service issues.</li>
	<li>You'll be responsible for paying Stripe's fees.</li>
</ul>
<br>
<br>
<p><a class="btn btn-primary" href="" id="connect">Connect with Stripe</a></p>
