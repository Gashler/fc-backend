@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Password Reset Complete</div>

                <div class="panel-body">
                    @if (isset($message))
                        <div class="alert alert-{{ $message['type'] }}">
                            {{ $message['body'] }}
                        </div>
                    @endif
                    <a
                        class="btn btn-primary"
                        href="{{ config('site.frontend_url') }}/login"
                    >Login <i class="fa fa-angle-right"></i></a>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
