<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <!-- meta -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('site.app_name') }}</title>

        <!-- js -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <!-- css -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div id="app">
            <div class="col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
                <h1>Login</h1>
                <passport-clients></passport-clients>
                <passport-authorized-clients></passport-authorized-clients>
                <passport-personal-access-tokens></passport-personal-access-tokens>
            </div>
        </div>
        <!-- footer scripts -->
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
