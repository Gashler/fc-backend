<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="background:rgb(235,235,235);">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<style>
			th { vertical-align: top; text-align: right; padding: 0 .5em .5em 0; }
			td { text-align: left; vertical-align: top; }
			.btn { color:white !important; padding: 15px 20px; background-color: rgb(64,64,64); text-decoration: none; display: inline-block; text-transform: uppercase; border-radius: 3px; }
			{!! $email->template->css || '' !!}
		</style>
	</head>
	<body style="font-family: arial; font-size: 12pt; margin: 0;">
		<div class="email-page" style="background:rgb(235,235,235);">
			<br>
			<div class="email-content" style="margin:40px auto; max-width: 520px; width: 100%; font-size: 12pt; box-shadow: 0 0 20px rgba(0,0,0,.13);">
				<div class="email-header" style="color:white; padding: 20px; background-color: rgb(64,64,64); text-align: center;">
					<a class="navbar-brand header" id="logo" href="{{ config('site.shop_url') }}">
						<img style="border: none; color: white; font-size: 17pt;" src="{{ config('site.shop_url') }}/wp-content/uploads/2018/01/white-logo.png" width="200" alt="{{ config('site.app_name') }}">
					</a>
				</div>
				<div class="email-body" style="padding: 40px; background: white; line-height: 1.5em;">
					{!! $email->template->body || '' !!}
					{!! $email->body !!}
					{!! $custom_body !!}
					<p style="color: gray; font-size: 10pt; line-height: 1.25em;">
						The {{ config('site.app_name') }} team<br>
						{{ config('site.shop_url') }}
					</p>
				</div>
				<div class="email-footer" style="padding: 20px; background:rgb(245,245,245); color: gray; font-size: 10pt; line-height: 1.25em;">
					<p>If you have any questions, please contact customer support at <a href="{{ config('site.shop_url') }}/contact">{{ config('site.app_url') }}/contact</a>.
					<p>You're receiving this message because you or someone else signed up for an account with {{ config('site.app_name') }}. To unsubscribe, <a href="{{ config('site.app_url') }}/unsubscribe?email={{ $user['email'] }}">click here</a>.</p>
				</div>
			</div>
			<br>
		</div>
	</body>
</html>
