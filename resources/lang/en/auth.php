<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'connect_filed' => 'Unable to connect with Marketplace.',
    'auth_filed' => 'Unable to authenticate with Marketplace.',
    'user_do_not_exists' => 'This user do not exists in API database.'
];
