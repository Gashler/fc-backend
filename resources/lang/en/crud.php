<?php

return [

    /*
    |--------------------------------------------------------------------------
    | CRUD Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during CRUD operations.
    |
    */

    'attached' =>  str_singular(":child") . " attached to :parent.",
    'created' => ":model created.",
    'deleted' => ":model deleted.",
    'detached' => str_singular(":child") . " detached from :parent.",
    'notFound' => ":model not found.",
    'unauthorized' => "You are not authorized to edit this :model.",
    'updated' => ":model updated.",
];
