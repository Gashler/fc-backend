# About

This project is the back-end/API for FunderCat, but with Laravel/PHP.

# Dependencies
- NginX/Apache
- PHP
- MySQL
- Composer

See Laravel's dependencies for a more complete list: https://laravel.com/docs/5.6/installation#server-requirements

# Installation

Clone the repository:
```bash
git clone git@gitlab.com:Gashler/fc-backend.git
```

Go to the newly-cloned directory, checkout the develop branch, and install the dependencies:
```bash
cd fc-backend
git checkout develop
composer install
```

Log in to MySQL and create a database (and, optionally, user) for this project. Example:
```mysql
create database fc;
```

For most local environments, you'll need to open `/etc/hosts` in an editor and create a new entry for your local testing domain. Example:
```bash
127.0.1.1 api.fc.local
```

For most local environments, you'll also need to create a config (NginX) or virtual host (Apache) file for your local testing domain. Following is an example of how to set this up with Nginx. First, create a new config file:
```bash
sudo touch /etc/nginx/sites-available/fc-backend.local
```

Open the new file in an editor and copy and paste the following into it. Make sure to update the domain, root path, and php version to reflect your environment:
```bash
server {
    listen 80;
    listen [::]:80;

    server_name api.fc.local www.api.fc.local; // update this
    root /home/myuser/code/fc-backend/public/; // update this

    location / {
            index index.php index.html;
            try_files $uri $uri/ /index.php?$args;
    }

    location ~ \.php$ {
            include snippets/fastcgi-php.conf;
            fastcgi_pass unix:/var/run/php/php7.2-fpm.sock; // update this
    }
}

```

Create a symlink in order to enable your new config file with Nginx:
```bash
sudo ln -s /etc/nginx/sites-available/fc-backend.local /etc/nginx/sites-enabled/
```

Copy the `env.stag` (staging environment) file to a new `.env` (local environment) file:
```bash
cp .env.stag .env
```

Update the `.env` file with your local variables. Example:
```bash
APP_NAME=FunderCat
APP_ENV=local
APP_KEY=base64:utXKVCv8HLam4VfA7SQp1TPH30Q9nQP1VBuq7Fmuc38=
APP_DEBUG=true
APP_URL=http://api.fc.local
APP_DOMAIN=api.fc.local
FRONTEND_URL=http://fc.local

TESTING_PASSWORD=mytestingpassword

LOG_CHANNEL=stack

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=mydb
DB_USERNAME=mydbusername
DB_PASSWORD=mydbpassword
```

Open the file `bash/import-db.sh` in an editor. Change the following variables to reflect your local environment:
```bash
local_mysql_db="fc"
local_mysql_user="root"
local_mysql_password="asdf"
local_dump_file="/home/myuser/code/backups/mysql/fc.sql"
```

You'll need to make sure that your public SSH key has been added to the server. Then run `bash bash/import-db.sh` to copy the database from the server and import it locally.

If everything worked correctly, you should now be able to access a public API endpoint in your browser. Example:
```
http://api.fc.local/v1/campaigns
```