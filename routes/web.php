<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// password reset routes
Route::get('password/forgot', 'Auth\ForgotPasswordController@forgot');
Route::post('password/request', 'Auth\ForgotPasswordController@request')->name('password.request');
Route::get('password/reset', 'Auth\ForgotPasswordController@reset')->name('password.reset');
Route::get('password/confirm', 'Auth\ForgotPasswordController@confirm')->name('password.confirm');
Route::post('password/update', 'Auth\ForgotPasswordController@update')->name('password.update');
Route::get('password/complete', 'Auth\ForgotPasswordController@complete')->name('password.complete');

/**
* Global contants for all requests
*/
Route::group(['prefix' => ''], function () {
    if (!defined('APP_URL')) {
        define('APP_URL', env('APP_URL'));
        define('DEFAULT_API_URL', env('DEFAULT_API_URL'));
        define('PROTOCOL', env('PROTOCOL'));
        define('APP_NAME', env('APP_NAME'));
        define('DOCUMENTATION_NAME', env('APP_NAME') . " API Documentation");
    }
});

Route::group(['middleware' => 'throttle:30,1'], function () {

    /**
    * Generate documentation
    */
    Route::resource('swagger', 'SwaggerController');

    /**
    * Return for unauthorized requests
    */
    Route::get('login', ['as' => 'login', 'uses' => function () {
        return response(['message' => 'Unauthorized. To learn how to access the API, see ' . config('site.app_url') . '/docs.'], 401);
    }]);

    /**
    * Testing route
    */
    Route::get('test', function() {
        $pdf_path = '/home/gashler/code/clients/dhealey/ubn/wp-content/themes/utahbabynamer/img/temp.pdf';
    	$im = new imagick();
    	$im->setResolution(700, 700);
    	$im->readimage($pdf_path);
    	$im->setImageResolution(700, 700);
    	$im->setImageFormat('jpeg');
    	$im->setImageCompression(imagick::COMPRESSION_JPEG);
    	$im->setImageCompressionQuality(100);
    	$im->resizeImage(1200, 0,  imagick::FILTER_LANCZOS, 1);
        $im->writeImage(time().'.jpg');
    	$im->clear();
    	$im->destroy();
    });
});

/**
* Client routes
*/
Route::group(['middleware' => 'client', 'prefix' => 'client'], function () {
    Route::get('user-login/{id}', 'Auth\AuthController@loginUserByClient')->name('client.auth.login');
    Route::post('user-register', 'Auth\AuthController@registerUserByClient')->name('client.auth.register');
});

/**
* Public views
*/

// campaigns
Route::get('c/{id}', 'V1\CampaignController@view');

// posts
Route::get('p/{id}', 'V1\PostController@view');

// images
Route::get('i/{year}/{month}/{file}', 'V1\ImageController@view');

/**
* API
*/
Route::group(['prefix' => 'v1'], function () {
    Route::group(['middleware' => 'throttle:60,1'], function () {

        /**
        * Authentication
        */
        Route::post('login', 'Auth\AuthController@login');
        Route::post('register', 'Auth\AuthController@register');
        Route::get('user', 'V1\UserController@getAuthUser');

        /**
        * Public routes
        */

        Route::apiResource('campaigns', 'V1\CampaignController', [
            'only' => ['index', 'show']
        ]);
        Route::resource('images', 'V1\ImageController', [
            'only' => ['store']
        ]);
        Route::post('payments', 'V1\PaymentController@store');
        Route::apiResource('posts', 'V1\PostController', [
            'only' => ['index', 'show']
        ]);

        // Stripe
        Route::get('stripe/finalize-account-connection', 'V1\StripeController@finalizeAccountConnection');

        /**
        * Protected routes
        */
        Route::group(['middleware' => 'auth:api'], function () {

            /**
            * Member routes
            */
            Route::post('send-email', 'V1\EmailController@send');
            Route::resource('images', 'V1\ImageController', [
                'only' => ['update', 'destroy', 'attach', 'detach']
            ]);
            ExtendedRoute::apiResource('campaigns', 'V1\CampaignController', [
                'only' => ['store', 'update', 'destroy', 'attach', 'detach']
            ]);
            Route::get('campaign', 'V1\CampaignController@index');
            ExtendedRoute::apiResource('comments', 'V1\CommentController', [
                'only' => ['show', 'store', 'update', 'destroy']
            ]);
            ExtendedRoute::apiResource('payments', 'V1\PaymentController', [
                'only' => ['show']
            ]);
            ExtendedRoute::apiResource('posts', 'V1\PostController', [
                'only' => ['store', 'update', 'destroy', 'attach', 'detach']
            ]);
            ExtendedRoute::apiResource('users', 'V1\UserController', [
                'only' => ['show', 'store', 'update', 'attach', 'detach']
            ]);

            // Because of how Passport handles requests, we need a unique route
            // for logged-in users to avoid conflicts with the route for
            // logged-out users
            Route::post('payment', 'V1\PaymentController@store');

            /**
            * Admin routes
            */
            Route::group(['middleware' => 'admin'], function () {

                // authentiction
                Route::get('login-as-user/{id}', 'Auth\AuthController@loginAsUser');

                ExtendedRoute::apiResource('comments', 'V1\CommentController', [
                    'only' => ['index', 'attach', 'detach']
                ]);
                ExtendedRoute::apiResource('payments', 'V1\PaymentController', [
                    'only' => ['update', 'destroy', 'attach', 'detach']
                ]);
                Route::apiResource('users', 'V1\UserController', [
                    'only' => ['index', 'destroy']
                ]);

                // Stripe
                Route::get('stripe/merchant-login-url/{merchant_id}', 'V1\StripeController@getMerchantLoginUrl');
                Route::get('stripe/pay-merchants', 'V1\StripeController@payMerchants');
            });
        });
    });
});
