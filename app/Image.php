<?php

namespace App;

use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description',
        'imageable_id',
        'imageable_type',
        'title',
        'uid',
        'url'
    ];

    /**
     * Relationships to be listed in documentation
     */
    public static $relationships = [
        'campaigns'
    ];

    /**
     * Custom attributes to include
     */
    protected $appends = [
        'web_url'
    ];

    /**
     * Relationships
     */

    public function campaigns()
    {
        return $this->belongsToMany(Campaign::class);
    }

    public function imageable()
    {
        return $this->morphTo('imageable');
    }

    /**
     * Validation rules
     */
    public static $rules = [
        'description' => 'string|nullable|max:255',
        'title' => 'string|nullable|max:75',
        'url' => 'string',
        // 'files' => 'image'
    ];

    /**
     * Functions
     */

    public static function faker()
    {
        $faker = Faker::create();
        return [
            'url' => str_random(20),
            'title' => $faker->sentence,
            'description' => $faker->sentence
        ];
    }

    /**
     * Accessors
     */

    public function getWebUrlAttribute()
    {
        return config('site.app_url') . '/i/' . $this->url;
    }
}
