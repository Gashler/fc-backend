<?php

namespace App;

use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'value'
    ];

    /**
     * Relationships to be listed in documentation
     */
    public static $relationships = [
        'linkable'
    ];

    /**
     * Generated attributes to be included
     */
    protected $appends = [
        'embed_url'
    ];

    /**
     * Relationships
     */

    public function linkable()
    {
        return $this->morphTo();
    }

    /**
     * Validation rules
     */
    public static $rules = [
        'key' => 'string',
        'value' => 'string'
    ];

    /**
     * Accessors
     */
    public function getEmbedUrlAttribute()
    {
        if ($this->attributes['key'] == 'youtube') {
            $str = str_replace('watch?v=', 'embed/', $this->attributes['value']);
            $str = str_replace('youtu.be', 'youtube.com/embed', $str);
            $str = str_replace('/embed/embed', '/embed', $str);
            $array = explode('&', $str);
            return $array[0] . '?rel=0';
        }
    }

    /**
     * Functions
     */

    public function faker()
    {
        $faker = Faker::create();
        return [
            'url' => $faker->url
        ];
    }
}
