<?php

namespace App;

use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'amount',
        'description',
        'title',
    ];

    /**
     * Relationships to be listed in documentation
     */
    public static $relationships = [
        'campaign',
        'users',
    ];

    /**
     * Validation rules
     */
    public static $rules = [
        'amount' => 'required|integer',
        'title' => 'required',
        'description' => 'required'
    ];

    /**
     * Generated attributes to be included
     */
    protected $appends = [
        //
    ];

    /**
     * Relationships
     */

    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * Accessors
     */


    /**
     * Functions
     */

    public function faker()
    {
        $faker = Faker::create();
        return [
            'amount' => rand(10,1000),
            'campaign_id' => rand(1,10),
            'description' => $faker->sentence,
            'title' => $faker->text(50),
        ];
    }
}
