<?php

namespace App;

use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'body',
        'campaign_id',
        'title'
    ];

    public static $rules = [
        'body' => 'required',
        'campaign_id' => 'required',
        'title' => 'required'
    ];

    /**
     * Generated attributes to include
     */
    protected $appends = [
        'formatted_created_at'
    ];

    /**
     * Relationships to be listed in documentation
     */
    public static $relationships = [
        'comments',
        'user'
    ];

    /**
    * Relationships
    */

    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function image()
    {
        return $this->belongsTo(Image::class);
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    /**
    * Accessors
    */
    public function getFormattedCreatedAtAttribute()
    {
        return $this->created_at->diffForHumans();
    }

    /**
     * Functions
     */
    public function faker()
    {
        $faker = Faker::create();
        return [
            'title' => $faker->text($maxNbChars = 70),
            'body' => $faker->text($maxNbChars = 1000),
            'user_id' => rand(1,10)
        ];
    }
}
