<?php

namespace App;

use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $fillable = [
        'key',
        'subject',
        'body',
    ];

    public static $rules = [
        'key' => 'required'
    ];

    protected $with = [
        'template'
    ];

    /**
     * Relationships to be listed in documentation
     */
    public static $relationships = [
        'campaigns',
        'templates',
        'users'
    ];

    /**
    * Relationships
    */

    public function subscriptions()
    {
        return $this->belongsToMany(Subscription::class);
    }

    public function template()
    {
        return $this->belongsTo(EmailTemplate::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot(['amount']);
    }
}
