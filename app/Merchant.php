<?php

namespace App;

use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Model;

class Merchant extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'google_place_id',
        'name',
        'vendor_id'
    ];

    /**
     * Relationships to be always included (use sparingly)
     */
    protected $with = [
        //
    ];

    /**
     * Validation rules
     */
    public static $rules = [
        'google_place_id' => 'string|nullable',
        'name' => 'required|string|max:255',
        'stripe_account' => 'array|sometimes'
    ];

    /**
     * Accessors
     */
    public function setPayRateIdAttribute()
    {
        if (!isset($this->pay_rate_id)) {
            $this->pay_rate_id = PayRate::first()->id;
        }
    }

    /**
     * Relationships to be listed in documentation
     */
    public static $relationships = [
        'addresses',
        'google_places',
        'orders',
        'products',
        'stripe_account',
        'tags',
        'users'
    ];

    /**
     * Relationships
     */

    public function addresses()
    {
        return $this->morphMany(Address::class, 'addressable');
    }

    public function google_places()
    {
        return $this->hasMany(GooglePlace::class);
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class)->withPivot(['amount']);
    }

    public function pay_rate()
    {
        return $this->belongsTo(PayRate::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function stripe_account()
    {
        return $this->hasOne(StripeAccount::class);
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot(['amount']);
    }

    /**
     * Functions
     */

    public function faker()
    {
        $faker = Faker::create();
        return [
            'name' => $faker->name
        ];
    }
}
