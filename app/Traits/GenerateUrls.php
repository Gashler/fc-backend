<?php

namespace App\Traits;

trait GenerateUrls
{
    /**
     * @params Multiple number of arguments
     * @return string  Full Url from connected args by "/".
     * @throws \Exception
     */
    public function generateUrl()
    {
        $segments = func_get_args();
        $url = array_shift($segments);
        if (substr($url, -1) !== '/') {
            $url .= '/';
        }
        foreach ($segments as $segment) {
            $type = gettype($segment);
            if ($type !== 'string' && $type !== 'integer') throw new \Exception("Passed url segment must be string or number.");
            $url .= "$segment/";
        }
        return $url;
    }
}