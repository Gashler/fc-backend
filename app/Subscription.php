<?php

namespace App;

use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $fillable = [
        'active',
        'campaign_id',
        'disabled',
        'key',
        'name',
        'start_at'
    ];

    public static $rules = [
        'start_at' => 'date|date_format|nullable'
    ];

    /**
     * Relationships to be listed in documentation
     */
    public static $relationships = [
        'emails',
        'users',
    ];

    /**
    * Relationships
    */

    public function emails()
    {
        return $this->belongsToMany(Email::class)->withPivot('interval', 'interval_type')->orderBy('interval');
    }

    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot('created_at');
    }
}
