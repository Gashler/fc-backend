<?php

namespace App;

use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
    protected $fillable = [
        'file_name',
    ];

    public static $rules = [
        //
    ];

    /**
     * Relationships to be listed in documentation
     */
    public static $relationships = [
        'campaigns',
        'templates'
    ];

    /**
    * Relationships
    */

    public function emails()
    {
        return $this->hasMany(Email::class);
    }
}
