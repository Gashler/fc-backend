<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'value'
    ];

    /**
     * Validation rules
     */
    public static $rules = [
        //
    ];

    /**
     * Relationships to be listed in documentation
     */
    public static $relationships = [
        //
    ];
}
