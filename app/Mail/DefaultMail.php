<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DefaultMail extends Mailable
{
    use Queueable, SerializesModels;

    public $custom_body;
    public $email;
    public $object;
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(
        \App\Email $email,
        \App\User $user,
        $custom_body = null,
        $object = null
    ) {
        $this->email = $email;
        $this->user = $user;
        $this->custom_body = $custom_body;
        $this->object = $object;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.' . $this->email->template->file_name)
            ->subject($this->email->subject);
    }
}
