<?php

namespace App\Policies;

use App\Marketplace;
use App\Order;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the Marketplace can create orders.
     *
     * @param  \App\Marketplace  $marketplace
     * @return mixed
     */
    public function create(Marketplace $marketplace)
    {
        return true;
    }

    /**
     * Determine whether the Marketplace can update the order.
     *
     * @param  \App\Marketplace  $marketplace
     * @param  \App\Order  $order
     * @return mixed
     */
    public function update(Marketplace $marketplace, Order $order)
    {
        return true;
    }

    /**
     * Determine whether the Marketplace can delete the order.
     *
     * @param  \App\Marketplace  $marketplace
     * @param  \App\Order  $order
     * @return mixed
     */
    public function delete(Marketplace $marketplace, Order $order)
    {
        return true;
    }
}
