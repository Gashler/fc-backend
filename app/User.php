<?php

namespace App;

use Faker\Factory as Faker;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'email',
        'first_name',
        'interested_in_becoming_affiliate',
        'last_name',
        'local_stimulus',
        'password',
        'purchase_volume',
        'role_id',
        'username',
        'market_user_id'
    ];

    /**
    * The attributes that should be hidden for arrays.
    *
    * @var array
    */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
    * Generated attributes to be included
    */
    protected $appends = [
        'gravatar',
        'has_password',
        'name'
    ];

    /**
    * Validation rules
    */
    public static $rules = [
        'email' => 'required|email|unique:users',
        'c_password' => 'required|same:password|sometimes',
        'password' => 'required|string|min:6|sometimes',
        'username' => 'required|max:40|unique:users|sometimes',
    ];

    /**
    * Set Attributes
    */

    public function setPasswordAttribute($password)
    {
        $this->attributes["password"] = \Hash::make($password);
    }

    /**
    * Get Accessors
    */

    public function getGravatarAttribute()
    {
        return 'https://gravatar.com/avatar/' . md5($this->email) . '?&size=50&d=' . urlencode(config('site.frontend_url') . '/assets/img/favicon.png');
    }

    public function getHasPasswordAttribute()
    {
        if (isset($this->attributes['password'])) {
            return true;
        }
        return false;
    }

    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
    * Relationships to be listed in documentation
    */
    public static $relationships = [
        'campaigns',
        'emails',
        'role',
        'payments',
        'stripeAccount',
        'stats',
        'subscriptions',
    ];

    /**
    * Relationships
    */

    public function campaigns()
    {
        return $this->hasMany(Campaign::class);
    }

    public function emails()
    {
        return $this->belongsToMany(Email::class);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function stats()
    {
        return $this->hasOne(UserStat::class);
    }

    public function subscriptions()
    {
        return $this->belongsToMany(Subscription::class);
    }

    public function stripe_account()
    {
        return $this->hasOne(StripeAccount::class);
    }

    /**
    * Methods
    */

    public function hasRole($key)
    {
        if (!is_array($key)) { return false;
        }
        foreach ($key as $role){
            if ($this->role->name === $role) {
                return true;
            }
        }
        return false;
    }

    /**
    * Functions
    */
    public function faker()
    {
        $faker = Faker::create();
        $password = \Hash::make(config('site.testing_password'));
        return [
            'email' => $faker->unique()->safeEmail,
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'password' => $password,
            'username' => str_random(20),
        ];
    }
}
