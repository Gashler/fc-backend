<?php

namespace App;

use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'amount',
        'anonymous',
        'campaign_id',
        'card_brand',
        'card_last4',
        'reward_id',
        'user_id'
    ];

    public static $rules = [
        'amount' => 'required'
    ];

    /**
     * Relationships to be listed in documentation
     */
    public static $relationships = [
        'campaign',
        'user',
    ];

    /**
    * Accessors
    */
    public $appends = [
        'formatted_created_at'
    ];

    /**
    * Relationships
    */

    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }

    public function reward()
    {
        return $this->belongsTo(Reward::class);
    }

    public function user()
    {
        if (!$this->anonymous) {
            return $this->belongsTo(User::class);
        }
        // have to return something to avoid error, hence an impossible condition:
        return $this->belongsTo(User::class)->where('id', -1);
    }

    /**
     * Functions
     */
    public function faker()
    {
        $faker = Faker::create();
        $number = rand(1,5);
        $refunded = false;
        if ($number == 1) {
            $refunded = true;
        }
        return [
            'amount' => rand(5, 100),
            'campaign_id' => rand(1, 10),
            'user_id' => rand(1, 10),
            'card_last4' => $faker->randomNumber(4),
            'refunded' => $refunded
        ];
    }

    /**
    * Accessors
    */
    public function getFormattedCreatedAtAttribute()
    {
        return $this->created_at->diffForHumans();
    }
}
