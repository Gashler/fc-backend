<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StripeAccount extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'stripe_user_id'
    ];

    /**
    * Generated Attributes to include
    */
    public $appends = [
        'dashboard_url'
    ];

    /**
     * Validation rules
     */
    public static $rules = [
        //
    ];

    /**
     * Relationships to be listed in documentation
     */
    public static $relationships = [
        'user',
    ];

    /**
     * Relationships
     */

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
    * Accessors
    */
    public function getDashboardUrlAttribute()
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        $account = \Stripe\Account::retrieve($this->stripe_user_id);
        $response = $account->login_links->create();
        return $response->url;
    }

    /**
     * Functions
     */

    public function faker()
    {
        return [
            'access_token' => str_random(10),
            'refresh_token' => str_random(10),
            'stripe_publishable_key' => str_random(10),
            'stripe_user_id' => str_random(10),
        ];
    }
}
