<?php

namespace App;

use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * Validation rules
     */
    public static $rules = [
        'name' => 'string|required'
    ];

    /**
     * Relationships to be listed in documentation
     */
    public static $relationships = [
        'taggable'
    ];

    /**
     * Relationships
     */

    public function taggable()
    {
        return $this->morphTo();
    }

    public function merchants()
    {
        return $this->morphedByMany(Merchant::class, 'taggable');
    }

    /**
     * Functions
     */

    public function faker()
    {
        $faker = Faker::create();
        return [
            'name' => $faker->word
        ];
    }
}
