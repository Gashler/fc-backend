<?php

namespace App\Transformers;

class OrderTransformer extends AbstractTransformer
{
    public function toNewWcOrder($params)
    {
        return [
            'transaction_id' => $params['transaction_id'],
            'status' => $params['status'],
            'order_key' => $params['order_key'],
            'currency' => $params['currency'],
            'total' => $params['total'],
            'total_tax' => $params['total_tax'],
            'shipping_tax' => $params['shipping_tax'],
            'shipping_total' => $params['shipping_total'],
            'payment_method' => $params['payment_method'],
            'prices_include_tax' => $params['prices_include_tax']
        ];
    }

    public function toWcOrder($params)
    {
        $data =  $this->toNewWcOrder($params);
        $data[] = ['id' => $params['id']];
        return $data;
    }

}