<?php
/**
 * Created by PhpStorm.
 * User: adam
 * Date: 29.01.18
 * Time: 10:33
 * Usage: use transformers for transforming data from other providers to our models, reason creating this: better for testing
 */

namespace App\Transformers;


use Illuminate\Database\Eloquent\Model;

abstract class AbstractTransformer
{
    public function transform(Model $model)
    {
        return $model;
    }

    /**
     * Function add prefix to keys in array.
     * @param $array
     * @param string $prefix
     * @return array $prefixed_array;
     */
    public final static function array_keys_add_prefix(array $array, string $prefix)
    {
        $prefixed_array = [];
        foreach ($array as $key => $item) {
            $tmp[$prefix . $key] = $item;
        }
        return $prefixed_array;
    }

    public final function getArrayKey(array $array, $key)
    {
        return $array[$key] ?? null;
    }
}