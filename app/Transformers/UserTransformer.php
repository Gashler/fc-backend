<?php

namespace App\Transformers;

use App\User;

class UserTransformer extends AbstractTransformer
{

    public function toRegisterData($params)
    {
        if (!(auth()->check() && auth()->user()->hasRole(['admin']))) {
            $params['role_id'] = 1;
        }

        // if no username is set, derive from email address
        if (!isset($params['username'])) {
            $array = explode('@', $params['email']);
            $params['username'] = $array[0];
        }

        // ensure unique username
        while (User::where('username', $params['username'])->first()) {
            $str = '';
            $num = '';
            for ($i = 0; $i < strlen($params['username']); $i ++) {
                if (is_numeric($params['username'][$i])) {
                    $num .= $params['username'][$i];
                } else {
                    $str .= $params['username'][$i];
                }
            }
            if ($num == '') {
                $num = 1;
            } else {
                $num = (int)$num;
            }
            $params['username'] = $str . ($num ++);
        }

        return $params;
    }
}
