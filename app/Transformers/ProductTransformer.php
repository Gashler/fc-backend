<?php

namespace App\Transformers;

class ProductTransformer extends AbstractTransformer
{
    public function toWcProduct($product)
    {
        // main data
        $data = [
            'name' => $product->name,
            'type' => 'simple',
            'regular_price' => $product->regular_price ?? null,
            'description' => $product->description ?? null,
            'short_description' => $product->short_description ?? null,
            'categories' => $product->categories ?? null
        ];

        // categories
        $categories = [];
        foreach ($product->categories as $category) {
            $categories[] = [
                'id' => $product->wc_category_id
            ];
        }
        $data['categories'] = $categories;

        // images
        $images = [];
        foreach ($product->images as $index => $image) {
            $images[] = [
                'position' => $index,
                'src' => $image->url
            ];
        }
        $data['images'] = $images;

        return $data;
    }
}
