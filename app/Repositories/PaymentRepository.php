<?php

namespace App\Repositories;

use App\Http\Controllers\Auth\AuthController;
use App\Repositories\CommonCrud;
use App\Repositories\CampaignRepository;
use App\Payment;
use App\Services\PaymentService;

class PaymentRepository extends CommonCrud
{
    public function __construct(
        PaymentService $paymentService
    ) {
        $this->campaignRepo = new CampaignRepository;
        $this->modelName = 'Payment';
        $this->model = 'App\\' . $this->modelName;
        $this->paymentService = $paymentService;
    }

    /**
     * Create Override
     */
    public function createOverride($params)
    {
        // create Stripe charge and other associated objects
        $paymentServiceResponse = $this->paymentService->create($params);
        if (isset($paymentServiceResponse['code']) && $paymentServiceResponse['code'] !== 200) {
            return $paymentServiceResponse;
        }

        // create payment object
        $paymentRepoResponse = $this->create($paymentServiceResponse['params'], null, false, false);
        if ($paymentRepoResponse['code'] !== 200) {
            $paymentRepoResponse['body']['message'] = "Payment was successful, however there was a problem with the server. " . $paymentRepoResponse['body']['message'];
            return $paymentRepoResponse;
        }

        // add user and token to response
        return [
            'body' => [
                'message' => $paymentRepoResponse['body']['message'],
                'payment' => $paymentRepoResponse['body']['data'],
                'token' => $paymentServiceResponse['token'],
                'user' => $paymentServiceResponse['user']
            ],
            'code' => 200
        ];
    }

    /**
    * Show Override
    */
    public function showOverride($id, $params)
    {
        // if including payments with users, deidentify anonymous users
        $response = $this->show($id, $params, false);
        $payment = $response['body'];
        if (isset($payment->user) && $payment->user->anonymous) {
            $payments->user = new User([
                'name' => 'Anonymous'
            ]);
        }
        $response['body'] = $payment;
        return $response;
    }
}
