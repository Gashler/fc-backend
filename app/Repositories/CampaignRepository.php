<?php

namespace App\Repositories;

use App\Repositories\CommonCrud;
use App\Campaign;
use App\User;

class CampaignRepository extends CommonCrud
{
    public function __construct()
    {
        $this->modelName = 'Campaign';
        $this->model = 'App\\' . $this->modelName;
    }

    /**
    * Create Override
    */
    public function createOverride($params)
    {
        // create subscription with campaign
        $params['subscription'] = ['key' => 'campaign'];
        return $this->create($params, null, false, false);
    }

    /**
    * Show Override
    */
    public function showOverride($id, $params)
    {
        // if including payments with users, deidentify anonymous users
        $response = $this->show($id, $params, false);
        $campaign = $response['body'];
        if (isset($campaign->payments)) {
            foreach ($campaign->payments as $index => $payment) {
                if (isset($payment->user) && $payment->anonymous) {
                    $campaign->payments[$index]->user = new User([
                        'name' => 'Anonymous'
                    ]);
                }
            }
        }
        $response['body'] = $campaign;
        return $response;
    }
}
