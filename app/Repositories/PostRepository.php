<?php

namespace App\Repositories;

use App\Repositories\CommonCrud;
use App\Post;
use App\User;

class PostRepository extends CommonCrud
{
    public function __construct()
    {
        $this->modelName = 'Post';
        $this->model = 'App\\' . $this->modelName;
    }

    /**
    * Check Permissions
    */
    private function checkPermissions(int $campaign_id = null, int $post_id = null)
    {
        if (
            auth()->user()->hasRole(['admin'])
            || (
                isset($campaign_id)
                && auth()->user()->campaigns()->find($campaign_id)
            ) || (
                isset($post_id)
                && $post = $this->model->find($post_id)
                && auth()->user()->campaigns()->find($post->campaign_id)
            )
        ) {
            return true;
        }
        return [
            'body' => [
                'message' => "Permission denied"
            ],
            'code' => 401
        ];
    }

    /**
    * Create Override
    */
    public function createOverride($params)
    {
        $response = $this->checkPermissions($params['campaign_id']);
        if ($response !== true) {
            return $response;
        }
        return $this->create($params, null, false, false);
    }

    /**
    * Update Override
    */
    public function updateOverride($id, $params)
    {
        $response = $this->checkPermissions(null, $id);
        if ($response !== true) {
            return $response;
        }
        return $this->update($id, $params, false);
    }

    /**
    * Destroy Override
    */
    public function destroyOverride($id)
    {
        $response = $this->checkPermissions(null, $params);
        if ($response !== true) {
            return $response;
        }
        return $this->destroy($id, false);
    }

    /**
    * Attach Override
    */
    public function attachOverride(int $id, string $relation, int $relation_id, $allowOverride = true)
    {
        $response = $this->checkPermissions(null, $id);
        if ($response !== true) {
            return $response;
        }
        return $this->attach($id, $relation, $relation_id, false);
    }

    /**
    * Destroy Override
    */
    public function detachOverride($id)
    {
        $response = $this->checkPermissions(null, $id);
        if ($response !== true) {
            return $response;
        }
        return $this->detach($id, $relation, $relation_id, false);
    }
}
