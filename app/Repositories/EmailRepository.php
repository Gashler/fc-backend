<?php

namespace App\Repositories;

use App\Repositories\CommonCrud;
use App\Email;

class EmailRepository extends CommonCrud
{
    public function __construct()
    {
        $this->modelName = 'Email';
        $this->model = 'App\\' . $this->modelName;
    }

    /**
     * Send an email
     */
    public function send()
    {
        $response = $this->mailService($relationship, $relationship_id);
    }
}
