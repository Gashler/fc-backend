<?php

namespace App\Repositories;

use App\Repositories\CommonCrud;
use App\Subscription;

class SubscriptionRepository extends CommonCrud
{
    public function __construct()
    {
        $this->modelName = 'Subscription';
        $this->model = 'App\\' . $this->modelName;
    }
}
