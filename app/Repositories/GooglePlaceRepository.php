<?php

namespace App\Repositories;

use App\Repositories\CommonCrud;
use App\GooglePlace;
use App\Services\GooglePlacesService;

class GooglePlaceRepository extends CommonCrud
{
    public function __construct() {
        $this->modelName = 'GooglePlace';
        $this->model = 'App\\' . $this->modelName;
        $this->googlePlacesService = new GooglePlacesService;
    }

    /**
     * Update Merchant's Google Place to ensure accurate information
     */
    public function updatePlaces($object)
    {
        if (isset($object->google_places) || isset($object->google_place_id)) {
            if (isset($object->google_place_id) && count($object->google_places) == 0) {
                $object->google_places = [
                    'google_place_id' => $object->google_place_id
                ];
            }
            foreach ($object->google_places as $index => $google_place) {
                $google_place->data = $this->googlePlacesService->getDetails($google_place->google_place_id);
                $google_place->update([
                    'data' => $google_place->data
                ]);
                $object->google_places[$index] = $google_place;
            }
        }
        return $object;
    }
}
