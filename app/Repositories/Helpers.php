<?php

namespace App\Repositories;

class Helpers
{
    /**
     * Validate
     */
    public function validate(array $params, string $relation = null, $updating = false)
    {
        // determine model
        if (isset($relation)) {
            $model = str_singular('App\\' . studly_case($relation));
            $modelName = str_singular(studly_case($relation));
        } else {
            $model = $this->model;
            $modelName = $this->modelName;
        }

        $rules = $model::$rules;

        // define exceptions to validation rules
        foreach ($rules as $key => $ruleLines) {

            // if user_id is required but not included, use authorized user's ID
            if ($key == 'user_id') {

                // if not admin, force authorized user's ID
                if (!auth()->user()->hasRole(['admin'])) {
                    $params['user_id'] = auth()->user()->id;
                }

                // otherwise, only use admin's ID if user_id isn't included
                $array = explode('|', $ruleLines);
                foreach ($array as $rule) {
                    if ($rule == 'required') {
                        if (!isset($params['user_id'])) {
                            \Log::info('user_id is not included. Use admin ID.');
                            $params['user_id'] = auth()->user()->id;
                            break;
                        }
                    }
                }
                break;
            }

            // if updating
            if ($updating) {
                $array = explode('|', $ruleLines);
                foreach ($array as $index => $rule) {

                    // handle unique rules when updating
                    if (isset($params['id']) && $rule == 'unique:' . snake_case(str_plural($modelName))) {
                        // $object = $model::find($params['id']);
                        $array[$index] = 'unique:' . snake_case(str_plural($modelName)) . ',' . $key . ',' . $params['id'];
                    }

                    // ignore "required" rules
                    if ($rule == 'required') {
                        unset($array[$index]);
                    }

                    // udate rules
                    $ruleLines = implode('|', $array);
                    $rules[$key] = $ruleLines;
                }
            }
        }

        // validate
        $validator = \Validator::make($params, $rules);
        if ($validator->fails()) {

            $word = 'Error';
            if (count($validator->errors()) > 0) {
                $word = 'Errors';
            }
            $model = $this->modelName;
            if (isset($relation)) {
                $model = str_singular($relation);
            }

            return [
                'message' => $word . ' while validating ' . $model . '.',
                'errors' => $validator->errors()
            ];
        }

        return [
            'params' => $params
        ];
    }

    /**
    * Validate relations (when posted with another object)
    */
    public function validateRelations(
        $params,
        $path = null,
        $parentName = null,
        $relationships = [],
        $relationshipNames = [],
        $errors = null,
        $code = 200,
        $firstIteration = true
    ) {
        // see if there are any relations to store
        foreach ($params as $key => $value) {
            if (is_array($value)) {

                // if a class doesn't exist for the relation, remove it
                if (!class_exists('App\\' . studly_case(str_singular($key)))) {
                    unset($params[$key]);
                    continue;
                }

                // create arrays of relationships and relationship names for top-level relations
                if (!isset($parentName)) {
                    $relationships[$key] = $value;
                    $relationshipNames[] = $key;

                // add to array of relationship names for sub-level relations
                } elseif ($firstIteration) {
                    if (!is_numeric($parentName)) {
                        $path .= '.' . $parentName;
                        $relationshipNames[] = $path;
                    }
                }

                // recursively handle sub-relations
                foreach ($value as $subKey => $subValue) {
                    if (is_array($subValue)) {
                        if (is_numeric($subKey)) {
                            if (isset($path) && $subKey == 0) {
                                $key = $path . '.' . $key;
                                $relationshipNames[] = $key;
                            }
                        }
                        $response = $this->validateRelations(
                            $subValue,
                            $key,
                            $subKey,
                            $relationships,
                            $relationshipNames,
                            $errors,
                            $code,
                            $firstIteration
                        );
                        if ($response['code'] !== 200) {
                            $errors = (object) array_merge((array) $errors, (array) $response['body']);
                            $code = $response['code'];
                            break;
                        }
                        $relationships = $response['body']['relationships'];
                        $relationshipNames = $response['body']['relationshipNames'];
                    }
                    $firstIteration = false;
                }
            }
        }

        // standardize formats for different relation types
        foreach ($relationships as $relationshipName => $relation) {
            foreach ($relation as $key => $value) {
                if (!is_int($key)) {
                    $relationships[$relationshipName] = [$relation];
                }
                break;
            }
        }

        // validate relations
        foreach ($relationships as $relationshipName => $relationCategory) {

            // cycle through relations
            foreach ($relationCategory as $index => $relation) {

                // validate
                $updating = isset($relation['id']) ? true : false;
                $relationshipModel = 'App\\' . studly_case(str_singular($relationshipName));

                if ($response = $this->validate($relation, $relationshipName, $updating)) {
                    if (isset($response['errors'])) {
                        return [
                            'body' => $response,
                            'code' => 400
                        ];
                    }
                    $relationships[$relationshipName][$index] = $response['params'];
                }

                // if updating, get record to attach if it exists
                if ($updating) {
                    if ($childObject = $relationshipModel::find($relation['id'])) {
                        $relationships[$relationshipName][$index]['object'] = $childObject;
                    }
                }
            }
        }

        // handle response
        if ($code !== 200) {
            $body = $errors;
        } else {
            $body = [
                'relationships' => $relationships,
                'relationshipNames' => $relationshipNames
            ];
        }
        return [
            'body' => $body,
            'code' => $code
        ];
    }

    /**
     *  Prepare relations array for Eloquent request
     */
    public function prepareRelations($params)
    {
        if (isset($params['with'])) {
            return $relationships = explode(',', $params['with']);
        }
        return [];
    }

    /**
     *  Prepare where clauses for Eloquent request
     */
    public function prepareWhere($params)
    {
        if (isset($params['where'])) {
            $wheres = explode(',', $params['where']);
            $operators = [
                '~',
                '<=',
                '>=',
                '<',
                '>'
            ];
            foreach ($wheres as $index => $where) {
                foreach ($operators as $operator) {
                    if (strpos($where, $operator) !== false) {
                        $array = explode($operator, $where);
                        $array[2] = $array[1];

                        if ($array[2] == 'me') {
                            $array[2] = auth()->user()->id;
                        }

                        if ($operator == '~') {
                            $operator = '=';
                        }
                        $array[1] = $operator;
                        $wheres[$index] = $array;
                        break;
                    }
                }
            }
            return $wheres;
        }
        return [];
    }

    /**
    * Determine what verb to use when creating a relationship
    */
    public function getRelationshipVerb($object, $relationship, $method = 'connect')
    {
        $className = get_class($object->{$relationship}());
        $array = explode('\\', $className);
        $relationshipType = end($array);
        $verbs = [
            'BelongsTo' => [
                'connect' => 'associate',
                'disconnect' => 'disassociate'
            ],
            'BelongsToMany' => [
                'connect' => 'attach',
                'disconnect' => 'detach'
            ],
            'HasOne' => [
                'connect' => 'save',
                'disconnect' => 'detach'
            ],
            'HasMany' => [
                'connect' => 'save',
                'disconnect' => 'destroy'
            ],
            'MorphTo' => [
                'connect' => 'save',
                'disconnect' => 'detach'
            ],
            'MorphToMany' => [
                'connect' => 'save',
                'disconnect' => 'detach'
            ],
            'MorphMany' => [
                'connect' => 'save',
                'disconnect' => 'destroy'
            ]
        ];
        return $verbs[$relationshipType][$method];
    }

    /**
    * Store/attach relations (when posted with another object)
    */
    public function createRelationships($parentObject, $relationships)
    {
        // cycle through relationships
        foreach ($relationships as $relationshipName => $relationship) {

            // cycle through relations
            if (!is_array($relationship)) {
                continue;
            }
            foreach ($relationship as $index => $relation) {
                if (!is_array($relation)) {
                    continue;
                }

                // get relationship verb
                $verb = $this->getRelationshipVerb($parentObject, $relationshipName);

                // if "unique" parameter is specified, search for existing object with the specified key and value
                $relationshipModel = 'App\\' . studly_case(str_singular($relationshipName));
                if (isset($relation['unique'])) {
                    $childObject = $relationshipModel::where(
                        $relation['unique'], $relation[$relation['unique']]
                    )->first();
                }

                // first separate extraneous data
                if (isset($relation['pivot'])) {
                    $pivot = $relation['pivot'];
                    unset($relation['pivot']);
                }
                if (isset($relation['object'])) {
                    $childObject = $relation['object'];
                    unset($relation['object']);
                }
                if (isset($relation['unique'])) {
                    $unique = $relation['unique'];
                    unset($relation['unique']);
                }

                // recursively handle sub-relations
                foreach ($relation as $key => $value) {
                    if (is_array($value)) {
                        $response = $this->createRelationships($parentObject, $value);
                        $parentObject->$relationshipName = $response->$relationshipName;
                    }
                }

                // if record exists
                if (isset($childObject)) {

                    // update object
                    $childObject->update($relation);

                    // attach the object to the parent (if not already attached)
                    if (!$parentObject->$relationshipName()->find($childObject->id)) {
                        $parentObject->$relationshipName()->$verb($childObject);
                    }

                // if record doesn't exist
                } else {
                    $relationshipRepoName = '\App\Repositories\\' . str_singular(studly_case($relationshipName)) . 'Repository';
                    $relationshipRepo = new $relationshipRepoName;
                    $response = $relationshipRepo->create($relation, null, true);
                    if ($response['code'] == 200) {
                        $parentObject->$relationshipName()->$verb($response['body']['data']);
                        unset($parentObject->$relationshipName);
                        // $parentObject->save();
                    }
                }

                // store pivot data if exists
                if (isset($pivot) && isset($relation['id'])) {
                    $parentObject->$relationshipName()->updateExistingPivot($relation['id'], $pivot);
                }

                unset($childObject);
            }
        }
        return $parentObject;
    }

    /**
     * Determinate whether to attach or detach a relation
     */
    public function connection(int $id, string $relation, int $relation_id, string $method)
    {
        if (!$this->validateRelationMethod($this->model, $relation)) {
            return [
                'data' => trans('validation.wrong_relation', ['attribute' => $relation]),
                'code' => 400
            ];
        }
        return $this->$method($id, $relation, $relation_id);
    }

    /**
     * Validate relation exists
     * TODO: Check if relation is manyToMany, or use existing $relations array to check if relation exists
     */
    protected function validateRelationMethod($model, $relation)
    {
        return method_exists($model, $relation);
    }

    /**
     * Find object by ID
     */
    public final function find(int $id, array $with = []) {
        return $this->model::with($with)->find($id);
    }

    /**
     * Find object by key / value
     */
    public final function findBy(string $key, $value) {
        return $this->model::where($key, $value)->first();
    }
}
