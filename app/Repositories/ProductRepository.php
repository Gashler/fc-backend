<?php

namespace App\Repositories;

use App\Product;
use App\Setting;
use App\Repositories\CommonCrud;
use App\Repositories\GooglePlaceRepository;
use App\Services\MailService;

class ProductRepository extends CommonCrud
{
    public function __construct() {
        $this->googlePlaceRepo = new GooglePlaceRepository;
        $this->modelName = 'Product';
        $this->model = 'App\\' . $this->modelName;
        $this->mailService = new MailService;
    }

    /**
     * Create Override
     */
    public function createOverride(array $params, $object = null, $child = false, $allowOverride = true)
    {
        if (isset($params['merchant']) && isset($params['merchant']['google_place_id'])) {

            // assign google place to merchant
            if (!isset($params['google_places'])) {
                $params['google_places'] = [
                    [
                        'google_place_id' => $params['merchant']['google_place_id'],
                        'unique' => 'google_place_id'
                    ]
                ];
            }

            // assign google place to products
            if (!isset($params['merchant']['google_places'])) {
                $params['merchant']['google_places'] = [
                    [
                        'google_place_id' => $params['merchant']['google_place_id'],
                        'unique' => 'google_place_id'
                    ]
                ];
            }
        }
        return $this->create($params, $object, $child, false);
    }

    /**
     * Create Callback
     */
    public function createCallback(array $response)
    {
        if (isset($response['body']['data']['google_places'])) {
            $response['body']['data'] = $this->googlePlaceRepo->updatePlaces($response['body']['data']);
            $this->mailService->send('product_submitted_admin', [
                'product' => $response['body']['data']
            ], [config('site.admin_user_id')]);
            $this->mailService->send('product_submitted_user', ['product' => $response['body']['data']]);
        }

        return $response;
    }

    /**
     * Show Callback
     */
    public function showCallback(array $response)
    {
        $response['body'] = $this->googlePlaceRepo->updatePlaces($response['body']);
        return $response;
    }

    /**
     * Update Override
     */
    public function updateOverride($id, array $params, $allowOverride = true)
    {
        if (!$originalProduct = Product::find($id)) {
            return [
                'body' => [
                    'message' => "Product not found."
                ],
                'code' => 404
            ];
        }
        $response = $this->update($id, $params, false);
        if ($response['code'] !== 200) {
            return $response;
        }
        $product = $response['body']['data'];

        // if approving product, send email
        if (isset($params['approved'])
            && $params['approved']
            && !$originalProduct->approved
            && $product->approved
        ) {
            $this->mailService->send('product_approved',
                ['product' => $response['body']['data']], [$product->user_id]
            );
        }

        // if accepting product, send email
        if (isset($params['accepted'])
            && $params['accepted']
            && !$originalProduct->accepted
            && $product->accepted
        ) {
            $this->mailService->send('product_accepted', [
                    'product' => $response['body']['data'],
                    'advocate_commission_rate' => Setting::where('key', 'advocate_commission_rate')->first()->value,
                    'minimum_payout_amount' => Setting::where('key', 'minimum_payout_amount')->first()->value,
                    'payout_schedule' => Setting::where('key', 'payout_schedule')->first()->value
                ], [$product->user_id]
            );
        }

        return $response;
    }
}
