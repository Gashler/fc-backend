<?php

namespace App\Repositories;

use App\Repositories\CommonCrud;
use App\Address;

class AddressRepository extends CommonCrud
{
    public function __construct()
    {
        $this->modelName = 'Address';
        $this->model = 'App\\' . $this->modelName;
    }
}
