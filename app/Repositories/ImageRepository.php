<?php

namespace App\Repositories;

use App\Repositories\CommonCrud;
use App\Image;
use Intervention\Image\Facades\Image as Intervention;

class ImageRepository extends CommonCrud
{
    public function __construct()
    {
        $this->modelName = 'Image';
        $this->model = 'App\\' . $this->modelName;
        $this->uploadcare = new \Uploadcare\Api(
            config('services.uploadcare.key'),
            config('services.uploadcare.secret')
        );
    }

    /**
     * Create Callback
     */
    public function createOverride($params, $object = null, $child = false)
    {
        $params = request()->all();
        $index = 0;
        $processedFiles = [];
        foreach($params['files'] as $file) {

            // create path for files if it doesn't already exist
            $path = date('Y/m');
            $fullPath = public_path() . '/uploads/' . $path;
            if (!file_exists($fullPath)) {
                mkdir($fullPath, 0755, true);
            }

            // get file extension
            $params['extension'] = strtolower($file->getClientOriginalExtension());

            // get filename
            $filename = basename($_FILES['files']["name"][$index]);
            $url = $path . '/' . $filename;
            $array = explode('.', $filename);
            $filename = $array[0];
            $extension = $array[1];

            // ensure unique URL
            while ($existing_file = Image::where('url', $url)->first()) {
                $url = $path . '/' . $filename . '-' . time() . '.' . $extension;
            }

            // move the uploaded file to its destination
            $uploadSuccess = $file->move($fullPath, $filename . '.' . $params['extension']);

            // assign values to data array
            $file_number = $index + 1;
            if ($index < 10) {
                $file_number = '00' . $file_number;
            } elseif ($index < 100) {
                $file_number = '0' . $file_number;
            }
            $processedFile['url'] = $url;
            if (isset($params['title'])) {
                if ($params['title'] == '') {
                    $processedFile['title'] = $filename . '.' . $params['extension'];
                } else {
                    $processedFile['title'] = $params['title'] . ' ' . $file_number;
                }
            }
            if (isset($params['description'])) {
                $processedFile['description'] = $params['description'];
            }
            $processedFile['imageable_type'] = $params['imageable_type'];
            $processedFile['imageable_id'] = $params['imageable_id'];
            $processedFiles[] = $processedFile;
            $index ++;
        }

        // store in db and redirect
        if (count($processedFiles) > 0) {
            foreach ($processedFiles as $processedFile) {
                \Log::info('$processedFile["url"] = ' . $processedFile['url']);
                $responses[] = $this->create($processedFile, null, false, false);
            }
            $response = $responses[0];
            $s = '';
            if (count($processedFiles) > 1) {
                $s = 's';
            }
            $response['body']['message'] = count($processedFiles) . " image" . $s . " created.";
            $response['body']['data'] = [];
            foreach ($responses as $r) {
                $response['body']['data'][] = $r;
            }
            return $response;
        }

        // if files have already been processed
        return $this->create($params, null, false, false);
    }

    /**
     * Destroy
     */
    public function destroyOverride($id)
    {
        $image = Image::find($id);
        if (isset($image->uid)) {
            $file = $this->uploadcare->getFile($image->uid);
            $file->delete();
        }
        return $this->destroy($id, false);
    }
}
