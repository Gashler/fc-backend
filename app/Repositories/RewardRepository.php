<?php

namespace App\Repositories;

use App\Repositories\CommonCrud;
use App\Reward;

class RewardRepository extends CommonCrud
{
    public function __construct()
    {
        $this->modelName = 'Reward';
        $this->model = 'App\\' . $this->modelName;
    }
}
