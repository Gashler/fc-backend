<?php

namespace App\Repositories;

use App\Repositories\CommonCrud;
use App\Link;

class LinkRepository extends CommonCrud
{
    public function __construct()
    {
        $this->modelName = 'Link';
        $this->model = 'App\\' . $this->modelName;
    }
}
