<?php

namespace App\Repositories;

use App\Repositories\CommonCrud;
use App\Like;

class LikeRepository extends CommonCrud
{
    public function __construct()
    {
        $this->modelName = 'Like';
        $this->model = 'App\\' . $this->modelName;
    }
}
