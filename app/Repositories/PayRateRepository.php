<?php

namespace App\Repositories;

use App\Repositories\CommonCrud;
use App\PayRate;

class PayRateRepository extends CommonCrud
{
    public function __construct()
    {
        $this->modelName = 'PayRate';
        $this->model = 'App\\' . $this->modelName;
    }
}
