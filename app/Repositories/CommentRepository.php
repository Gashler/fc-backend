<?php

namespace App\Repositories;

use App\Repositories\CommonCrud;
use App\Comment;

class CommentRepository extends CommonCrud
{
    public function __construct()
    {
        $this->modelName = 'Comment';
        $this->model = 'App\\' . $this->modelName;
    }
}
