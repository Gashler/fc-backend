<?php

namespace App\Repositories;

use App\Order;
use App\Transformers\OrderTransformer;
use App\WcOrder;

class OrderRepository extends CommonCrud
{
    public function __construct()
    {
        $this->modelName = 'Order';
        $this->model = 'App\\' . $this->modelName;
    }

    public function findByWcOrder($wcOrderId)
    {
        return WcOrder::find($wcOrderId)->order;
    }

    public function storeWebhookOrder($params)
    {
        $order = Order::create();
        $order->wcOrder()->save(new WcOrder($params));

        // TODO: Implementation of saving items

        return $order;
    }

    public function updateWebhookOrder($params)
    {
        $order = $this->findByWcOrder($params['id']);
        $order->wcOrder()->update($params);

        return $order;
    }

    public function destroyWebhookOrder($id)
    {
        $order = $this->findByWcOrder($id);
        return $this->destroy($order->id);
    }
}


