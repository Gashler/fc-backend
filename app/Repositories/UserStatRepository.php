<?php

namespace App\Repositories;

use App\Repositories\CommonCrud;
use App\UserStat;

class UserStatRepository extends CommonCrud
{
    public function __construct()
    {
        $this->modelName = 'UserStat';
        $this->model = 'App\\' . $this->modelName;
    }

    /**
     * Calculate UserStats
     */
    public function calculate($params)
    {
        /**
        * Consumer Funding Stimulation ($local_boost) lifetime purchase times pre-tax ratio to sales
        * Local Pre-tax Ratio to Sales (local_multiplier) (hard number such as 20%)
        * Conglomerate Pre-tax Ratio to Sales (conglomerate_multiplier) (hard number such as 10%)
        */

        if (!isset($params['total_volume'])) {
            $total_volume = \App\User::find($params['user_id'])->orders->sum('pivot.amount');
        } else {
            $total_volume = $params['total_volume'];
        }

        $conglomerate_multiplier = \App\Setting::where('key', 'conglomerate_multiplier')->first()->value;
        $local_multiplier = \App\Setting::where('key', 'local_multiplier')->first()->value;
        $business_donation_rate = round(\App\Setting::where('key', 'business_donation_rate')->first()->value, 2);
        $local_boost = round(($total_volume * $local_multiplier) - ($total_volume * $conglomerate_multiplier), 2);
        $generated_funding = round($local_boost * $business_donation_rate, 2);

        return [
            'generated_funding' => $generated_funding,
            'local_boost' => $local_boost,
            'total_volume' => $total_volume,
            'user_id' => $params['user_id']
        ];
    }

    /**
     * Create
     */
    public function createOverride($params, $object = null, $child = false)
    {
        $params = $this->calculate($params);
        return $this->create($params, null, false, false);
    }
}
