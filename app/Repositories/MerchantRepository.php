<?php

namespace App\Repositories;

use App\Merchant;
use App\Repositories\CommonCrud;
use App\Repositories\GooglePlaceRepository;
use App\Services\GooglePlacesService;
use App\Services\MailService;

class MerchantRepository extends CommonCrud
{
    public function __construct() {
        $this->modelName = 'Merchant';
        $this->model = 'App\\' . $this->modelName;
        $this->googlePlaceRepo = new GooglePlaceRepository;
        $this->googlePlacesService = new GooglePlacesService;
        $this->mailService = new MailService;
    }

    /**
     * Create Override
     */
    public function createOverride($params, $object = null, $child = false, $allowOverride = true)
    {
        if (isset($params['google_place_id'])) {

            // assign google place to merchant
            if (!isset($params['google_places'])) {
                $params['google_places'] = [
                    [
                        'google_place_id' => $params['google_place_id'],
                        'unique' => 'google_place_id'
                    ]
                ];
            }

            // assign google place to products
            if (isset($params['products'])) {
                foreach ($params['products'] as $index => $product) {
                    if (!isset($product['google_places'])) {
                        $params['products'][$index]['google_places'] = [
                            [
                                'google_place_id' => $params['google_place_id'],
                                'unique' => 'google_place_id'
                            ]
                        ];
                    }
                }
            }
        }
        return $this->create($params, $object, $child, false);
    }

    /**
     * Create Callback
     */
    public function createCallback(array $response)
    {
        // retrieve Google Place data
        $response['body']['data'] = $this->googlePlaceRepo->updatePlaces($response['body']['data']);

        // if created product with merchant, send emails
        if (isset($response['body']['data']['products']) && count($response['body']['data']['products']) > 0) {
            $merchant = $response['body']['data'];
            $product = $merchant->products[0];
            $product->merchant = $merchant;
            unset($product->merchant->products);
            $this->mailService->send('product_submitted_admin', [
                'product' => $response['body']['data']['products'][0]
            ], [config('site.admin_user_id')]);
            $this->mailService->send('product_submitted_user', [
                'product' => $response['body']['data']['products'][0]
            ]);
        }

        return $response;
    }

    /**
     * Show Callback
     */
    public function showCallback(array $response)
    {
        $response['body'] = $this->googlePlaceRepo->updatePlaces($response['body']);
        return $response;
    }
}
