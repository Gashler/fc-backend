<?php

namespace App\Repositories;

use App\Repositories\Helpers;
use Illuminate\Database\QueryException;

class CommonCrud extends Helpers
{
    protected $model;

    public function __construct() {
        if (!isset($this->model)) {
            $arguments = func_get_args();
            $this->model = $arguments[0];
        }
        if (!isset($this->modelName)) {
            $array = explode("\\", $this->model);
            $this->modelName = $array[1];
        }
    }

    /**
     * Index
     */
    public function index($params, $allowOverride = true)
    {
        if ($allowOverride && method_exists($this, 'indexOverride')) {
            return $this->indexOverride($params);
        }

        $relationships = $this->prepareRelations($params);
        $where = $this->prepareWhere($params);
        $orderBy = $params['orderby'] ?? 'created_at';
        $direction = $params['direction'] ?? 'DESC';
        $perPage = $params['per_page'] ?? 25;
        if (!isset($limit)) {
            $response = $this->model::with($relationships)
                ->where($where)
                ->orderBy($orderBy, $direction)
                ->orderBy('created_at', 'DESC')
                ->paginate($perPage);
        } else {
            if ($params['limit'] == 0) {
                $params['limit'] = -1;
            }
            $response = $this->model::with($relationships)
                ->where($where)
                ->orderBy($orderBy, $direction)
                ->take($params['limit'])
                ->get();
        }

        // handle response
        if (method_exists($this, 'indexCallback')) {
            return $this->indexCallback($response);
        }
        return $response;
    }

    /**
     * Create
     */
    public function create($params, $object = null, $child = false, $allowOverride = true)
    {
        // determine if creating or updating
        if (isset($params['id'])) {
            $object = $this->model::find($params['id']);
        }
        if (isset($object)) {
            $updating = true;
        } else {
            $updating = false;
        }

        // run override method (if exists)
        if (!$updating && $allowOverride && method_exists($this, 'createOverride')) {
            return $this->createOverride($params, $object, $child);
        }

        // validate data
        $response = $this->validate($params, null, $updating);
        if (isset($response['errors'])) {
            return [
                'body' => $response['errors'],
                'code' => 400
            ];
        } else {
            $params = $response['params'];
        }

        // validate relations
        $response = $this->validateRelations($params);
        if ($response['code'] !== 200) {
            return $response;
        }
        $relationships = $response['body']['relationships'];
        $relationshipNames = $response['body']['relationshipNames'];

        // store new object
        if (!$updating) {
            $object = $this->model::create($params);

        // or update existing object
        } else {
            foreach ($params as $key => $param) {
                if (is_array($param)) {
                    unset($params[$key]);
                }
            }
            $object->update($params);
        }

        // preserve data from being lost by recursion
        if (!$child) {
            session([
                'parent_object' => $object,
                'relationship_names' => $relationshipNames
            ]);
        }

        // store relations (if any)
        $object = $this->createRelationships($object, $relationships);

        // prepare message
        if ($updating) {
            $message = trans('crud.updated', ['model' => $this->modelName]);
        } else {
            $message = trans('crud.created', ['model' => $this->modelName]);
        }

        // handle response
        if (!$child) {
            $object = session('parent_object');
            $object = $object::with(session('relationship_names'))->find($object['id']);
        }
        $response = [
            'body' => [
                'message' => $message,
                'data' => $object
            ],
            'code' => 200
        ];
        if (!$updating && !$child && method_exists($this, 'createCallback')) {
            return $this->createCallback($response);
        }
        return $response;
    }

    /**
     * Show
     */
    public function show($id, $params, $allowOverride = true)
    {
        if ($allowOverride && method_exists($this, 'showOverride')) {
            return $this->showOverride($id, $params);
        }

        if ($id == 'me') {
            $id = auth()->user()->id;
        }
        $relationships = $this->prepareRelations($params);
        $object = $this->model::with($relationships)->find($id);
        if (!isset($object)) {
            return [
                'body' => [
                    'message' => trans('crud.notFound', ['model' => $this->modelName])
                ],
                'code' => 404
            ];
        }

        // handle response
        $response = [
            'body' => $object,
            'code' => 200
        ];
        if (method_exists($this, 'showCallback')) {
            return $this->showCallback($response);
        }
        return $response;
    }

    /**
     * Update
     */
    public function update($id, $params, $allowOverride = true)
    {
        if ($allowOverride && method_exists($this, 'updateOverride')) {
            return $this->updateOverride($id, $params);
        }

        if ($id == 'me') {
            $id = auth()->user()->id;
        }
        $object = $this->model::find($id);
        $response = $this->create($params, $object);
        if ($response['code'] == 200 && method_exists($this, 'updateCallback')) {
            return $this->updateCallback($response);
        }
        return $response;
    }

    /**
     * Destroy
     */
    public function destroy($id, $allowOverride = true)
    {
        if ($allowOverride && method_exists($this, 'destroyOverride')) {
            return $this->destroyOverride($id);
        }

        try {
            $this->model::destroy($id);
        } catch (QueryException $e) {
            return [
                'body' => [
                    'message' => $e,
                ],
                'code' => 422
            ];
        }
        $response = [
            'body' => [
                'message' => trans('crud.deleted', ['model' => $this->modelName]),
            ],
            'code' => 200
        ];
        if (method_exists($this, 'destroyCallback')) {
            return $this->destroyCallback($response);
        }
        return $response;
    }

    /**
     * Attach relation
     */
    public function attach(int $id, string $relation, int $relation_id, $allowOverride = true)
    {
        if ($allowOverride && method_exists($this, 'attachOverride')) {
            return $this->attachOverride($id, $relation, $relation_id);
        }

        if ($id == 'me') {
            $id = auth()->user()->id;
        }
        $parentObject = $this->model::find($id);
        $childModel = 'App\\' . str_singular(ucfirst($relation));
        $childObject = $childModel::find($relation_id);

        // return if relationship already exists
        if ($parentObject->$relation()->find($childObject->id)) {
            return [
                'body' => [
                    'message' => "Relationship already exists."
                ],
                'code' => 200
            ];
        }

        try {
            $parentObject->$relation()->attach($childObject);
        } catch (QueryException $e) {
            return [
                'body' => [
                    'message' => $e->getMessage()
                ],
                'code' => 422
            ];
        }

        // handle response
        $response = [
            'body' => [
                'message' => trans('crud.attached', [
                    'parent' => $this->modelName,
                    'child' => $relation
                ]),
                'data' => $this->model::find($id)
            ],
            'code' => 200
        ];
        if (method_exists($this, 'attachCallback')) {
            return $this->attachCallback($response);
        }
        return $response;
    }

    /**
     * Detach relation
     */
    public function detach(int $id, string $relation, int $relation_id, $allowOverride = true)
    {
        if ($allowOverride && method_exists($this, 'detachOverride')) {
            return $this->detachOverride($id, $relation, $relation_id);
        }

        if ($id == 'me') {
            $id = auth()->user()->id;
        }
        $parentObject = $this->find($id);
        $childModel = 'App\\' . str_singular(ucfirst($relation));
        $childObject = $childModel::find($relation_id);

        // return if relationship doesn't exist
        if (!$parentObject->$relation()->find($childObject->id)) {
            return [
                'body' => [
                    'message' => "Relationship is already detached."
                ],
                'code' => 200
            ];
        }

        $verb = $this->getRelationshipVerb($parentObject, $relation, 'disconnect');

        $message = null;
        if ($verb == 'destroy') {
            try {
                $detached = $childModel::$verb($childObject->id);
            } catch (QueryException $e) {
                $message = $e->getResponse();
            }
        } else {
            try {
                $detached = $parentObject->$relation()->$verb($childObject);
            } catch (QueryException $e) {
                $message = $e->getResponse();
            }
        }

        // handle response
        $response = [
            'body' => [
                'message' => $detached ? trans('crud.detached', [
                    'parent' => $this->modelName,
                    'child' => $relation
                ]) : $message,
            ],
            'code' => $detached ? 200 : 422
        ];
        if (method_exists($this, 'detachCallback')) {
            return $this->detachCallback($response);
        }
        return $response;
    }
}
