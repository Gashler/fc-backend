<?php

namespace App\Repositories;

use App\Repositories\CommonCrud;
use App\Services\MailService;
use App\Vote;

class VoteRepository extends CommonCrud
{
    public function __construct()
    {
        $this->modelName = 'Vote';
        $this->model = 'App\\' . $this->modelName;
        $this->mailService = new MailService;
    }

    /**
     * Logic to run if a product's vote threshold is met
     */
    public function threshold(\App\Vote $vote)
    {
        $product = \App\Product::find($vote['product_id']);
        $threshold = \App\Setting::where('key', 'product_vote_threshold')->first()->value;
        \Log::info([
            'product' => $product->toArray(),
            'threshold' => $threshold
        ]);
        if ($product->vote_count >= $threshold) {
            $this->mailService->send('product_vote_threshold_met',
                ['product' => $product], [config('site.admin_user_id')]
            );
        }
    }

    /**
     * Create Callback
     */
    public function createCallback(array $response)
    {
        $this->threshold($response['body']['data']);
        return $response;
    }

    /**
     * Update Callback
     */
    public function updateCallback(array $response)
    {
        $this->threshold($response['body']['data']);
        return $response;
    }
}
