<?php

namespace App\Repositories;

use App\Http\Controllers\Auth\AuthController;
use App\Repositories\CommonCrud;
use App\Repositories\SubscriptionRepository;
use App\Transformers\UserTransformer;
use App\User;

class UserRepository extends CommonCrud
{
    public function __construct() {
        $this->modelName = 'User';
        $this->model = 'App\\' . $this->modelName;
        $this->userTrans = new UserTransformer;
        $this->subscriptionRepo = new SubscriptionRepository;
    }

    public function createOverride($params)
    {
        $params = $this->userTrans->toRegisterData($params);
        $response = $this->create($params, null, false, false);
        if ($response['code'] !== 200) {
            return $response;
        }

        return $response;
    }
}
