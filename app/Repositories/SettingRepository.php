<?php

namespace App\Repositories;

use App\Repositories\CommonCrud;
use App\Setting;

class SettingRepository extends CommonCrud
{
    public function __construct()
    {
        $this->modelName = 'Setting';
        $this->model = 'App\\' . $this->modelName;
    }
}
