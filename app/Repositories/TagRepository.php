<?php

namespace App\Repositories;

use App\Repositories\CommonCrud;
use App\Tag;

class TagRepository extends CommonCrud
{
    public function __construct()
    {
        $this->modelName = 'Tag';
        $this->model = 'App\\' . $this->modelName;
    }
}
