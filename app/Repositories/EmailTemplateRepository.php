<?php

namespace App\Repositories;

use App\Repositories\CommonCrud;
use App\EmailTemplate;

class EmailTemplateRepository extends CommonCrud
{
    public function __construct()
    {
        $this->modelName = 'EmailTemplate';
        $this->model = 'App\\' . $this->modelName;
    }
}
