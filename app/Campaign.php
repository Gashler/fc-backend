<?php

namespace App;

use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $fillable = [
        'days',
        'image_id',
        'live',
        'long_description',
        'monetary_goal',
        'monetary_total',
        'short_description',
        'title',
        'user_id',
        'video_id',
    ];

    public static $rules = [
        'days' => 'integer|nullable',
        'short_description' => 'required',
        'long_description' => 'required',
        'title' => 'required',
        'user_id' => 'integer|required'
    ];

    /**
     * Generated attributes to include
     */
    protected $appends = [
        'backers_count',
        'percent_raised',
        'time_left'
    ];

    /**
     * Relationships to be listed in documentation
     */
    public static $relationships = [
        'comments',
        'image',
        'images',
        'links',
        'payments',
        'posts',
        'rewards',
        'subscriptions',
        'user',
        'users',
        'video',
        'videos',
    ];

    /**
    * Relationships
    */

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function image()
    {
        return $this->belongsTo(Image::class);
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function links()
    {
        return $this->morphMany(Link::class, 'linkable');
    }

    public function payments()
    {
        return $this->hasMany(Payment::class)->orderBy('created_at', 'desc');
    }

    public function posts()
    {
        return $this->hasMany(Post::class)->orderBy('created_at', 'desc');
    }

    public function rewards()
    {
        return $this->hasMany(Reward::class)->orderBy('amount', 'ASC');
    }

    public function subscription()
    {
        return $this->hasOne(Subscription::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function video()
    {
        return $this->belongsTo(Link::class, 'video_id');
    }

    public function videos()
    {
        return $this->morphMany(Link::class, 'linkable')->where('key', 'youtube');
    }

    /**
     * Accessors
     */

    public function getBackersCountAttribute()
    {
        return $this->users()->count();
    }

    public function getPercentRaisedAttribute()
    {
        if ($this->monetary_goal == 0) {
            return 0;
        }
        return round(($this->monetary_total / $this->monetary_goal) * 100);
    }

    public function getTimeLeftAttribute()
    {
        Carbon::setLocale('en');
        $translator = Carbon::getTranslator();
        $translator->setMessages('en', array(
            'day' => ':count day|:count days',
            'month' => ':count month|:count months'
        ));
        $time = strtotime($this->created_at);
        $timeZone = 'America/Denver';
        $date = \Carbon\Carbon::create(
            date('Y', $time),
            date('m', $time),
            date('d', $time),
            date('h', $time),
            date('m', $time),
            date('s', $time),
            $timeZone
        );
        $date->addDays($this->days);
        $now = date('Y-m-d h:i:s', time());
        return $date->diffForHumans($now, true, false, 2);
    }

    /**
     * Functions
     */
    public function faker()
    {
        $faker = Faker::create();
        $password = \Hash::make(config('site.testing_password'));
        return [
            'live' => 1,
            'long_description' => $faker->paragraph . ' ' . $faker->paragraph,
            'monetary_goal' => rand(100,10000),
            'monetary_total' => rand(0,15000),
            'short_description' => $faker->paragraph,
            'title' => $faker->text($maxNbChars = 70),
            'user_id' => rand(1,10)
        ];
    }
}
