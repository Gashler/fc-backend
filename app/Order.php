<?php

namespace App;

use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'stripe_transfer_group'
    ];

    /**
     * Relationships to be always included (use sparingly)
     */
    protected $with = [
        //
    ];

    /**
     * Validation rules
     */
    public static $rules = [
        'stripe_transfer_group' => 'required|string'
    ];

    /**
     * Relationships to be listed in documentation
     */
    public static $relationships = [
        'merchants'
    ];

    /**
     * Relationships
     */

    public function merchants()
    {
        return $this->belongsToMany(Merchant::class)->withPivot(['amount', 'paid']);
    }

    public function wcOrder() {
        return $this->hasOne(WcOrder::class);
    }

    /**
     * Functions
     */

    public function faker()
    {
        $faker = Faker::create();
        return [
            //
        ];
    }
}
