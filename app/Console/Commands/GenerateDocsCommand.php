<?php

namespace App\Console\Commands;

use App\Http\Controllers\SwaggerController;
use Illuminate\Console\Command;

class GenerateDocsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'si:generate_docs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate App\Http\Controllers\Documentation.php, which includes the annotations for generating Swagger documentation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        SwaggerController::index();
    }
}
