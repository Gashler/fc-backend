<?php

namespace App\Console\Commands;

use App\Services\StripeService;
use Illuminate\Console\Command;

class PayMerchantsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'si:pay_merchants';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Tell Stripe to pay merchants for their recorded sales.';

    protected $stripeService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(StripeService $stripeService) {
        parent::__construct();
        $this->stripeService = $stripeService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        return $this->stripeService->payMerchants();
    }
}
