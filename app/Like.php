<?php

namespace App;

use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'likable_id',
        'likable_type',
        'value'
    ];

    /**
     * Validation rules
     */
    public static $rules = [
        'user_id' => 'integer|required',
        'likable_id' => 'integer|required',
        'likable_type' => 'required|in:App\Product,App\Merchant',
        'value' => 'numeric'
    ];
    
    /**
     * Relationships to be listed in documentation
     */
    public static $relationships = [
        'user',
        'likable'
    ];

    /**
     * Relationships
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function likable()
    {
        return $this->morphTo();
    }

    /**
     * Functions
     */

    public function faker()
    {
        $types = [
            'Merchant',
            'Product'
        ];
        return [
            'user_id' => rand(1, 10),
            'likable_type' => 'App\\'.$types[rand(0, 1)],
            'likable_id' => rand(1, 10),
            'value' => rand(0, 1)
        ];
    }
}
