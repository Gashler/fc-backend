<?php

namespace App;

use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Model;

class PayRate extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'percentage'
    ];

    /**
     * Relationships to be always included (use sparingly)
     */
    protected $with = [
        //
    ];

    /**
     * Validation rules
     */
    public static $rules = [
        'percentage' => 'number'
    ];

    /**
     * Relationships to be listed in documentation
     */
    public static $relationships = [
        'merchants'
    ];

    /**
     * Relationships
     */

    public function merchants()
    {
        return $this->hasMany(Merchant::class);
    }

    /**
     * Functions
     */

    public function faker()
    {
        $faker = Faker::create();
        return [
            //
        ];
    }
}
