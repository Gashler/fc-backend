<?php

namespace App;

use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'accepted',
        'approved',
        'description',
        'discounted_price',
        'merchant_id',
        'name',
        'regular_price',
        'short_description',
        'type',
        'user_id'
    ];

    /**
     * Validation rules
     */
    public static $rules = [
        'approved' => 'boolean',
        'description' => 'string|nullable',
        'discount_price' => 'numeric',
        'merchant_id' => 'integer',
        'name' => 'required|string|max:255',
        'regular_price' => 'numeric|nullable',
        'user_id' => 'integer|required',
    ];

    /**
     * Custom attributes
     */
    protected $appends = [
        'featured_image',
        'price',
        'vote_count'
    ];

    /**
     * Relationships to be always included (use sparingly)
     */
    protected $with = [
        //
    ];

    /**
     * Relationships to be listed in documentation
     */
    public static $relationships = [
        'images',
        'google_places',
        'merchant',
        'user',
        'votes'
    ];

    /**
     * Relationships
     */

     // This is a placeholder (keep to avoid breaking product posts to WooCommerce)
    public function categories()
    {
         return $this->belongsToMany(Category::class);
    }

    public function google_places()
    {
         return $this->belongsToMany(GooglePlace::class);
    }

    public function images()
    {
        return $this->belongsToMany(Image::class);
    }

    public function merchant()
    {
        return $this->belongsTo(Merchant::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function votes()
    {
        return $this->hasMany(Vote::class);
    }

    /**
     * Attributes
     */

    public function getFeaturedImageAttribute()
    {
        return $this->images()->first();
    }

    public function getPriceAttribute()
    {
        return $this->discounted_price ?? $this->regular_price;
    }

    public function getVoteCountAttribute()
    {
        return $this->votes()->count();
    }

    /**
     * Functions
     */

    public function faker()
    {
        $faker = Faker::create();
        return [
            'description' => $faker->text,
            'name' => $faker->sentence,
            'merchant_id' => rand(1,10),
            'user_id' => rand(1,10),
            'regular_price' => $faker->randomFloat($nbMaxDecimals = null, $min = 5, $max = 100)
        ];
    }
}
