<?php

namespace App;

use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //
    ];

    /**
     * Validation rules
     */
    public static $rules = [
        //
    ];

    /**
     * Relationships to be listed in documentation
     */
    public static $relationships = [
        //
    ];

    /**
     * Relationships
     */

    public function faker()
    {
        $faker = Faker::create();
        return [
            //
        ];
    }
}
