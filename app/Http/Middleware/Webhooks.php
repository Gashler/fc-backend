<?php
/**
 * Created by PhpStorm.
 * User: adam
 * Date: 26.01.18
 * Time: 11:57
 */

namespace App\Http\Middleware;

use Closure;

class Webhooks
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->header('x-wc-webhook-signature') === config('site.wc-webhook-signature')) {
            return $next($request);
        } else {
            return response()->json(
                [
                    'message' => 'Unauthorized.'
                ], 401
            );
        }
    }
}