<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrderWebhookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|unique:wc_orders',
            'transaction_id' => 'required|unique:wc_orders',
            'order_key' => 'required',
            'status' => 'required',
            'payment_method' => 'required',
            'customer_id' => 'required',
            'currency' => 'required',
            'total' => 'required',
            'total_tax' => 'required',
            'shipping_tax' => 'required',
            'shipping_total' => 'required',
            'prices_include_tax' => 'required'
        ];
    }
}
