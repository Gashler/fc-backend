<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateOrderWebhookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:wc_orders',
            'transaction_id' => 'required|exists:wc_orders',
            'status' => 'required',
            'payment_method' => 'required',
            'currency' => 'required',
            'total' => 'required',
            'total_tax' => 'required',
            'shipping_tax' => 'required',
            'shipping_total' => 'required',
            'prices_include_tax' => 'required'
        ];
    }
}
