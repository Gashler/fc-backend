<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Repositories\CampaignRepository;

class CampaignController extends Controller
{
    protected $repo;

    public function __construct(CampaignRepository $repo)
    {
        $this->modelName = 'Campaign';
        $this->model = 'App\\' . $this->modelName;
        $this->repo = $repo;
    }

    public function view($id)
    {
        $object = \App\Campaign::with([
            'comments',
            'image',
            'images',
            'links',
            'payments',
            'payments.user',
            'posts',
            'rewards',
            'user',
            'video'
        ])->find($id);
        if (auth()->check()) {
            $user = auth()->user();
        }
        return view('campaigns.show', compact('object', 'user'));
    }
}
