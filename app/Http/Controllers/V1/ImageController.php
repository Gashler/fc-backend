<?php

namespace App\Http\Controllers\V1;

use App\Repositories\ImageRepository;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class ImageController extends Controller
{
    protected $repo;

    public function __construct(ImageRepository $repo)
    {
        $this->modelName = 'Image';
        $this->model = 'App\\' . $this->modelName;
        $this->repo = $repo;
    }

    public function view(int $year, string $month, string $file)
    {
        $params = request()->all();
        $params['width'] = $params['width'] ?? null;
        $params['height'] = $params['height'] ?? null;
        if (isset($params['width']) || isset($params['height'])) {
            $img = Image::make(public_path() . '/uploads/' . $year . '/' . $month . '/' . $file)
                ->orientate()
                ->fit($params['width'], $params['height'])
            ;
        } else {
            $img = Image::make(public_path() . '/img/' . $image->url)
                ->orientate()
            ;
        }
        return $img->response();
    }
}
