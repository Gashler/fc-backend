<?php

namespace App\Http\Controllers\V1\WC;

use App\Services\Marketplace\OrderService;

class OrderController extends Controller
{
    protected $service;

    public function __construct(OrderService $service)
    {
        $this->serviceName = 'Order';
        $this->service = 'App\Services\Marketplace\\' . $this->serviceName;
        $this->service = $service;
    }
}
