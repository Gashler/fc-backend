<?php

namespace App\Http\Controllers\V1\WC;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * index
     */
    public function index()
    {
        return $this->service->request('get', request()->all());
    }

    /**
     * store
     */
    public function store(int $id = null)
    {
        $response = $this->service->request('post', request()->all());
        return response()->json($response['body'], $response['code']);
    }

    /**
     * show
     */
    public function show($id)
    {
        $response = $this->service->show($id, request()->all());
        return response()->json($response['body'], $response['code']);
    }

    /**
     * update
     */
    public function update($id, $params = null)
    {
        $response = $this->service->update($id, $params);
        return response()->json($response['body'], $response['code']);
    }

    /**
     * destroy
     */
    public function destroy($id)
    {
        $response = $this->service->destroy($id);
        return response()->json($response['body'], $response['code']);
    }
}
