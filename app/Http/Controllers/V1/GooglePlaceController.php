<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Repositories\VoteRepository;

class GooglePlaceController extends Controller
{
    protected $repo;

    public function __construct(VoteRepository $repo)
    {
        $this->modelName = 'Vote';
        $this->model = 'App\\' . $this->modelName;
        $this->repo = $repo;
    }
}
