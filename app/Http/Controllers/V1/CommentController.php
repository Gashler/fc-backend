<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Repositories\CommentRepository;

class CommentController extends Controller
{
    protected $repo;

    public function __construct(CommentRepository $repo)
    {
        $this->modelName = 'Comment';
        $this->model = 'App\\' . $this->modelName;
        $this->repo = $repo;
    }
}
