<?php

namespace App\Http\Controllers\V1;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        if ($arguments = func_get_args()) {
            foreach ($arguments[0] as $key => $value) {
                $this->$key = $value;
            }
        }
    }

    /**
     * index
     */
    public function index()
    {
        return $this->repo->index(request()->all());
    }

    /**
     * store
     */
    public function store(int $id = null)
    {
        // decode values
        $params = request()->all();

        foreach ($params as $key => $value) {
            if (!is_object($value)) {
                if (isset($value[0]) && $value[0] == '{') {
                    $params[$key] = json_decode($value, true);
                }
            }
        }

        // optionally check to see if this record already exists
        if (isset($params['unique'])) {
            if ($object = $this->model::where(
                $params['unique'], $params[$params['unique']]
            )->first()) {

                // if it does exist, update it instead
                return $this->update($object->id, $params);
            }
        }

        // store
        $response = $this->repo->create($params);
        return response()->json($response['body'], $response['code']);
    }

    /**
     * show
     */
    public function show($id)
    {
        $response = $this->repo->show($id, request()->all());
        return response()->json($response['body'], $response['code']);
    }

    /**
     * update
     */
    public function update($id, $params = null)
    {
        // check permissions
        if (!auth()->user()->hasRole(['admin'])) {
            $object = $this->model::find($id);
            if (get_class($object) == 'App\User' && $object->id == auth()->user()->id) {
                //
            } elseif (!isset($object->user_id) || $object->user_id !== auth()->user()->id) {
                return response()->json([
                    'message' => trans('crud.unauthorized', ['model' => snake_case($this->modelName)])
                ], 401);
            }
        }

        if (!isset($params)) {
            $params = request()->all();
        }

        // decode values
        foreach ($params as $key => $value) {
            if (isset($value[0]) && $value[0] == '{') {
                $params[$key] = json_decode($value, true);
            }
        }

        // modify validation rules for updating
        foreach ($this->model::$rules as $index => $rule) {
            if (strpos($rule, 'email') !== false) {
                $rule = 'required|email|unique:users,email,' . $id;
            }
            $rules[$index] = $rule . "|sometimes";
        }

        $response = $this->repo->update($id, $params);
        return response()->json($response['body'], $response['code']);
    }

    /**
     * destroy
     */
    public function destroy($id)
    {
        // check permissions
        if (!auth()->user()->hasRole(['admin'])) {
            $object = $this->model::find($id);
            if (!isset($object->user_id) || $object->user_id !== auth()->user()->id) {
                return response()->json([
                    'message' => trans('crud.unauthorized', ['model' => snake_case($this->modelName)])
                ], 401);
            }
        }

        // delete record
        $response = $this->repo->destroy($id);
        return response()->json($response['body'], $response['code']);
    }

    /**
     * Attach
     */
    public function attach($id, $relationship, $relationship_id)
    {
        $response = $this->repo->connection($id, $relationship, $relationship_id, 'attach');
        return response()->json($response['body'], $response['code']);
    }

    /**
     * Detach
     */
    public function detach($id, $relationship, $relationship_id)
    {
        $response = $this->repo->connection($id, $relationship, $relationship_id, 'detach');
        return response()->json($response['body'], $response['code']);
    }
}
