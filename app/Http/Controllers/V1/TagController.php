<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Repositories\TagRepository;

class TagController extends Controller
{
    protected $repo;

    public function __construct(TagRepository $repo)
    {
        $this->modelName = 'Tag';
        $this->model = 'App\\' . $this->modelName;
        $this->repo = $repo;
    }
}
