<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Repositories\LikeRepository;

class LikeController extends Controller
{
    protected $repo;

    public function __construct(LikeRepository $repo)
    {
        $this->modelName = 'Like';
        $this->model = 'App\\' . $this->modelName;
        $this->repo = $repo;
    }
}
