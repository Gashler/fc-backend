<?php

namespace App\Http\Controllers\V1;

use App\Payment;
use Illuminate\Http\Request;
use App\Repositories\PaymentRepository;

class PaymentController extends Controller
{
    protected $repo;

    public function __construct(PaymentRepository $repo)
    {
        $this->modelName = 'Payment';
        $this->model = 'App\\' . $this->modelName;
        $this->repo = $repo;
    }

    public function create(int $campaign_id, int $reward_id)
    {
        return $this->repo->createForm($campaign_id, $reward_id);
    }
}
