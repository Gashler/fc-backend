<?php

namespace App\Http\Controllers\V1;

use App\User;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;

class UserController extends Controller
{
    protected $repo;

    public function __construct(UserRepository $repo)
    {
        $this->modelName = 'User';
        $this->model = 'App\\' . $this->modelName;
        $this->repo = $repo;
    }

    /**
     * Note:: this would be better in the AuthController, but I can't get it to work there.
     */
    public function getAuthUser()
    {
        if (auth()->check()) {
            $user = auth()->user();
            $token = $user->createToken('MyApp')->accessToken;
            return response(['token' => $token, 'user' => $user], 200);
        }
        return response(['message' => 'Unauthenticated'], 401);
    }
}
