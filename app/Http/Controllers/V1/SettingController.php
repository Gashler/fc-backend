<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Repositories\SettingRepository;

class SettingController extends Controller
{
    protected $repo;

    public function __construct(SettingRepository $repo)
    {
        $this->modelName = 'Setting';
        $this->model = 'App\\' . $this->modelName;
        $this->repo = $repo;
    }
}
