<?php

namespace App\Http\Controllers\V1;

use App\Http\Requests\DeleteOrderWebhookRequest;
use App\Http\Requests\StoreOrderWebhookRequest;
use App\Http\Requests\UpdateOrderWebhookRequest;
use App\Repositories\OrderRepository;
use App\Transformers\OrderTransformer;

class OrderController extends Controller
{
    protected $repo;
    protected $orderTrans;

    public function __construct(
        OrderTransformer $orderTrans,
        OrderRepository $repo
    )
    {
        $this->modelName = 'Order';
        $this->model = 'App\\' . $this->modelName;
        $this->repo = $repo;
        $this->orderTrans = $orderTrans;
    }

    public function storeWebhookOrder(StoreOrderWebhookRequest $request)
    {
        return response()->json($this->repo->storeWebhookOrder($this->orderTrans->toNewWcOrder($request->all())));
    }

    public function updateWebHookOrder(UpdateOrderWebhookRequest $request)
    {
        return response()->json($this->repo->storeWebhookOrder($this->orderTrans->toWcOrder($request->all())));
    }

    public function deleteWebHookOrder(DeleteOrderWebhookRequest $request)
    {
        return response()->json($this->repo->destroyWebhookOrder($request->id));
    }

}
