<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Repositories\PostRepository;

class PostController extends Controller
{
    protected $repo;

    public function __construct(PostRepository $repo)
    {
        $this->modelName = 'Post';
        $this->model = 'App\\' . $this->modelName;
        $this->repo = $repo;
    }

    public function view($id)
    {
        $object = \App\Post::with([
            'campaign',
            'campaign.user',
            'image',
            'images',
        ])->find($id);
        return view('posts.show', compact('object'));
    }
}
