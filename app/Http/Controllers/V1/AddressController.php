<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Repositories\AddressRepository;

class AddressController extends Controller
{
    protected $repo;

    public function __construct(AddressRepository $repo)
    {
        $this->modelName = 'Address';
        $this->model = 'App\\' . $this->modelName;
        $this->repo = $repo;
    }
}
