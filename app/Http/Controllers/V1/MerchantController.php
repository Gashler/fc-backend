<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Repositories\MerchantRepository;

class MerchantController extends Controller
{
    protected $repo;

    public function __construct(MerchantRepository $repo)
    {
        $this->modelName = 'Merchant';
        $this->model = 'App\\' . $this->modelName;
        $this->repo = $repo;
    }
}
