<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Repositories\UserStatRepository;

class UserStatController extends Controller
{
    protected $repo;

    public function __construct(UserStatRepository $repo)
    {
        $this->modelName = 'UserStat';
        $this->model = 'App\\' . $this->modelName;
        $this->repo = $repo;
    }
}
