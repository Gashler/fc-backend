<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Repositories\EmailTemplateRepository;

class EmailTemplateController extends Controller
{
    protected $repo;

    public function __construct(EmailTemplateRepository $repo)
    {
        $this->modelName = 'EmailTemplate';
        $this->model = 'App\\' . $this->modelName;
        $this->repo = $repo;
    }
}
