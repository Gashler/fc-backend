<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Repositories\PayRateRepository;

class PayRateController extends Controller
{
    protected $repo;

    public function __construct(PayRateRepository $repo)
    {
        $this->modelName = 'PayRate';
        $this->model = 'App\\' . $this->modelName;
        $this->repo = $repo;
    }
}
