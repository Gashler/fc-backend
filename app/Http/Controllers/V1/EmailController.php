<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Repositories\EmailRepository;
use App\Services\MailService;

class EmailController extends Controller
{
    protected $repo;

    public function __construct(
        EmailRepository $repo,
        MailService $mailService
    ) {
        $this->modelName = 'Email';
        $this->model = 'App\\' . $this->modelName;
        $this->repo = $repo;
        $this->mailService = $mailService;
    }

    /**
     * index
     */
    public function index()
    {
        $emails = $this->repo->index(request()->all());
        foreach ($emails as $index => $email) {
            unset($emails[$index]->body);
        }
        return $emails;
    }

    /**
     * @SWG\Post(path="/send-email", summary="Send an email to users", tags={"Email"},
     * @SWG\Parameter(name="email_key", description="The key of the email to send (if not specifying an ID). For a list of available emails, access the `GET /emails` endpoint.", in="body", schema="", required=false, type="string"),
     * @SWG\Parameter(name="email_id", description="The ID of the email to send (if not specifying a key).", in="body", schema="", required=false, type="string"),
     * @SWG\Parameter(name="user_ids", description="ID's of the users to send to. If both the `user_ids` and the `emails` fields are left blank, the email will be sent to the authenticated user.", in="body", schema="", required=false, type="array"),
     * @SWG\Parameter(name="emails", description="Emails of the recipients to send to.", in="body", schema="", required=false, type="array"),
     * @SWG\Parameter(name="subject", description="Optionally overwrite the email's subject.", in="body", schema="", required=false, type="string"),
     * @SWG\Parameter(name="body", description="Optionally append text to the body of the email.", in="body", schema="", required=false, type="text"),
     * @SWG\Response(response=200, description="")
     * )
     */
    public function send()
    {
        // basic validation
        $params = request()->all();
        if (!isset($params['email_key']) && !isset($params['email_id'])) {
            return response()->json([
                'message' => "You must specify either an `email_key` or an `email_id`."
            ], 400);
        }

        // prepare data
        $params['email_key'] = $params['email_key'] ?? null;
        $params['email_id'] = $params['email_id'] ?? null;
        $params['user_ids'] = $params['user_ids'] ?? [];
        $params['emails'] = $params['emails'] ?? [];
        $params['custom_subject'] = $params['subject'] ?? null;
        $params['custom_body'] = $params['body'] ?? null;

        // send data to MailService
        $response = $this->mailService->send(
            $params['email_key'],
            $params['email_id'],
            $params['user_ids'],
            $params['emails'],
            $params['custom_subject'],
            $params['custom_body']
        );
        return response()->json($response['body'], $response['code']);
    }

    /**
     * Test the display of an email template
     */
    public function test(string $key = null)
    {
        if (isset($key)) {
            $email = \App\Email::where('key', $key)->first();
        } else {
            $email = \App\Email::all()->last();
        }
        $user = auth()->user();
        $data = [\App\Product::all()->last()];
        $custom_body = null;
        return view('emails.default', compact('email', 'user', 'data', 'custom_body'));
    }
}
