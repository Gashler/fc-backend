<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Repositories\ProductRepository;
use App\Services\Marketplace\ProductService;

class ProductController extends Controller
{
    protected $repo;

    public function __construct(
        ProductRepository $repo,
        ProductService $productService
    ) {
        $this->modelName = 'Product';
        $this->model = 'App\\' . $this->modelName;
        $this->repo = $repo;
        $this->productService = $productService;
    }

    /**
     * Transfer a product to marketplace
     */
    public function transfer($id)
    {
        $response = $this->productService->transfer($id);
        return response()->json([
            $response['body'],
            $response['code']
        ]);
    }
}
