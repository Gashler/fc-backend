<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\PasswordReset;
use App\Http\Requests\ConfirmEmailRequest;
use App\Http\Requests\RequestEmailRequest;
use App\Repositories\UserRepository;
use App\Services\MailService;
use App\Services\Marketplace\AuthService;
use App\User;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling the password process.
    |
    */

    public function __construct(
        AuthService $authService,
        MailService $mailService,
        UserRepository $userRepo
    ) {
        $this->authService = $authService;
        $this->mailService = $mailService;
        $this->userRepo = $userRepo;
    }

    /**
     * @SWG\Get(path="/password/forgot", summary="Return the view for requesting a password reset", tags={"Authentication"},
     * @SWG\Response(response=200, description="Forgot password form")
     * )
     */
    public function forgot()
    {
        return view('auth.passwords.forgot');
    }

    public function request(RequestEmailRequest $request)
    {
        $email = $request->email;
        $token = str_random(191);
        PasswordReset::create(['email' => $email, 'token' => $token]);
        if (!$user = User::where('email', $email)->first()) {
            return redirect()->back()->with([
                'message' => [
                    'type' => 'danger',
                    'body' => "No user was found with this email address."
                ]
            ]);
        }
        $response = $this->mailService->send(
            'password_reset', ['token' => $token], [$user->id]
        );
        if ($response['code'] !== 200) {
            return response()->json($resopnse['body'], $response['code']);
        }
        return redirect()->back()->with([
            'complete' => true,
            'message' => [
                'type' => 'success',
                'body' => "A confirmation email has been sent to $email."
            ]
        ]);
    }

    public function confirm(ConfirmEmailRequest $request)
    {
        // verify token
        if (!$passwordReset = \App\PasswordReset::where([
            'email' => $request->email,
            'token' => $request->token
        ])->first()) {
            return redirect()->back()->with([
                'message' => [
                    'type' => 'danger',
                    'body' => "Incorrect email address or token. Please try again."
                ]
            ]);
        }

        // if token has expired
        if (date('Y-m-d h:i:s', strtotime($passwordReset->created_at)) < date('Y-m-d h:i:s', strtotime('1 hour ago'))) {
            return redirect()->back()->with([
                'message' => [
                    'type' => 'danger',
                    'body' => "Your password reset token has expired. Please try again."
                ]
            ]);
        }

        // log in user
        $user = User::where('email', $passwordReset->email)->first();
        if (!$user = auth()->loginUsingId($user->id))
        {
            throw new Exception('Error logging in');
        }

        // delete password reset(s)
        PasswordReset::where('email', $request->email)->delete();

        return redirect()->route('password.reset');
    }

    public function reset()
    {
        return view('auth.passwords.reset');
    }

    public function update()
    {
        $response = $this->userRepo->update(auth()->user()->id, request()->all());
        if ($response['code'] !== 200) {
            return redirect()->back()->with([
                'message' => [
                    'type' => 'danger',
                    'body' => $response['body']
                ]
            ]);
        }
        return redirect()->route('password.complete');
    }

    public function complete()
    {
        $message = [
            'type' => 'success',
            'body' => "Your password has been reset. Try logging in again."
        ];
        return view('auth.passwords.complete', compact('message'));
    }
}
