<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\V1\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterUserRequest;
use App\Repositories\SubscriptionRepository;
use App\Repositories\UserRepository;
use App\Services\MailService;
use App\Transformers\UserTransformer;

class AuthController extends Controller
{
    protected $tags = array();
    protected $userRepo;
    protected $provider;
    protected $userTrans;

    public function __construct(
        MailService $mailService,
        SubscriptionRepository $subscriptionRepo,
        UserRepository $userRepo,
        UserTransformer $userTrans
    ) {
        $this->mailService = $mailService;
        $this->subscriptionRepo = $subscriptionRepo;
        $this->userRepo = $userRepo;
        $this->userTrans = $userTrans;
    }

    /**
     * @SWG\Post(path="/register", summary="Register a user", tags={"Authentication"},
     * @SWG\Parameter(schema="", name="email", description="User's email address", in="body", required=true, type="string"),
     * @SWG\Parameter(schema="", name="password", description="User's password", in="body", required=true, type="string"),
     * @SWG\Parameter(schema="", name="password_c", description="Password confirmation", in="body", required=false, type="string"),
     * @SWG\Parameter(schema="", name="username", description="A unique username", in="body", required=false, type="string"),
     * @SWG\Response(response=200, description="Authentication token, user object, and Marketplace session ID")
     * )
     */
    public function register()
    {
        $params = request()->all();
        $response = $this->userRepo->create($params);
        if ($response['code'] !== 200) {
            return response($response['body'], $response['code']);
        }
        $user = $response['body']['data'];
        $response = $this->userRepo->show($user->id, ['subscriptions']);
        $user = $response['body'];
        $token = $user->createToken('Auth token')->accessToken;
        $user->save();

        // attach user to "creators" subscription
        $subscription = $this->subscriptionRepo->findBy('key', 'creators');
        $user->subscriptions()->attach($subscription->id);

        // login newly-registered user
        if (!auth()->attempt([
            'email' => $params['email'],
            'password' => $params['password']
        ])) {
            return response([
                'message' => 'Error logging in new user.'
            ], 500);
        }

        // send emails
        // $this->mailService->send('user_registered_user', ['user' => auth()->user()]);
        // $this->mailService->send('user_registered_admin',
        //     ['user' => auth()->user()], [config('site.admin_user_id')]
        // );

        return response([
            'message' => $response['body']['message'],
            'user' => $user,
            'token' => $token
        ], 200);
    }

    /**
     * @SWG\Post(path="/login", summary="Log in a user", tags={"Authentication"},
     * @SWG\Parameter(schema="", name="email", description="User's email address", in="body", required=true, type="string"),
     * @SWG\Parameter(schema="", name="password", description="User's password", in="body", required=true, type="string"),
     * @SWG\Response(response=200, description="Authentication token, user object")
     * )
     */
    public function login()
    {
        $params = request()->all();
        if (auth()->attempt($params)) {
            $user = $this->userRepo->find(auth()->user()->id, ['subscriptions']);
            $token = $user->createToken('MyApp')->accessToken;
            return response(['token' => $token, 'user' => $user,], 200);
        } else {
            return response(['message' => 'Incorrect email or password'], 401);
        }
    }

    /**
     * @SWG\Post(path="/login-as-user", summary="Log in as a user", tags={"Authentication"},
     * @SWG\Parameter(schema="", name="id", description="User's ID", in="request", required=true, type="string"),
     * @SWG\Response(response=200, description="Authentication token, user object")
     * )
     */
    public function loginAsUser($id)
    {
        if (auth()->check() && auth()->user()->hasRole(['admin'])) {
            if (!$user = $this->userRepo->find($id)) {
                return response(['message' => 'User not found.'], 400);
            }
            $token = $user->createToken('MyApp')->accessToken;
            return response(['token' => $token, 'user' => $user,], 200);
        }
    }
}
