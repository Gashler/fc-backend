<?php

namespace App\Http\Controllers;

use App\Http\Controllers\V1\Controller;

/**
 * @SWG\Swagger(
 *     host=DEFAULT_API_URL,
 *     basePath="/",
 * @SWG\Info(
 *         version="1",
 *         title=DOCUMENTATION_NAME,
 * @SWG\Contact(
 *             email="steve@spendindie.com"
 *         )
 *     )
 * )
 *     securityDefinitions(
 *          Bearer(
 *              type="apiKey"
 *              name="Authorization"
 *              in="header"
 *         )
 *     )
 */

 /**
 * @SWG\Tag(
 *   name="Authentication",
 *   description="",
 * )
 * @SWG\Tag(
 *   name="Address",
 *   description="**Admin access only**",
 * )
 * @SWG\Tag(
 *   name="Image",
 *   description="**Admin access only**",
 * )
 * @SWG\Tag(
 *   name="Merchant",
 *   description="**Admin access only**",
 * )
 * @SWG\Tag(
 *   name="Order",
 *   description="**Admin access only**",
 * )
 * @SWG\Tag(
 *   name="PayRate",
 *   description="**Admin access only**",
 * )
 * @SWG\Tag(
 *   name="Product",
 *   description="**Partial admin access _(update, delete, attach, detach)_**",
 * )
 * @SWG\Tag(
 *   name="Tag",
 *   description="**Admin access only**",
 * )
 * @SWG\Tag(
 *   name="User",
 *   description="**Admin access only**",
 * )
 * @SWG\Tag(
 *   name="Vote",
 *   description="",
 * )
 * @SWG\Tag(
 *   name="Stripe",
 *   description="**Admin access only**",
 * )
 */

class SwaggerController
{
    /**
     * Generate annotations for Swaggeer documentation
     */
    public static function index()
    {
        // define models and paths
        $files = scandir(app_path());
        $modelExceptions = [
            'PasswordReset',
            'Role',
            'StripeAccount'
        ];
        $publicRoutes = [
            [
                'model_name' => 'Product',
                'method_name' => 'Index'
            ],
            [
                'model_name' => 'Product',
                'method_name' => 'Show'
            ],
        ];

        $models = [];
        foreach ($files as $index => $file) {
            $name = str_replace('.php', '', $file);
            if (in_array($name, $modelExceptions) || $file == '.' || $file == '..') {
                continue;
            }
            if (!is_dir(app_path() . '/' . $file)) {
                $models[] = [
                    'name' => $name,
                    'namespace' => 'App\\' . $name,
                    'path' => '/' . str_plural(kebab_case($name)),
                ];
            }
        }

        foreach ($models as $index => $model) {

            // define model's parameters
            $params = [];
            foreach ($model['namespace']::$rules as $key => $rule) {
                $param = [
                    'name' => $key,
                    'required' => 'false',
                    'type' => 'string'
                ];
                if (
                    strpos($rule, 'required') !== false
                    && strpos($rule, 'sometimes') === false
                ) {
                    $param['required'] = 'true';
                }
                $types = [
                    'boolean',
                    'integer',
                    'numeric',
                ];
                foreach ($types as $type) {
                    if (strpos($rule, $type) !== false) {
                        if ($type == 'numeric') {
                            $type = 'number';
                        }
                        $param['type'] = $type;
                    }
                }
                $params[] = $param;
            }
            $models[$index]['params'] = $params;

            // define model's relationships
            $relationships = '';
            foreach ($model['namespace']::$relationships as $key => $relationship) {
                if ($relationships == '') {
                    $relationships = "'" . $relationship . "'";
                } else {
                    $relationships .= ", '" . $relationship . "'";
                }
            }
            $models[$index]['relations'] = $relationships;
        }

        // define methods
        $methods = [
            [
                'http' => 'Get',
                'name' => 'Index',
                'path' => '',
                'summary' => 'Get a list of ',
                'response' => 'A list of '
            ],
            [
                'http' => 'Get',
                'name' => 'Show',
                'path' => '/{id}',
                'summary' => 'Get the specified ',
                'response' => 'The specified '
            ],
            [
                'http' => 'Post',
                'name' => 'Store',
                'path' => '',
                'summary' => 'Create a new ',
                'response' => 'The new '
            ],
            [
                'http' => 'Patch',
                'name' => 'Update',
                'path' => '/{id}',
                'summary' => 'Update the specified ',
                'response' => 'The updated '
            ],
            [
                'http' => 'Delete',
                'name' => 'Destroy',
                'path' => '/{id}',
                'summary' => 'Delete the specified ',
                'response' => ''
            ],
            [
                'http' => 'POST',
                'name' => 'Attach',
                'path' => '/{id}/{relationship}/{relationship_id}',
                'summary' => 'Attach a relationship to the specified ',
                'response' => ''
            ],
            [
                'http' => 'Delete',
                'name' => 'Detach',
                'path' => '/{id}/{relationship}/{relationship_id}',
                'summary' => 'Detach a relationship from the specified ',
                'response' => ''
            ],
        ];

        // generate annotations
        $txt = '<?php';
        foreach ($models as $model) {
            foreach ($methods as $method) {

                // combine model and method verbiage
                if ($method['name'] == 'Index') {
                    $method['summary'] .= str_plural(strtolower($model['name']));
                    $method['response'] .= str_plural(strtolower($model['name']));
                } elseif ($method['name'] == 'Destroy') {
                    $method['summary'] .= strtolower($model['name']);
                    $method['response'] .= '';
                } else {
                    $method['summary'] .= strtolower($model['name']);
                    $method['response'] .= strtolower($model['name']);
                }

                // begin annotation
                $txt .= '
                    /**
                    * @SWG\\' . $method['http'] . '(
                    *   path="/v1' . $model['path'] . $method['path'] . '",
                    *   summary="' . $method['summary'] . '",
                    *   tags={"' . $model['name'] . '"},';

                // determine if the current route is public or private
                $publicRoute = false;
                foreach ($publicRoutes as $pr) {
                    if (
                        $pr['model_name'] == $model['name']
                        && $pr['method_name'] == $method['name']
                    ) {
                        $publicRoute = true;
                    }
                }

                // if private, require a token
                if (!$publicRoute) {
                    $txt .= '
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),';
                }

                // Show
                if ($method['name'] == 'Show' || $method['name'] == 'Index') {
                    $txt .= '
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: ' . $model['relations'] . '. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),';
                }

                // Index
                if ($method['name'] == 'Index') {
                    $txt .= '
                    *   @SWG\Parameter(name="where", in="query", required=false, type="string", description="Comma-separated list of conditions. Examples:
                    (1) `?where=user_id~123` (use ~ instead of =), (2) `?where=created_at>=' . date('Y') . '-01-01,created_at<=' . date('Y') . '-12-31`"),
                    *   @SWG\Parameter(name="page", in="query", required=false, type="number", description="Tells the paginator what page to return"),
                    *   @SWG\Parameter(name="per_page", in="query", required=false, type="number", description="The number of results to include per page"),
                    *   @SWG\Parameter(name="limit", in="query", required=false, type="number", description="The number of items to return. This will disable pagination. Use \'limit=0\' for no limit."),';
                }

                // Show, Update, Delete
                if (($method['name'] == 'Show') || $method['name'] == 'Update' || $method['name'] == 'Destroy') {
                    $txt .= '
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),';
                }

                // Store
                if ($method['name'] == 'Store') {
                    foreach ($model['params'] as $param) {
                        $description = '';
                        if ($param['name'] == 'files') {
                            $description = 'Multipart form data';
                        }
                        $txt .= '
                    *   @SWG\Parameter(schema={"type": "' . $param['type'] . '"}, name="' . $param['name'] . '", in="body", required=' . $param['required'] . ', type="' . $param['type'] . '", description="' . $description . '"),';
                    }
                    $txt .= '
                    *   @SWG\Parameter(schema="", name="unique", in="body", required=false, type="string", description="The name of another parameter in the request. If the API finds a record with the specified parameter\'s property and value, it will update this recrord instead of creating a new record. For example, the following will either create a new merchant or, if a merchant with the specified `google_place_id` is found, it will update this merchant instead: `merchant[name] = \'Swimwear Unlimited\', google_place_id = \'abc123\', unique = \'google_place_id\'`"),';
                }

                // Store, Update
                if ($method['name'] == 'Store' || $method['name'] == 'Update') {
                    foreach ($model['params'] as $param) {
                        $description = '';
                        if ($param['name'] == 'files') {
                            $description = 'Multipart form data';
                        }
                        $txt .= '
                    *   @SWG\Parameter(schema={"type": "' . $param['type'] . '"}, name="' . $param['name'] . '", in="body", required=' . $param['required'] . ', type="' . $param['type'] . '", description="' . $description . '"),';
                    }
                    $txt .= '
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: ' . $model['relations'] . '. Example: `merchant[name] = \'Swimwear Unlimited\', merchant[tags][0][name] = \'Clothing\', merchant[tags][1][name] = \'Swimwear\', products[0][name] = \'Wetsuit\', products[0][images][files] = \'{multipart form data}\'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = \'Wetsuit\', products[1][id] = 123, products[1][name] = \'Goggles\'` The first related product will be created. The second related product will be updated."),';
                }

                // Update
                if ($method['name'] == 'Update') {
                    foreach ($model['params'] as $param) {
                        $txt .= '
                    *   @SWG\Parameter(schema="", name="' . $param['name'] . '", in="body", required=false, type="' . $param['type'] . '"),';
                    }
                }

                // Attach / Detach
                if ($method['name'] == 'Attach' || $method['name'] == 'Detach') {
                    $txt .= '
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: ' . $model['relations'] . '"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),';
                }
                $txt .= '
                    *   @SWG\Response(response=200, description="' . $method['response'] . '")
                    * )
                    */
                ';
            }
        }

        // write file
        $file = fopen(app_path() . "/Http/Controllers/Documentation.php", "w") or die("Unable to open file!");
        fwrite($file, $txt);
        fclose($file);
    }
}
