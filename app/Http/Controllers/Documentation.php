<?php
                    /**
                    * @SWG\Get(
                    *   path="/v1/addresses",
                    *   summary="Get a list of addresses",
                    *   tags={"Address"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: 'addressable'. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="where", in="query", required=false, type="string", description="Comma-separated list of conditions. Examples:
                    (1) `?where=user_id~123` (use ~ instead of =), (2) `?where=created_at>=2018-01-01,created_at<=2018-12-31`"),
                    *   @SWG\Parameter(name="page", in="query", required=false, type="number", description="Tells the paginator what page to return"),
                    *   @SWG\Parameter(name="per_page", in="query", required=false, type="number", description="The number of results to include per page"),
                    *   @SWG\Parameter(name="limit", in="query", required=false, type="number", description="The number of items to return. This will disable pagination. Use 'limit=0' for no limit."),
                    *   @SWG\Response(response=200, description="A list of addresses")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/addresses/{id}",
                    *   summary="Get the specified address",
                    *   tags={"Address"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: 'addressable'. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="The specified address")
                    * )
                    */
                
                    /**
                    * @SWG\Post(
                    *   path="/v1/addresses",
                    *   summary="Create a new address",
                    *   tags={"Address"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(schema={"type": "string"}, name="line_1", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="line_2", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="city", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="state", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="zip", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema="", name="unique", in="body", required=false, type="string", description="The name of another parameter in the request. If the API finds a record with the specified parameter's property and value, it will update this recrord instead of creating a new record. For example, the following will either create a new merchant or, if a merchant with the specified `google_place_id` is found, it will update this merchant instead: `merchant[name] = 'Swimwear Unlimited', google_place_id = 'abc123', unique = 'google_place_id'`"),
                    *   @SWG\Parameter(schema={"type": "string"}, name="line_1", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="line_2", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="city", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="state", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="zip", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: 'addressable'. Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Response(response=200, description="The new address")
                    * )
                    */
                
                    /**
                    * @SWG\Patch(
                    *   path="/v1/addresses/{id}",
                    *   summary="Update the specified address",
                    *   tags={"Address"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Parameter(schema={"type": "string"}, name="line_1", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="line_2", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="city", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="state", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="zip", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: 'addressable'. Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Parameter(schema="", name="line_1", in="body", required=false, type="string"),
                    *   @SWG\Parameter(schema="", name="line_2", in="body", required=false, type="string"),
                    *   @SWG\Parameter(schema="", name="city", in="body", required=false, type="string"),
                    *   @SWG\Parameter(schema="", name="state", in="body", required=false, type="string"),
                    *   @SWG\Parameter(schema="", name="zip", in="body", required=false, type="string"),
                    *   @SWG\Response(response=200, description="The updated address")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/addresses/{id}",
                    *   summary="Delete the specified address",
                    *   tags={"Address"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="")
                    * )
                    */
                
                    /**
                    * @SWG\POST(
                    *   path="/v1/addresses/{id}/{relationship}/{relationship_id}",
                    *   summary="Attach a relationship to the specified address",
                    *   tags={"Address"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: 'addressable'"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="address")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/addresses/{id}/{relationship}/{relationship_id}",
                    *   summary="Detach a relationship from the specified address",
                    *   tags={"Address"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: 'addressable'"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="address")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/campaigns",
                    *   summary="Get a list of campaigns",
                    *   tags={"Campaign"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: 'emails', 'users'. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="where", in="query", required=false, type="string", description="Comma-separated list of conditions. Examples:
                    (1) `?where=user_id~123` (use ~ instead of =), (2) `?where=created_at>=2018-01-01,created_at<=2018-12-31`"),
                    *   @SWG\Parameter(name="page", in="query", required=false, type="number", description="Tells the paginator what page to return"),
                    *   @SWG\Parameter(name="per_page", in="query", required=false, type="number", description="The number of results to include per page"),
                    *   @SWG\Parameter(name="limit", in="query", required=false, type="number", description="The number of items to return. This will disable pagination. Use 'limit=0' for no limit."),
                    *   @SWG\Response(response=200, description="A list of campaigns")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/campaigns/{id}",
                    *   summary="Get the specified campaign",
                    *   tags={"Campaign"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: 'emails', 'users'. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="The specified campaign")
                    * )
                    */
                
                    /**
                    * @SWG\Post(
                    *   path="/v1/campaigns",
                    *   summary="Create a new campaign",
                    *   tags={"Campaign"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(schema={"type": "string"}, name="name", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="start_at", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema="", name="unique", in="body", required=false, type="string", description="The name of another parameter in the request. If the API finds a record with the specified parameter's property and value, it will update this recrord instead of creating a new record. For example, the following will either create a new merchant or, if a merchant with the specified `google_place_id` is found, it will update this merchant instead: `merchant[name] = 'Swimwear Unlimited', google_place_id = 'abc123', unique = 'google_place_id'`"),
                    *   @SWG\Parameter(schema={"type": "string"}, name="name", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="start_at", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: 'emails', 'users'. Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Response(response=200, description="The new campaign")
                    * )
                    */
                
                    /**
                    * @SWG\Patch(
                    *   path="/v1/campaigns/{id}",
                    *   summary="Update the specified campaign",
                    *   tags={"Campaign"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Parameter(schema={"type": "string"}, name="name", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="start_at", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: 'emails', 'users'. Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Parameter(schema="", name="name", in="body", required=false, type="string"),
                    *   @SWG\Parameter(schema="", name="start_at", in="body", required=false, type="string"),
                    *   @SWG\Response(response=200, description="The updated campaign")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/campaigns/{id}",
                    *   summary="Delete the specified campaign",
                    *   tags={"Campaign"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="")
                    * )
                    */
                
                    /**
                    * @SWG\POST(
                    *   path="/v1/campaigns/{id}/{relationship}/{relationship_id}",
                    *   summary="Attach a relationship to the specified campaign",
                    *   tags={"Campaign"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: 'emails', 'users'"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="campaign")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/campaigns/{id}/{relationship}/{relationship_id}",
                    *   summary="Detach a relationship from the specified campaign",
                    *   tags={"Campaign"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: 'emails', 'users'"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="campaign")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/categories",
                    *   summary="Get a list of categories",
                    *   tags={"Category"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: . You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="where", in="query", required=false, type="string", description="Comma-separated list of conditions. Examples:
                    (1) `?where=user_id~123` (use ~ instead of =), (2) `?where=created_at>=2018-01-01,created_at<=2018-12-31`"),
                    *   @SWG\Parameter(name="page", in="query", required=false, type="number", description="Tells the paginator what page to return"),
                    *   @SWG\Parameter(name="per_page", in="query", required=false, type="number", description="The number of results to include per page"),
                    *   @SWG\Parameter(name="limit", in="query", required=false, type="number", description="The number of items to return. This will disable pagination. Use 'limit=0' for no limit."),
                    *   @SWG\Response(response=200, description="A list of categories")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/categories/{id}",
                    *   summary="Get the specified category",
                    *   tags={"Category"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: . You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="The specified category")
                    * )
                    */
                
                    /**
                    * @SWG\Post(
                    *   path="/v1/categories",
                    *   summary="Create a new category",
                    *   tags={"Category"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(schema="", name="unique", in="body", required=false, type="string", description="The name of another parameter in the request. If the API finds a record with the specified parameter's property and value, it will update this recrord instead of creating a new record. For example, the following will either create a new merchant or, if a merchant with the specified `google_place_id` is found, it will update this merchant instead: `merchant[name] = 'Swimwear Unlimited', google_place_id = 'abc123', unique = 'google_place_id'`"),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: . Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Response(response=200, description="The new category")
                    * )
                    */
                
                    /**
                    * @SWG\Patch(
                    *   path="/v1/categories/{id}",
                    *   summary="Update the specified category",
                    *   tags={"Category"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: . Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Response(response=200, description="The updated category")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/categories/{id}",
                    *   summary="Delete the specified category",
                    *   tags={"Category"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="")
                    * )
                    */
                
                    /**
                    * @SWG\POST(
                    *   path="/v1/categories/{id}/{relationship}/{relationship_id}",
                    *   summary="Attach a relationship to the specified category",
                    *   tags={"Category"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: "),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="category")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/categories/{id}/{relationship}/{relationship_id}",
                    *   summary="Detach a relationship from the specified category",
                    *   tags={"Category"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: "),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="category")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/emails",
                    *   summary="Get a list of emails",
                    *   tags={"Email"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: 'campaigns', 'templates', 'users'. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="where", in="query", required=false, type="string", description="Comma-separated list of conditions. Examples:
                    (1) `?where=user_id~123` (use ~ instead of =), (2) `?where=created_at>=2018-01-01,created_at<=2018-12-31`"),
                    *   @SWG\Parameter(name="page", in="query", required=false, type="number", description="Tells the paginator what page to return"),
                    *   @SWG\Parameter(name="per_page", in="query", required=false, type="number", description="The number of results to include per page"),
                    *   @SWG\Parameter(name="limit", in="query", required=false, type="number", description="The number of items to return. This will disable pagination. Use 'limit=0' for no limit."),
                    *   @SWG\Response(response=200, description="A list of emails")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/emails/{id}",
                    *   summary="Get the specified email",
                    *   tags={"Email"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: 'campaigns', 'templates', 'users'. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="The specified email")
                    * )
                    */
                
                    /**
                    * @SWG\Post(
                    *   path="/v1/emails",
                    *   summary="Create a new email",
                    *   tags={"Email"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(schema={"type": "string"}, name="key", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema="", name="unique", in="body", required=false, type="string", description="The name of another parameter in the request. If the API finds a record with the specified parameter's property and value, it will update this recrord instead of creating a new record. For example, the following will either create a new merchant or, if a merchant with the specified `google_place_id` is found, it will update this merchant instead: `merchant[name] = 'Swimwear Unlimited', google_place_id = 'abc123', unique = 'google_place_id'`"),
                    *   @SWG\Parameter(schema={"type": "string"}, name="key", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: 'campaigns', 'templates', 'users'. Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Response(response=200, description="The new email")
                    * )
                    */
                
                    /**
                    * @SWG\Patch(
                    *   path="/v1/emails/{id}",
                    *   summary="Update the specified email",
                    *   tags={"Email"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Parameter(schema={"type": "string"}, name="key", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: 'campaigns', 'templates', 'users'. Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Parameter(schema="", name="key", in="body", required=false, type="string"),
                    *   @SWG\Response(response=200, description="The updated email")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/emails/{id}",
                    *   summary="Delete the specified email",
                    *   tags={"Email"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="")
                    * )
                    */
                
                    /**
                    * @SWG\POST(
                    *   path="/v1/emails/{id}/{relationship}/{relationship_id}",
                    *   summary="Attach a relationship to the specified email",
                    *   tags={"Email"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: 'campaigns', 'templates', 'users'"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="email")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/emails/{id}/{relationship}/{relationship_id}",
                    *   summary="Detach a relationship from the specified email",
                    *   tags={"Email"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: 'campaigns', 'templates', 'users'"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="email")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/email-templates",
                    *   summary="Get a list of emailtemplates",
                    *   tags={"EmailTemplate"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: 'campaigns', 'templates'. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="where", in="query", required=false, type="string", description="Comma-separated list of conditions. Examples:
                    (1) `?where=user_id~123` (use ~ instead of =), (2) `?where=created_at>=2018-01-01,created_at<=2018-12-31`"),
                    *   @SWG\Parameter(name="page", in="query", required=false, type="number", description="Tells the paginator what page to return"),
                    *   @SWG\Parameter(name="per_page", in="query", required=false, type="number", description="The number of results to include per page"),
                    *   @SWG\Parameter(name="limit", in="query", required=false, type="number", description="The number of items to return. This will disable pagination. Use 'limit=0' for no limit."),
                    *   @SWG\Response(response=200, description="A list of emailtemplates")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/email-templates/{id}",
                    *   summary="Get the specified emailtemplate",
                    *   tags={"EmailTemplate"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: 'campaigns', 'templates'. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="The specified emailtemplate")
                    * )
                    */
                
                    /**
                    * @SWG\Post(
                    *   path="/v1/email-templates",
                    *   summary="Create a new emailtemplate",
                    *   tags={"EmailTemplate"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(schema="", name="unique", in="body", required=false, type="string", description="The name of another parameter in the request. If the API finds a record with the specified parameter's property and value, it will update this recrord instead of creating a new record. For example, the following will either create a new merchant or, if a merchant with the specified `google_place_id` is found, it will update this merchant instead: `merchant[name] = 'Swimwear Unlimited', google_place_id = 'abc123', unique = 'google_place_id'`"),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: 'campaigns', 'templates'. Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Response(response=200, description="The new emailtemplate")
                    * )
                    */
                
                    /**
                    * @SWG\Patch(
                    *   path="/v1/email-templates/{id}",
                    *   summary="Update the specified emailtemplate",
                    *   tags={"EmailTemplate"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: 'campaigns', 'templates'. Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Response(response=200, description="The updated emailtemplate")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/email-templates/{id}",
                    *   summary="Delete the specified emailtemplate",
                    *   tags={"EmailTemplate"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="")
                    * )
                    */
                
                    /**
                    * @SWG\POST(
                    *   path="/v1/email-templates/{id}/{relationship}/{relationship_id}",
                    *   summary="Attach a relationship to the specified emailtemplate",
                    *   tags={"EmailTemplate"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: 'campaigns', 'templates'"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="emailtemplate")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/email-templates/{id}/{relationship}/{relationship_id}",
                    *   summary="Detach a relationship from the specified emailtemplate",
                    *   tags={"EmailTemplate"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: 'campaigns', 'templates'"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="emailtemplate")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/google-places",
                    *   summary="Get a list of googleplaces",
                    *   tags={"GooglePlace"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: 'merchant', 'products'. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="where", in="query", required=false, type="string", description="Comma-separated list of conditions. Examples:
                    (1) `?where=user_id~123` (use ~ instead of =), (2) `?where=created_at>=2018-01-01,created_at<=2018-12-31`"),
                    *   @SWG\Parameter(name="page", in="query", required=false, type="number", description="Tells the paginator what page to return"),
                    *   @SWG\Parameter(name="per_page", in="query", required=false, type="number", description="The number of results to include per page"),
                    *   @SWG\Parameter(name="limit", in="query", required=false, type="number", description="The number of items to return. This will disable pagination. Use 'limit=0' for no limit."),
                    *   @SWG\Response(response=200, description="A list of googleplaces")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/google-places/{id}",
                    *   summary="Get the specified googleplace",
                    *   tags={"GooglePlace"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: 'merchant', 'products'. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="The specified googleplace")
                    * )
                    */
                
                    /**
                    * @SWG\Post(
                    *   path="/v1/google-places",
                    *   summary="Create a new googleplace",
                    *   tags={"GooglePlace"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(schema={"type": "string"}, name="google_place_id", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema="", name="unique", in="body", required=false, type="string", description="The name of another parameter in the request. If the API finds a record with the specified parameter's property and value, it will update this recrord instead of creating a new record. For example, the following will either create a new merchant or, if a merchant with the specified `google_place_id` is found, it will update this merchant instead: `merchant[name] = 'Swimwear Unlimited', google_place_id = 'abc123', unique = 'google_place_id'`"),
                    *   @SWG\Parameter(schema={"type": "string"}, name="google_place_id", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: 'merchant', 'products'. Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Response(response=200, description="The new googleplace")
                    * )
                    */
                
                    /**
                    * @SWG\Patch(
                    *   path="/v1/google-places/{id}",
                    *   summary="Update the specified googleplace",
                    *   tags={"GooglePlace"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Parameter(schema={"type": "string"}, name="google_place_id", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: 'merchant', 'products'. Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Parameter(schema="", name="google_place_id", in="body", required=false, type="string"),
                    *   @SWG\Response(response=200, description="The updated googleplace")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/google-places/{id}",
                    *   summary="Delete the specified googleplace",
                    *   tags={"GooglePlace"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="")
                    * )
                    */
                
                    /**
                    * @SWG\POST(
                    *   path="/v1/google-places/{id}/{relationship}/{relationship_id}",
                    *   summary="Attach a relationship to the specified googleplace",
                    *   tags={"GooglePlace"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: 'merchant', 'products'"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="googleplace")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/google-places/{id}/{relationship}/{relationship_id}",
                    *   summary="Detach a relationship from the specified googleplace",
                    *   tags={"GooglePlace"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: 'merchant', 'products'"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="googleplace")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/images",
                    *   summary="Get a list of images",
                    *   tags={"Image"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: 'products'. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="where", in="query", required=false, type="string", description="Comma-separated list of conditions. Examples:
                    (1) `?where=user_id~123` (use ~ instead of =), (2) `?where=created_at>=2018-01-01,created_at<=2018-12-31`"),
                    *   @SWG\Parameter(name="page", in="query", required=false, type="number", description="Tells the paginator what page to return"),
                    *   @SWG\Parameter(name="per_page", in="query", required=false, type="number", description="The number of results to include per page"),
                    *   @SWG\Parameter(name="limit", in="query", required=false, type="number", description="The number of items to return. This will disable pagination. Use 'limit=0' for no limit."),
                    *   @SWG\Response(response=200, description="A list of images")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/images/{id}",
                    *   summary="Get the specified image",
                    *   tags={"Image"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: 'products'. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="The specified image")
                    * )
                    */
                
                    /**
                    * @SWG\Post(
                    *   path="/v1/images",
                    *   summary="Create a new image",
                    *   tags={"Image"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(schema={"type": "string"}, name="description", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="title", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="url", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="files", in="body", required=false, type="string", description="Multipart form data"),
                    *   @SWG\Parameter(schema="", name="unique", in="body", required=false, type="string", description="The name of another parameter in the request. If the API finds a record with the specified parameter's property and value, it will update this recrord instead of creating a new record. For example, the following will either create a new merchant or, if a merchant with the specified `google_place_id` is found, it will update this merchant instead: `merchant[name] = 'Swimwear Unlimited', google_place_id = 'abc123', unique = 'google_place_id'`"),
                    *   @SWG\Parameter(schema={"type": "string"}, name="description", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="title", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="url", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="files", in="body", required=false, type="string", description="Multipart form data"),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: 'products'. Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Response(response=200, description="The new image")
                    * )
                    */
                
                    /**
                    * @SWG\Patch(
                    *   path="/v1/images/{id}",
                    *   summary="Update the specified image",
                    *   tags={"Image"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Parameter(schema={"type": "string"}, name="description", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="title", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="url", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="files", in="body", required=false, type="string", description="Multipart form data"),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: 'products'. Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Parameter(schema="", name="description", in="body", required=false, type="string"),
                    *   @SWG\Parameter(schema="", name="title", in="body", required=false, type="string"),
                    *   @SWG\Parameter(schema="", name="url", in="body", required=false, type="string"),
                    *   @SWG\Parameter(schema="", name="files", in="body", required=false, type="string"),
                    *   @SWG\Response(response=200, description="The updated image")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/images/{id}",
                    *   summary="Delete the specified image",
                    *   tags={"Image"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="")
                    * )
                    */
                
                    /**
                    * @SWG\POST(
                    *   path="/v1/images/{id}/{relationship}/{relationship_id}",
                    *   summary="Attach a relationship to the specified image",
                    *   tags={"Image"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: 'products'"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="image")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/images/{id}/{relationship}/{relationship_id}",
                    *   summary="Detach a relationship from the specified image",
                    *   tags={"Image"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: 'products'"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="image")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/likes",
                    *   summary="Get a list of likes",
                    *   tags={"Like"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: 'user', 'likable'. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="where", in="query", required=false, type="string", description="Comma-separated list of conditions. Examples:
                    (1) `?where=user_id~123` (use ~ instead of =), (2) `?where=created_at>=2018-01-01,created_at<=2018-12-31`"),
                    *   @SWG\Parameter(name="page", in="query", required=false, type="number", description="Tells the paginator what page to return"),
                    *   @SWG\Parameter(name="per_page", in="query", required=false, type="number", description="The number of results to include per page"),
                    *   @SWG\Parameter(name="limit", in="query", required=false, type="number", description="The number of items to return. This will disable pagination. Use 'limit=0' for no limit."),
                    *   @SWG\Response(response=200, description="A list of likes")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/likes/{id}",
                    *   summary="Get the specified like",
                    *   tags={"Like"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: 'user', 'likable'. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="The specified like")
                    * )
                    */
                
                    /**
                    * @SWG\Post(
                    *   path="/v1/likes",
                    *   summary="Create a new like",
                    *   tags={"Like"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(schema={"type": "integer"}, name="user_id", in="body", required=true, type="integer", description=""),
                    *   @SWG\Parameter(schema={"type": "integer"}, name="likable_id", in="body", required=true, type="integer", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="likable_type", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "number"}, name="value", in="body", required=false, type="number", description=""),
                    *   @SWG\Parameter(schema="", name="unique", in="body", required=false, type="string", description="The name of another parameter in the request. If the API finds a record with the specified parameter's property and value, it will update this recrord instead of creating a new record. For example, the following will either create a new merchant or, if a merchant with the specified `google_place_id` is found, it will update this merchant instead: `merchant[name] = 'Swimwear Unlimited', google_place_id = 'abc123', unique = 'google_place_id'`"),
                    *   @SWG\Parameter(schema={"type": "integer"}, name="user_id", in="body", required=true, type="integer", description=""),
                    *   @SWG\Parameter(schema={"type": "integer"}, name="likable_id", in="body", required=true, type="integer", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="likable_type", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "number"}, name="value", in="body", required=false, type="number", description=""),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: 'user', 'likable'. Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Response(response=200, description="The new like")
                    * )
                    */
                
                    /**
                    * @SWG\Patch(
                    *   path="/v1/likes/{id}",
                    *   summary="Update the specified like",
                    *   tags={"Like"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Parameter(schema={"type": "integer"}, name="user_id", in="body", required=true, type="integer", description=""),
                    *   @SWG\Parameter(schema={"type": "integer"}, name="likable_id", in="body", required=true, type="integer", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="likable_type", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "number"}, name="value", in="body", required=false, type="number", description=""),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: 'user', 'likable'. Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Parameter(schema="", name="user_id", in="body", required=false, type="integer"),
                    *   @SWG\Parameter(schema="", name="likable_id", in="body", required=false, type="integer"),
                    *   @SWG\Parameter(schema="", name="likable_type", in="body", required=false, type="string"),
                    *   @SWG\Parameter(schema="", name="value", in="body", required=false, type="number"),
                    *   @SWG\Response(response=200, description="The updated like")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/likes/{id}",
                    *   summary="Delete the specified like",
                    *   tags={"Like"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="")
                    * )
                    */
                
                    /**
                    * @SWG\POST(
                    *   path="/v1/likes/{id}/{relationship}/{relationship_id}",
                    *   summary="Attach a relationship to the specified like",
                    *   tags={"Like"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: 'user', 'likable'"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="like")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/likes/{id}/{relationship}/{relationship_id}",
                    *   summary="Detach a relationship from the specified like",
                    *   tags={"Like"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: 'user', 'likable'"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="like")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/merchants",
                    *   summary="Get a list of merchants",
                    *   tags={"Merchant"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: 'addresses', 'google_places', 'orders', 'products', 'stripe_account', 'tags', 'users'. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="where", in="query", required=false, type="string", description="Comma-separated list of conditions. Examples:
                    (1) `?where=user_id~123` (use ~ instead of =), (2) `?where=created_at>=2018-01-01,created_at<=2018-12-31`"),
                    *   @SWG\Parameter(name="page", in="query", required=false, type="number", description="Tells the paginator what page to return"),
                    *   @SWG\Parameter(name="per_page", in="query", required=false, type="number", description="The number of results to include per page"),
                    *   @SWG\Parameter(name="limit", in="query", required=false, type="number", description="The number of items to return. This will disable pagination. Use 'limit=0' for no limit."),
                    *   @SWG\Response(response=200, description="A list of merchants")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/merchants/{id}",
                    *   summary="Get the specified merchant",
                    *   tags={"Merchant"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: 'addresses', 'google_places', 'orders', 'products', 'stripe_account', 'tags', 'users'. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="The specified merchant")
                    * )
                    */
                
                    /**
                    * @SWG\Post(
                    *   path="/v1/merchants",
                    *   summary="Create a new merchant",
                    *   tags={"Merchant"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(schema={"type": "string"}, name="google_place_id", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="name", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="stripe_account", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema="", name="unique", in="body", required=false, type="string", description="The name of another parameter in the request. If the API finds a record with the specified parameter's property and value, it will update this recrord instead of creating a new record. For example, the following will either create a new merchant or, if a merchant with the specified `google_place_id` is found, it will update this merchant instead: `merchant[name] = 'Swimwear Unlimited', google_place_id = 'abc123', unique = 'google_place_id'`"),
                    *   @SWG\Parameter(schema={"type": "string"}, name="google_place_id", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="name", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="stripe_account", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: 'addresses', 'google_places', 'orders', 'products', 'stripe_account', 'tags', 'users'. Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Response(response=200, description="The new merchant")
                    * )
                    */
                
                    /**
                    * @SWG\Patch(
                    *   path="/v1/merchants/{id}",
                    *   summary="Update the specified merchant",
                    *   tags={"Merchant"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Parameter(schema={"type": "string"}, name="google_place_id", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="name", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="stripe_account", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: 'addresses', 'google_places', 'orders', 'products', 'stripe_account', 'tags', 'users'. Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Parameter(schema="", name="google_place_id", in="body", required=false, type="string"),
                    *   @SWG\Parameter(schema="", name="name", in="body", required=false, type="string"),
                    *   @SWG\Parameter(schema="", name="stripe_account", in="body", required=false, type="string"),
                    *   @SWG\Response(response=200, description="The updated merchant")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/merchants/{id}",
                    *   summary="Delete the specified merchant",
                    *   tags={"Merchant"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="")
                    * )
                    */
                
                    /**
                    * @SWG\POST(
                    *   path="/v1/merchants/{id}/{relationship}/{relationship_id}",
                    *   summary="Attach a relationship to the specified merchant",
                    *   tags={"Merchant"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: 'addresses', 'google_places', 'orders', 'products', 'stripe_account', 'tags', 'users'"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="merchant")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/merchants/{id}/{relationship}/{relationship_id}",
                    *   summary="Detach a relationship from the specified merchant",
                    *   tags={"Merchant"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: 'addresses', 'google_places', 'orders', 'products', 'stripe_account', 'tags', 'users'"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="merchant")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/orders",
                    *   summary="Get a list of orders",
                    *   tags={"Order"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: 'merchants'. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="where", in="query", required=false, type="string", description="Comma-separated list of conditions. Examples:
                    (1) `?where=user_id~123` (use ~ instead of =), (2) `?where=created_at>=2018-01-01,created_at<=2018-12-31`"),
                    *   @SWG\Parameter(name="page", in="query", required=false, type="number", description="Tells the paginator what page to return"),
                    *   @SWG\Parameter(name="per_page", in="query", required=false, type="number", description="The number of results to include per page"),
                    *   @SWG\Parameter(name="limit", in="query", required=false, type="number", description="The number of items to return. This will disable pagination. Use 'limit=0' for no limit."),
                    *   @SWG\Response(response=200, description="A list of orders")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/orders/{id}",
                    *   summary="Get the specified order",
                    *   tags={"Order"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: 'merchants'. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="The specified order")
                    * )
                    */
                
                    /**
                    * @SWG\Post(
                    *   path="/v1/orders",
                    *   summary="Create a new order",
                    *   tags={"Order"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(schema={"type": "string"}, name="stripe_transfer_group", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema="", name="unique", in="body", required=false, type="string", description="The name of another parameter in the request. If the API finds a record with the specified parameter's property and value, it will update this recrord instead of creating a new record. For example, the following will either create a new merchant or, if a merchant with the specified `google_place_id` is found, it will update this merchant instead: `merchant[name] = 'Swimwear Unlimited', google_place_id = 'abc123', unique = 'google_place_id'`"),
                    *   @SWG\Parameter(schema={"type": "string"}, name="stripe_transfer_group", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: 'merchants'. Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Response(response=200, description="The new order")
                    * )
                    */
                
                    /**
                    * @SWG\Patch(
                    *   path="/v1/orders/{id}",
                    *   summary="Update the specified order",
                    *   tags={"Order"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Parameter(schema={"type": "string"}, name="stripe_transfer_group", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: 'merchants'. Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Parameter(schema="", name="stripe_transfer_group", in="body", required=false, type="string"),
                    *   @SWG\Response(response=200, description="The updated order")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/orders/{id}",
                    *   summary="Delete the specified order",
                    *   tags={"Order"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="")
                    * )
                    */
                
                    /**
                    * @SWG\POST(
                    *   path="/v1/orders/{id}/{relationship}/{relationship_id}",
                    *   summary="Attach a relationship to the specified order",
                    *   tags={"Order"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: 'merchants'"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="order")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/orders/{id}/{relationship}/{relationship_id}",
                    *   summary="Detach a relationship from the specified order",
                    *   tags={"Order"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: 'merchants'"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="order")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/pay-rates",
                    *   summary="Get a list of payrates",
                    *   tags={"PayRate"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: 'merchants'. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="where", in="query", required=false, type="string", description="Comma-separated list of conditions. Examples:
                    (1) `?where=user_id~123` (use ~ instead of =), (2) `?where=created_at>=2018-01-01,created_at<=2018-12-31`"),
                    *   @SWG\Parameter(name="page", in="query", required=false, type="number", description="Tells the paginator what page to return"),
                    *   @SWG\Parameter(name="per_page", in="query", required=false, type="number", description="The number of results to include per page"),
                    *   @SWG\Parameter(name="limit", in="query", required=false, type="number", description="The number of items to return. This will disable pagination. Use 'limit=0' for no limit."),
                    *   @SWG\Response(response=200, description="A list of payrates")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/pay-rates/{id}",
                    *   summary="Get the specified payrate",
                    *   tags={"PayRate"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: 'merchants'. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="The specified payrate")
                    * )
                    */
                
                    /**
                    * @SWG\Post(
                    *   path="/v1/pay-rates",
                    *   summary="Create a new payrate",
                    *   tags={"PayRate"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(schema={"type": "string"}, name="percentage", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema="", name="unique", in="body", required=false, type="string", description="The name of another parameter in the request. If the API finds a record with the specified parameter's property and value, it will update this recrord instead of creating a new record. For example, the following will either create a new merchant or, if a merchant with the specified `google_place_id` is found, it will update this merchant instead: `merchant[name] = 'Swimwear Unlimited', google_place_id = 'abc123', unique = 'google_place_id'`"),
                    *   @SWG\Parameter(schema={"type": "string"}, name="percentage", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: 'merchants'. Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Response(response=200, description="The new payrate")
                    * )
                    */
                
                    /**
                    * @SWG\Patch(
                    *   path="/v1/pay-rates/{id}",
                    *   summary="Update the specified payrate",
                    *   tags={"PayRate"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Parameter(schema={"type": "string"}, name="percentage", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: 'merchants'. Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Parameter(schema="", name="percentage", in="body", required=false, type="string"),
                    *   @SWG\Response(response=200, description="The updated payrate")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/pay-rates/{id}",
                    *   summary="Delete the specified payrate",
                    *   tags={"PayRate"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="")
                    * )
                    */
                
                    /**
                    * @SWG\POST(
                    *   path="/v1/pay-rates/{id}/{relationship}/{relationship_id}",
                    *   summary="Attach a relationship to the specified payrate",
                    *   tags={"PayRate"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: 'merchants'"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="payrate")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/pay-rates/{id}/{relationship}/{relationship_id}",
                    *   summary="Detach a relationship from the specified payrate",
                    *   tags={"PayRate"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: 'merchants'"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="payrate")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/products",
                    *   summary="Get a list of products",
                    *   tags={"Product"},
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: 'images', 'google_places', 'merchant', 'user', 'votes'. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="where", in="query", required=false, type="string", description="Comma-separated list of conditions. Examples:
                    (1) `?where=user_id~123` (use ~ instead of =), (2) `?where=created_at>=2018-01-01,created_at<=2018-12-31`"),
                    *   @SWG\Parameter(name="page", in="query", required=false, type="number", description="Tells the paginator what page to return"),
                    *   @SWG\Parameter(name="per_page", in="query", required=false, type="number", description="The number of results to include per page"),
                    *   @SWG\Parameter(name="limit", in="query", required=false, type="number", description="The number of items to return. This will disable pagination. Use 'limit=0' for no limit."),
                    *   @SWG\Response(response=200, description="A list of products")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/products/{id}",
                    *   summary="Get the specified product",
                    *   tags={"Product"},
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: 'images', 'google_places', 'merchant', 'user', 'votes'. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="The specified product")
                    * )
                    */
                
                    /**
                    * @SWG\Post(
                    *   path="/v1/products",
                    *   summary="Create a new product",
                    *   tags={"Product"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(schema={"type": "boolean"}, name="approved", in="body", required=false, type="boolean", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="description", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "number"}, name="discount_price", in="body", required=false, type="number", description=""),
                    *   @SWG\Parameter(schema={"type": "integer"}, name="merchant_id", in="body", required=false, type="integer", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="name", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "number"}, name="regular_price", in="body", required=false, type="number", description=""),
                    *   @SWG\Parameter(schema={"type": "integer"}, name="user_id", in="body", required=true, type="integer", description=""),
                    *   @SWG\Parameter(schema="", name="unique", in="body", required=false, type="string", description="The name of another parameter in the request. If the API finds a record with the specified parameter's property and value, it will update this recrord instead of creating a new record. For example, the following will either create a new merchant or, if a merchant with the specified `google_place_id` is found, it will update this merchant instead: `merchant[name] = 'Swimwear Unlimited', google_place_id = 'abc123', unique = 'google_place_id'`"),
                    *   @SWG\Parameter(schema={"type": "boolean"}, name="approved", in="body", required=false, type="boolean", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="description", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "number"}, name="discount_price", in="body", required=false, type="number", description=""),
                    *   @SWG\Parameter(schema={"type": "integer"}, name="merchant_id", in="body", required=false, type="integer", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="name", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "number"}, name="regular_price", in="body", required=false, type="number", description=""),
                    *   @SWG\Parameter(schema={"type": "integer"}, name="user_id", in="body", required=true, type="integer", description=""),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: 'images', 'google_places', 'merchant', 'user', 'votes'. Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Response(response=200, description="The new product")
                    * )
                    */
                
                    /**
                    * @SWG\Patch(
                    *   path="/v1/products/{id}",
                    *   summary="Update the specified product",
                    *   tags={"Product"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Parameter(schema={"type": "boolean"}, name="approved", in="body", required=false, type="boolean", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="description", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "number"}, name="discount_price", in="body", required=false, type="number", description=""),
                    *   @SWG\Parameter(schema={"type": "integer"}, name="merchant_id", in="body", required=false, type="integer", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="name", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "number"}, name="regular_price", in="body", required=false, type="number", description=""),
                    *   @SWG\Parameter(schema={"type": "integer"}, name="user_id", in="body", required=true, type="integer", description=""),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: 'images', 'google_places', 'merchant', 'user', 'votes'. Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Parameter(schema="", name="approved", in="body", required=false, type="boolean"),
                    *   @SWG\Parameter(schema="", name="description", in="body", required=false, type="string"),
                    *   @SWG\Parameter(schema="", name="discount_price", in="body", required=false, type="number"),
                    *   @SWG\Parameter(schema="", name="merchant_id", in="body", required=false, type="integer"),
                    *   @SWG\Parameter(schema="", name="name", in="body", required=false, type="string"),
                    *   @SWG\Parameter(schema="", name="regular_price", in="body", required=false, type="number"),
                    *   @SWG\Parameter(schema="", name="user_id", in="body", required=false, type="integer"),
                    *   @SWG\Response(response=200, description="The updated product")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/products/{id}",
                    *   summary="Delete the specified product",
                    *   tags={"Product"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="")
                    * )
                    */
                
                    /**
                    * @SWG\POST(
                    *   path="/v1/products/{id}/{relationship}/{relationship_id}",
                    *   summary="Attach a relationship to the specified product",
                    *   tags={"Product"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: 'images', 'google_places', 'merchant', 'user', 'votes'"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="product")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/products/{id}/{relationship}/{relationship_id}",
                    *   summary="Detach a relationship from the specified product",
                    *   tags={"Product"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: 'images', 'google_places', 'merchant', 'user', 'votes'"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="product")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/settings",
                    *   summary="Get a list of settings",
                    *   tags={"Setting"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: . You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="where", in="query", required=false, type="string", description="Comma-separated list of conditions. Examples:
                    (1) `?where=user_id~123` (use ~ instead of =), (2) `?where=created_at>=2018-01-01,created_at<=2018-12-31`"),
                    *   @SWG\Parameter(name="page", in="query", required=false, type="number", description="Tells the paginator what page to return"),
                    *   @SWG\Parameter(name="per_page", in="query", required=false, type="number", description="The number of results to include per page"),
                    *   @SWG\Parameter(name="limit", in="query", required=false, type="number", description="The number of items to return. This will disable pagination. Use 'limit=0' for no limit."),
                    *   @SWG\Response(response=200, description="A list of settings")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/settings/{id}",
                    *   summary="Get the specified setting",
                    *   tags={"Setting"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: . You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="The specified setting")
                    * )
                    */
                
                    /**
                    * @SWG\Post(
                    *   path="/v1/settings",
                    *   summary="Create a new setting",
                    *   tags={"Setting"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(schema="", name="unique", in="body", required=false, type="string", description="The name of another parameter in the request. If the API finds a record with the specified parameter's property and value, it will update this recrord instead of creating a new record. For example, the following will either create a new merchant or, if a merchant with the specified `google_place_id` is found, it will update this merchant instead: `merchant[name] = 'Swimwear Unlimited', google_place_id = 'abc123', unique = 'google_place_id'`"),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: . Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Response(response=200, description="The new setting")
                    * )
                    */
                
                    /**
                    * @SWG\Patch(
                    *   path="/v1/settings/{id}",
                    *   summary="Update the specified setting",
                    *   tags={"Setting"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: . Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Response(response=200, description="The updated setting")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/settings/{id}",
                    *   summary="Delete the specified setting",
                    *   tags={"Setting"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="")
                    * )
                    */
                
                    /**
                    * @SWG\POST(
                    *   path="/v1/settings/{id}/{relationship}/{relationship_id}",
                    *   summary="Attach a relationship to the specified setting",
                    *   tags={"Setting"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: "),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="setting")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/settings/{id}/{relationship}/{relationship_id}",
                    *   summary="Detach a relationship from the specified setting",
                    *   tags={"Setting"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: "),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="setting")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/tags",
                    *   summary="Get a list of tags",
                    *   tags={"Tag"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: 'taggable'. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="where", in="query", required=false, type="string", description="Comma-separated list of conditions. Examples:
                    (1) `?where=user_id~123` (use ~ instead of =), (2) `?where=created_at>=2018-01-01,created_at<=2018-12-31`"),
                    *   @SWG\Parameter(name="page", in="query", required=false, type="number", description="Tells the paginator what page to return"),
                    *   @SWG\Parameter(name="per_page", in="query", required=false, type="number", description="The number of results to include per page"),
                    *   @SWG\Parameter(name="limit", in="query", required=false, type="number", description="The number of items to return. This will disable pagination. Use 'limit=0' for no limit."),
                    *   @SWG\Response(response=200, description="A list of tags")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/tags/{id}",
                    *   summary="Get the specified tag",
                    *   tags={"Tag"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: 'taggable'. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="The specified tag")
                    * )
                    */
                
                    /**
                    * @SWG\Post(
                    *   path="/v1/tags",
                    *   summary="Create a new tag",
                    *   tags={"Tag"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(schema={"type": "string"}, name="name", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema="", name="unique", in="body", required=false, type="string", description="The name of another parameter in the request. If the API finds a record with the specified parameter's property and value, it will update this recrord instead of creating a new record. For example, the following will either create a new merchant or, if a merchant with the specified `google_place_id` is found, it will update this merchant instead: `merchant[name] = 'Swimwear Unlimited', google_place_id = 'abc123', unique = 'google_place_id'`"),
                    *   @SWG\Parameter(schema={"type": "string"}, name="name", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: 'taggable'. Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Response(response=200, description="The new tag")
                    * )
                    */
                
                    /**
                    * @SWG\Patch(
                    *   path="/v1/tags/{id}",
                    *   summary="Update the specified tag",
                    *   tags={"Tag"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Parameter(schema={"type": "string"}, name="name", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: 'taggable'. Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Parameter(schema="", name="name", in="body", required=false, type="string"),
                    *   @SWG\Response(response=200, description="The updated tag")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/tags/{id}",
                    *   summary="Delete the specified tag",
                    *   tags={"Tag"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="")
                    * )
                    */
                
                    /**
                    * @SWG\POST(
                    *   path="/v1/tags/{id}/{relationship}/{relationship_id}",
                    *   summary="Attach a relationship to the specified tag",
                    *   tags={"Tag"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: 'taggable'"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="tag")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/tags/{id}/{relationship}/{relationship_id}",
                    *   summary="Detach a relationship from the specified tag",
                    *   tags={"Tag"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: 'taggable'"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="tag")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/users",
                    *   summary="Get a list of users",
                    *   tags={"User"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: 'merchants', 'orders', 'products', 'role', 'stats', 'votes'. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="where", in="query", required=false, type="string", description="Comma-separated list of conditions. Examples:
                    (1) `?where=user_id~123` (use ~ instead of =), (2) `?where=created_at>=2018-01-01,created_at<=2018-12-31`"),
                    *   @SWG\Parameter(name="page", in="query", required=false, type="number", description="Tells the paginator what page to return"),
                    *   @SWG\Parameter(name="per_page", in="query", required=false, type="number", description="The number of results to include per page"),
                    *   @SWG\Parameter(name="limit", in="query", required=false, type="number", description="The number of items to return. This will disable pagination. Use 'limit=0' for no limit."),
                    *   @SWG\Response(response=200, description="A list of users")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/users/{id}",
                    *   summary="Get the specified user",
                    *   tags={"User"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: 'merchants', 'orders', 'products', 'role', 'stats', 'votes'. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="The specified user")
                    * )
                    */
                
                    /**
                    * @SWG\Post(
                    *   path="/v1/users",
                    *   summary="Create a new user",
                    *   tags={"User"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(schema={"type": "string"}, name="email", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="password", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="c_password", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="username", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "boolean"}, name="interested_in_becoming_affiliate", in="body", required=false, type="boolean", description=""),
                    *   @SWG\Parameter(schema="", name="unique", in="body", required=false, type="string", description="The name of another parameter in the request. If the API finds a record with the specified parameter's property and value, it will update this recrord instead of creating a new record. For example, the following will either create a new merchant or, if a merchant with the specified `google_place_id` is found, it will update this merchant instead: `merchant[name] = 'Swimwear Unlimited', google_place_id = 'abc123', unique = 'google_place_id'`"),
                    *   @SWG\Parameter(schema={"type": "string"}, name="email", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="password", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="c_password", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="username", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "boolean"}, name="interested_in_becoming_affiliate", in="body", required=false, type="boolean", description=""),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: 'merchants', 'orders', 'products', 'role', 'stats', 'votes'. Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Response(response=200, description="The new user")
                    * )
                    */
                
                    /**
                    * @SWG\Patch(
                    *   path="/v1/users/{id}",
                    *   summary="Update the specified user",
                    *   tags={"User"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Parameter(schema={"type": "string"}, name="email", in="body", required=true, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="password", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="c_password", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "string"}, name="username", in="body", required=false, type="string", description=""),
                    *   @SWG\Parameter(schema={"type": "boolean"}, name="interested_in_becoming_affiliate", in="body", required=false, type="boolean", description=""),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: 'merchants', 'orders', 'products', 'role', 'stats', 'votes'. Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Parameter(schema="", name="email", in="body", required=false, type="string"),
                    *   @SWG\Parameter(schema="", name="password", in="body", required=false, type="string"),
                    *   @SWG\Parameter(schema="", name="c_password", in="body", required=false, type="string"),
                    *   @SWG\Parameter(schema="", name="username", in="body", required=false, type="string"),
                    *   @SWG\Parameter(schema="", name="interested_in_becoming_affiliate", in="body", required=false, type="boolean"),
                    *   @SWG\Response(response=200, description="The updated user")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/users/{id}",
                    *   summary="Delete the specified user",
                    *   tags={"User"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="")
                    * )
                    */
                
                    /**
                    * @SWG\POST(
                    *   path="/v1/users/{id}/{relationship}/{relationship_id}",
                    *   summary="Attach a relationship to the specified user",
                    *   tags={"User"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: 'merchants', 'orders', 'products', 'role', 'stats', 'votes'"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="user")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/users/{id}/{relationship}/{relationship_id}",
                    *   summary="Detach a relationship from the specified user",
                    *   tags={"User"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: 'merchants', 'orders', 'products', 'role', 'stats', 'votes'"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="user")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/user-stats",
                    *   summary="Get a list of userstats",
                    *   tags={"UserStat"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: 'user'. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="where", in="query", required=false, type="string", description="Comma-separated list of conditions. Examples:
                    (1) `?where=user_id~123` (use ~ instead of =), (2) `?where=created_at>=2018-01-01,created_at<=2018-12-31`"),
                    *   @SWG\Parameter(name="page", in="query", required=false, type="number", description="Tells the paginator what page to return"),
                    *   @SWG\Parameter(name="per_page", in="query", required=false, type="number", description="The number of results to include per page"),
                    *   @SWG\Parameter(name="limit", in="query", required=false, type="number", description="The number of items to return. This will disable pagination. Use 'limit=0' for no limit."),
                    *   @SWG\Response(response=200, description="A list of userstats")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/user-stats/{id}",
                    *   summary="Get the specified userstat",
                    *   tags={"UserStat"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: 'user'. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="The specified userstat")
                    * )
                    */
                
                    /**
                    * @SWG\Post(
                    *   path="/v1/user-stats",
                    *   summary="Create a new userstat",
                    *   tags={"UserStat"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(schema={"type": "integer"}, name="user_id", in="body", required=true, type="integer", description=""),
                    *   @SWG\Parameter(schema="", name="unique", in="body", required=false, type="string", description="The name of another parameter in the request. If the API finds a record with the specified parameter's property and value, it will update this recrord instead of creating a new record. For example, the following will either create a new merchant or, if a merchant with the specified `google_place_id` is found, it will update this merchant instead: `merchant[name] = 'Swimwear Unlimited', google_place_id = 'abc123', unique = 'google_place_id'`"),
                    *   @SWG\Parameter(schema={"type": "integer"}, name="user_id", in="body", required=true, type="integer", description=""),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: 'user'. Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Response(response=200, description="The new userstat")
                    * )
                    */
                
                    /**
                    * @SWG\Patch(
                    *   path="/v1/user-stats/{id}",
                    *   summary="Update the specified userstat",
                    *   tags={"UserStat"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Parameter(schema={"type": "integer"}, name="user_id", in="body", required=true, type="integer", description=""),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: 'user'. Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Parameter(schema="", name="user_id", in="body", required=false, type="integer"),
                    *   @SWG\Response(response=200, description="The updated userstat")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/user-stats/{id}",
                    *   summary="Delete the specified userstat",
                    *   tags={"UserStat"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="")
                    * )
                    */
                
                    /**
                    * @SWG\POST(
                    *   path="/v1/user-stats/{id}/{relationship}/{relationship_id}",
                    *   summary="Attach a relationship to the specified userstat",
                    *   tags={"UserStat"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: 'user'"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="userstat")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/user-stats/{id}/{relationship}/{relationship_id}",
                    *   summary="Detach a relationship from the specified userstat",
                    *   tags={"UserStat"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: 'user'"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="userstat")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/votes",
                    *   summary="Get a list of votes",
                    *   tags={"Vote"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: 'product', 'user'. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="where", in="query", required=false, type="string", description="Comma-separated list of conditions. Examples:
                    (1) `?where=user_id~123` (use ~ instead of =), (2) `?where=created_at>=2018-01-01,created_at<=2018-12-31`"),
                    *   @SWG\Parameter(name="page", in="query", required=false, type="number", description="Tells the paginator what page to return"),
                    *   @SWG\Parameter(name="per_page", in="query", required=false, type="number", description="The number of results to include per page"),
                    *   @SWG\Parameter(name="limit", in="query", required=false, type="number", description="The number of items to return. This will disable pagination. Use 'limit=0' for no limit."),
                    *   @SWG\Response(response=200, description="A list of votes")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/votes/{id}",
                    *   summary="Get the specified vote",
                    *   tags={"Vote"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: 'product', 'user'. You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="The specified vote")
                    * )
                    */
                
                    /**
                    * @SWG\Post(
                    *   path="/v1/votes",
                    *   summary="Create a new vote",
                    *   tags={"Vote"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(schema={"type": "integer"}, name="product_id", in="body", required=true, type="integer", description=""),
                    *   @SWG\Parameter(schema={"type": "number"}, name="value", in="body", required=false, type="number", description=""),
                    *   @SWG\Parameter(schema="", name="unique", in="body", required=false, type="string", description="The name of another parameter in the request. If the API finds a record with the specified parameter's property and value, it will update this recrord instead of creating a new record. For example, the following will either create a new merchant or, if a merchant with the specified `google_place_id` is found, it will update this merchant instead: `merchant[name] = 'Swimwear Unlimited', google_place_id = 'abc123', unique = 'google_place_id'`"),
                    *   @SWG\Parameter(schema={"type": "integer"}, name="product_id", in="body", required=true, type="integer", description=""),
                    *   @SWG\Parameter(schema={"type": "number"}, name="value", in="body", required=false, type="number", description=""),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: 'product', 'user'. Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Response(response=200, description="The new vote")
                    * )
                    */
                
                    /**
                    * @SWG\Patch(
                    *   path="/v1/votes/{id}",
                    *   summary="Update the specified vote",
                    *   tags={"Vote"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Parameter(schema={"type": "integer"}, name="product_id", in="body", required=true, type="integer", description=""),
                    *   @SWG\Parameter(schema={"type": "number"}, name="value", in="body", required=false, type="number", description=""),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: 'product', 'user'. Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Parameter(schema="", name="product_id", in="body", required=false, type="integer"),
                    *   @SWG\Parameter(schema="", name="value", in="body", required=false, type="number"),
                    *   @SWG\Response(response=200, description="The updated vote")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/votes/{id}",
                    *   summary="Delete the specified vote",
                    *   tags={"Vote"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="")
                    * )
                    */
                
                    /**
                    * @SWG\POST(
                    *   path="/v1/votes/{id}/{relationship}/{relationship_id}",
                    *   summary="Attach a relationship to the specified vote",
                    *   tags={"Vote"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: 'product', 'user'"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="vote")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/votes/{id}/{relationship}/{relationship_id}",
                    *   summary="Detach a relationship from the specified vote",
                    *   tags={"Vote"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: 'product', 'user'"),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="vote")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/wc-orders",
                    *   summary="Get a list of wcorders",
                    *   tags={"WcOrder"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: . You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="where", in="query", required=false, type="string", description="Comma-separated list of conditions. Examples:
                    (1) `?where=user_id~123` (use ~ instead of =), (2) `?where=created_at>=2018-01-01,created_at<=2018-12-31`"),
                    *   @SWG\Parameter(name="page", in="query", required=false, type="number", description="Tells the paginator what page to return"),
                    *   @SWG\Parameter(name="per_page", in="query", required=false, type="number", description="The number of results to include per page"),
                    *   @SWG\Parameter(name="limit", in="query", required=false, type="number", description="The number of items to return. This will disable pagination. Use 'limit=0' for no limit."),
                    *   @SWG\Response(response=200, description="A list of wcorders")
                    * )
                    */
                
                    /**
                    * @SWG\Get(
                    *   path="/v1/wc-orders/{id}",
                    *   summary="Get the specified wcorder",
                    *   tags={"WcOrder"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="with", in="query", required=false, type="string", description="Comma-separated list of relationships. Options: . You can include any level of sub-relationships (relationships belonging to the relationship) using dot notation. Example: `?with=product,product.images,product.images.tags`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="The specified wcorder")
                    * )
                    */
                
                    /**
                    * @SWG\Post(
                    *   path="/v1/wc-orders",
                    *   summary="Create a new wcorder",
                    *   tags={"WcOrder"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(schema="", name="unique", in="body", required=false, type="string", description="The name of another parameter in the request. If the API finds a record with the specified parameter's property and value, it will update this recrord instead of creating a new record. For example, the following will either create a new merchant or, if a merchant with the specified `google_place_id` is found, it will update this merchant instead: `merchant[name] = 'Swimwear Unlimited', google_place_id = 'abc123', unique = 'google_place_id'`"),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: . Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Response(response=200, description="The new wcorder")
                    * )
                    */
                
                    /**
                    * @SWG\Patch(
                    *   path="/v1/wc-orders/{id}",
                    *   summary="Update the specified wcorder",
                    *   tags={"WcOrder"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Parameter(schema="", name="{relationship_name}", in="body", required=false, type="array", items={"type": "string"}, description="An array of parameters to be posted to any level of relationships or subrelationships. Options: . Example: `merchant[name] = 'Swimwear Unlimited', merchant[tags][0][name] = 'Clothing', merchant[tags][1][name] = 'Swimwear', products[0][name] = 'Wetsuit', products[0][images][files] = '{multipart form data}'` If an `id` parameter is included in an array, the specified relationship will be updated instead of created. Example: `products[0][name] = 'Wetsuit', products[1][id] = 123, products[1][name] = 'Goggles'` The first related product will be created. The second related product will be updated."),
                    *   @SWG\Response(response=200, description="The updated wcorder")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/wc-orders/{id}",
                    *   summary="Delete the specified wcorder",
                    *   tags={"WcOrder"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="id", in="path", required=true, type="number"),
                    *   @SWG\Response(response=200, description="")
                    * )
                    */
                
                    /**
                    * @SWG\POST(
                    *   path="/v1/wc-orders/{id}/{relationship}/{relationship_id}",
                    *   summary="Attach a relationship to the specified wcorder",
                    *   tags={"WcOrder"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: "),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="wcorder")
                    * )
                    */
                
                    /**
                    * @SWG\Delete(
                    *   path="/v1/wc-orders/{id}/{relationship}/{relationship_id}",
                    *   summary="Detach a relationship from the specified wcorder",
                    *   tags={"WcOrder"},
                    *   @SWG\Parameter(name="Authorization", in="header", required=true, type="string", description="Use the access token obtained from the login method. Format: `Bearer {access_token}`"),
                    *   @SWG\Parameter(name="relationship", in="path", required=true, type="string", description="The name of the relationship. Options: "),
                    *   @SWG\Parameter(name="relationship_id", in="path", required=true, type="number", description="The ID of the object to attach"),
                    *   @SWG\Response(response=200, description="wcorder")
                    * )
                    */
                