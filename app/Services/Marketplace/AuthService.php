<?php

namespace App\Services\Marketplace;

use App\Transformers\UserTransformer;

class AuthService
{
    protected $userTrans;

    public function __construct(UserTransformer $userTrans) {
        $this->url = config('site.shop_url') . '/api.php?';
        $this->userTrans = $userTrans;
    }

    /**
     * Attach or register new user
     */
    function attachOrRegister($inputs)
    {
        try {
            $response = \Woocommerce::post('spendindie/register', $inputs);
            return $response;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Login using Marketplace authentication.
     */
    function login(int $id)
    {
        try {
            $user = \Woocommerce::get("customers/$id");
            return $user;
        } catch (\Exception $e) {
            dd($e->getResponse());
        }
    }

}
