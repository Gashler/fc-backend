<?php

namespace App\Services\Marketplace;

class OrderService
{
    public function __construct() {
        $this->url = config('site.shop_url') . '/api.php?';
    }

    /**
     * Transfer a product to WooCommerce
     */
    public function request($method = 'get', array $params = [])
    {
        try {
            $response = \Woocommerce::$method('orders', $params);
            return [
                'body' => [
                    'data' => $response
                ],
                'code' => 200
            ];
        } catch (\Exception $e) {
            return [
                'body' => [
                    'message' => $e->getMessage(),
                    'request' => $e->getRequest(),
                    'response' => $e->getResponse()
                ],
                'code' => accessProtected($e, 'code')
            ];
        }
    }
}
