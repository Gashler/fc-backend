<?php

namespace App\Services\Marketplace;

use App\Transformers\ProductTransformer;

class ProductService
{
    public function __construct(ProductTransformer $productTransformer) {
        $this->url = config('site.shop_url') . '/api.php?';
        $this->productTransformer = $productTransformer;
    }

    /**
     * Transfer a product to WooCommerce
     */
    function transfer($product_id)
    {
        if (!$product = \App\Product::with(['categories', 'images'])->find($product_id)) {
            return [
                'body' => [
                    'message' => "Product not found."
                ],
                'code' => 404
            ];
        }
        $wcProduct = $this->productTransformer->toWcProduct($product);
        try {
            $response = \Woocommerce::post('products', $wcProduct);
            return [
                'body' => [
                    'message' => "WooCommerce Product created.",
                    'data' => $response
                ],
                'code' => 200
            ];
        } catch (\Exception $e) {
            dd($e);
        }
    }
}
