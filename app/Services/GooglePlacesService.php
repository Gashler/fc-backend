<?php

namespace App\Services;

use App\Services\CurlService;

class GooglePlacesService
{
    public function __construct()
    {
        $this->curlService = new CurlService;
    }

    public function request(
        string $method,
        string $endpoint,
        string $google_place_id = null
    ) {
        $url = 'https://maps.googleapis.com/maps/api/place/' . $endpoint . '/json';
        $body = ['key' => config('google.places.key')];
        if (isset($google_place_id)) {
            $body['placeid'] = $google_place_id;
        }
        $response = $this->curlService->request($method, $url, $body);
        if ($response->status == 'INVALID_REQUEST') {
            return;
        }
        return $response->result;
    }

    /**
     * Get Place Details
     */
    public function getDetails($google_place_id)
    {
        return $this->request('get', 'details', $google_place_id);
    }
}
