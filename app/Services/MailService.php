<?php

namespace App\Services;

use App\Email;
use App\Mail\DefaultMail;

class MailService extends CurlService
{
    /**
     * Evaluate Blade/PHP code within a string
     */
     function render($blade, $user, $data)
     {
         $__php = \Blade::compileString($blade);
         $data['__env'] = app(\Illuminate\View\Factory::class);
         $obLevel = ob_get_level();
         ob_start();
         extract($data, EXTR_SKIP);
         try {
             eval('?' . '>' . $__php);
         } catch (Exception $e) {
             while (ob_get_level() > $obLevel) ob_end_clean();
             throw $e;
         } catch (Throwable $e) {
             while (ob_get_level() > $obLevel) ob_end_clean();
             throw new FatalThrowableError($e);
         }
         return ob_get_clean();
     }

    /**
     * Send an email
     */
    public function send(
        string $email_key = null,
        array $data = [],
        array $user_ids = [],
        array $emails = [],
        string $custom_subject = null,
        string $custom_body = null
    ) {
        // define variables
        if (count($user_ids) == 0 && count($emails) == 0) {
            $user_ids[] = auth()->user()->id;
        }
        $users = \App\User::whereIn('id', $user_ids)->get();
        foreach ($emails as $email) {
            $user = new \App\User;
            $user->email = $email;
            $users[] = $user;
        }
        if (!$email = Email::where('key', $email_key)->first()) {
            return [
                'body' => [
                    'message' => 'Email not found.'
                ],
                'code' => 500
            ];
        };
        $email->subject = $custom_subject ?? $email->subject;
        $email->body .= $custom_body;
        foreach ($users as $index => $user) {

            // evaluate code
            $email->subject = $this->render($email->subject, $user, $data);
            $email->body = $this->render($email->body, $user, $data);

            // send email
            $response = \Mail::to($user)
                ->send(new DefaultMail($email, $user));

            // if user isn't registered with system
            if (!isset($user->id)) {
                $user = \App\User::updateOrCreate(['email' => $user->email]);
                $users[$index] = $user;
            }

            // update number of times user has received email
            if ($relationship = $email->users()->find($user->id)) {
                $email->users()->updateExistingPivot($user->id, [
                    'amount' => $relationship->pivot->amount += 1
                ]);

            // or attach user to email (for keeping a sending history)
            } else {
                $email->users()->attach($user);
            }
        }

        // prepare response
        $emailString = $users[0]->email;
        if (count($users) > 1) {
            $sentence = "Emails sent to ";
            foreach ($users as $index => $user) {
                if ($index == 0) {
                    continue;
                }
                $emailString .= ", " . $user->email;
            }
        } else {
            $sentence = "Email sent to ";
        }
        $sentence .= $emailString . ".";
        $response = [
            'message' => $sentence
        ];
        return [
            'body' => $response,
            'code' => 200
        ];
    }
}
