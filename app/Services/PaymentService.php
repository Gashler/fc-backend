<?php

namespace App\Services;

use App\Repositories\CampaignRepository;
use App\Repositories\PaymentRepository;
use App\Repositories\UserRepository;
use App\Services\StripeService;
use App\Repositories\SubscriptionRepository;
use App\Transformers\UserTransformer;

class PaymentService
{
    public function __construct(
        CampaignRepository $campaignRepo,
        StripeService $stripeService,
        SubscriptionRepository $subscriptionRepo,
        UserRepository $userRepo
    ) {
        $this->campaignRepo = $campaignRepo;
        $this->subscriptionRepo = $subscriptionRepo;
        $this->stripeService = $stripeService;
        $this->userRepo = $userRepo;
        $this->userTrans = new UserTransformer;
    }

    /**
     * Create payment (and associatd objects)
     */
    public function create(array $params)
    {
        // create user and log in (if not already logged in)
        if (auth()->check()) {
            $user = auth()->user();
            unset($params['user']);
        } else {
            $response = $this->userRepo->create($params['user']);
            if ($response['code'] !== 200) {
                return $response;
            }
            $user = $response['body']['data'];
            auth()->loginUsingId($user->id);
            unset($params['user']);
        }

        // generate token for user
        $token = $user->createToken('Auth token')->accessToken;
        $user->save();

        // create Stripe charge
        $stripePayment = $this->stripeService->createPayment($params);

        // update campaign
        $campaign = $this->campaignRepo->find($params['campaign_id']);
        $campaign->monetary_total += $params['amount'];
        $campaign->save();

        // attach user (backer) to campaign (if not already attached)
        if (!$campaign->users()->find($user->id)) {
            $campaign->users()->save($user);
        }

        // get user's subscriptions
        $userWithSubscriptions = $this->userRepo->find($user->id, ['subscriptions']);
        $user->subscriptions = $userWithSubscriptions->subscriptions;

        // attach user to "backers" subscription
        $subscription = $this->subscriptionRepo->findBy('key', 'backers');
        if (!$user->subscriptions()->find($subscription->id)) {
            $user->subscriptions()->attach($subscription->id);
        }

        // prepare payment data
        $params['stripe_id'] = $stripePayment->id;
        $params['card_brand'] = $stripePayment->source->brand;
        $params['card_last4'] = $stripePayment->source->last4;
        $params['user_id'] = $user->id;


        return [
            'params' => $params,
            'token' => $token,
            'user' => $user
        ];
    }
}
