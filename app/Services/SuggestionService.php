<?php

namespace App\Services;

use App\Merchant;
use App\Product;
use App\Tag;
use App\Repositories\ImageRepository;
use App\Repositories\MerchantRepository;
use App\Repositories\ProductRepository;

class SuggestionService
{
    protected $imageRepo;
    protected $merchantRepo;
    protected $productRepo;

    public function __construct(
        ImageRepository $imageRepo,
        MerchantRepository $merchantRepo,
        ProductRepository $productRepo
    ) {
        $this->imageRepo = $imageRepo;
        $this->merchantRepo = $merchantRepo;
        $this->productRepo = $productRepo;
    }

    /**
     * Store a suggestion
     */
    public function store($input)
    {
        // create merchant if doesn't exist
        if (!$merchant = Merchant::where('google_place_id', $input['merchant']['google_place_id'])->first()) {
            $merchant = $this->merchantRepo->create([
                'google_place_id' => $input['merchant']['google_place_id'],
                'name' => $input['merchant']['name'],
            ]);
        }

        // assign tags to merchant
        if (isset($input['merchant']['types'])) {
            foreach ($input['merchant']['types'] as $tag_name) {
                if (!$tag = Tag::where('name', $tag_name)->first()) {
                    $tag = Tag::create(['name' => $tag_name]);
                }
                if (!$merchant->tags()->where('name', $tag_name)->first()) {
                    $merchant->tags()->attach($tag);
                }
            }
        }

        // create image
        $image = $this->imageRepo->create($input['image']);

        // create product
        $input['product']['merchant_id'] = $merchant->id;
        $input['product']['user_id'] = auth()->user()->id;
        $product = $this->productRepo->create($input['product']);

        // attach image to product
        $product->images()->attach($image);

        return Product::with('merchant', 'merchant.tags')->find($product->id);
    }
}
