<?php

namespace App\Services;

use App\Merchant;
use App\Order;
use App\Services\CurlService;
use App\Repositories\UserRepository;

class StripeService
{
    public function __construct(
        CurlService $curlService,
        UserRepository $userRepo
    ) {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        $this->stripeFeePercent = .029;
        $this->stripeFeeFlat = .3;
        $this->curlService = $curlService;
        $this->userRepo = $userRepo;
    }

    /**
     * Pay Merchants
     */
    public function createPayment($params)
    {
        return \Stripe\Charge::create([
            "amount" => $params['amount'] * 100, // Stripe counts in cents
            "currency" => "usd",
            "source" => $params['token']
        ]);
    }

    /**
     * Get Payments
     */
    public function getPayments($params = [])
    {
        return \Stripe\Charge::all($params);
    }

    /**
    * Finalize Account connection
    */
    public function finalizeAccountConnection(string $code, int $user_id)
    {
        $response = $this->curlService->post('https://connect.stripe.com/oauth/token', [
            'client_secret' => config('services.stripe.secret'),
            'code' => $code,
            'grant_type' => 'authorization_code'
        ]);

        // handle errors
        if (isset($response->error)) {
            return [
                'body' => [
                    'message' => $response->error . '. ' . $response->error_description
                ],
                'code' => 400
            ];
        }

        // save stripe account information
        $user = $this->userRepo->find($user_id);
        $user->stripe_account()->create([
            'stripe_user_id' => $response->stripe_user_id,
        ]);

        return [
            'body' => [
                'message' => "Stripe account saved."
            ],
            'code' => 200
        ];
    }
}
