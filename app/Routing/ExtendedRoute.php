<?php

namespace App\Routing;

use Illuminate\Support\Facades\Route;

/**
 * Class ExtraRoutes
 * This class create extended resource routes to keep clean code
 */
class ExtendedRoute
{
    static function apiResource(string $modelName, string $controller, array $params = [])
    {
        $modelSingle = substr($modelName, 0, -1);
        Route::apiResource($modelName, $controller, $params);
        Route::post("$modelName/{{$modelSingle}}/{relationship}/{relationship_id}", "$controller@attach")->name("{$modelName}.attach");
        Route::delete("$modelName/{{$modelSingle}}/{relationship}/{relationship_id}", "$controller@detach")->name("{$modelName}.detach");
    }
}
