<?php

namespace App;

use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'approved',
        'body',
        'commentable_type',
        'commentable_id',
        'user_id',
    ];

    public static $rules = [
        'body' => 'required',
        'user_id' => 'integer|required'
    ];

    /**
     * Relationships to be listed in documentation
     */
    public static $relationships = [
        'commentable',
        'user'
    ];

    /**
    * Relationships
    */

    public function commentable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Functions
     */
    public function faker()
    {
        $faker = Faker::create();
        $commentable_types = [
            'Campaign',
            'Post'
        ];
        $commentable_type = 'App\\' . $commentable_types[rand(0, count($commentable_types) - 1)];
        return [
            'approved' => rand(0,1),
            'body' => $faker->paragraph,
            'commentable_id' => rand(1, 10),
            'commentable_type' => $commentable_type,
            'user_id' => rand(1, 10),
        ];
    }
}
