<?php

use App\Subscription;

class SubscriptionsTableSeeder extends DatabaseSeeder
{

	public function run()
	{
		$subscriptions = [
			[
				'key' => "backers",
				'name' => "Backers"
			],
			[
				'key' => "campaigns",
				'name' => "Campaigns"
			],
			[
				'key' => "creators",
				'name' => "Creators"
			],
		];
		Subscription::insert($subscriptions);
	}
}
