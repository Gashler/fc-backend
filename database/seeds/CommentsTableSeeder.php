<?php

use Illuminate\Database\Seeder;
use App\Comment;

class CommentsTableSeeder extends Seeder
{
    public function run()
    {
        Comment::truncate();
        factory(Comment::class, 100)->create();
    }
}
