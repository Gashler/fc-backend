<?php

use App\Campaign;
use App\Image;
use App\Link;
use App\Subscription;
use App\User;

class CampaignsTableSeeder extends DatabaseSeeder
{

	public function run()
	{
		Schema::disableForeignKeyConstraints();

		// create campaigns
		Campaign::truncate();
		factory(Campaign::class, 10)->create();
		$campaigns = Campaign::all()->take(10);

		// associate images, links, and users
		$images = Image::all()->take(10);
		$videos = Link::all()->take(10);
		$users = User::all()->take(10);
		foreach ($campaigns as $index => $campaign) {

			// create subscription for campaign
			$campaign->subscription()->create(['key' => 'campaign']);

			// attach image and video
			$campaign->update([
				'image_id' => $images[$index]->id,
				'video_id' => $videos[$index]->id,
			]);

			// create links
			Link::insert([
				[
					'linkable_id' => $campaign->id,
					'linkable_type' => 'App\Campaign',
					'key' => 'facebook',
					'value' => 'https://facebook.com/' . str_random(10)
				],
				[
					'linkable_id' => $campaign->id,
					'linkable_type' => 'App\Campaign',
					'key' => 'youtube',
					'value' => 'https://youtube.com/?w=' . str_random(10)
				],
				[
					'linkable_id' => $campaign->id,
					'linkable_type' => 'App\Campaign',
					'key' => 'website',
					'value' => 'https://' . str_random(10) . '.com' . '/' . str_random(10)
				],
			]);

			// attach users
			foreach ($users as $user) {
				if (rand(0,1)) {
					$campaign->users()->save($user);
				}
			}
		}
	}
}
