<?php

class EmailTemplatesTableSeeder extends DatabaseSeeder
{
	public function run()
	{
		$array = [
			[
				'file_name' => 'default',
			]
		];
		\App\EmailTemplate::insert($array);
	}
}
