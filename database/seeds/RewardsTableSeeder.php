<?php

use Illuminate\Database\Seeder;
use App\Reward;

class RewardsTableSeeder extends Seeder
{
    public function run()
    {
        factory(Reward::class, 100)->create();
    }
}
