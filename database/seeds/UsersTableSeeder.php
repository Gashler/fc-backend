<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Role;
use App\User;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        User::truncate();
        $admin_role = Role::where('name', 'admin')->first();

        // create admin users
        User::insert([
            [
                'email' => 'admin@' . env('APP_DOMAIN'),
                'first_name' => 'Stephen',
                'last_name' => 'Gashler',
                'password' => \Hash::make(env('TESTING_PASSWORD')),
                'role_id' => $admin_role->id,
                'username' => str_random(20)
            ]
        ]);

        factory(User::class, 10)->create();
    }
}
