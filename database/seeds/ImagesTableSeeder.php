<?php

use App\Image;

class ImagesTableSeeder extends DatabaseSeeder
{

	public function run()
	{
		Image::truncate();

		$urls = [
			'https://ucarecdn.com/c4b92f27-b200-4eba-938b-8150942755b3/pexelsphoto941554.jpeg',
			'https://ucarecdn.com/9d7d1c6c-e07c-4d6d-8482-5581123abdf1/lightnightlensshadow.jpg',
			'https://ucarecdn.com/15f4d8ce-7537-4b20-b756-f6bcb8223f81/pexelsphoto257904.jpeg',
			'https://ucarecdn.com/2020b178-fb98-40d2-b628-b3101437ed52/pexelsphoto274959.jpeg',
			'https://ucarecdn.com/668db6b9-ea7f-4faa-83a9-b2068ad4ccca/machinemillindustrysteam633850.jpeg',
			'https://ucarecdn.com/b8772a32-abb5-435e-a04f-1ba2e8054176/pexelsphoto258244.jpeg',
			'https://ucarecdn.com/36fa0c52-454e-4347-82b5-96bb72dc64bc/pexelsphoto341523.jpeg',
			'https://ucarecdn.com/3b151a4d-84d5-49a6-b923-9cdef0ef5510/nightproductwatchdramatic84475.jpeg',
			'https://ucarecdn.com/8a59bd04-69c2-4882-b9ae-5016099fb775/pexelsphoto411592.jpeg',
			'https://ucarecdn.com/a4817e13-3f1f-4e65-96ea-41b209e7148a/pexelsphoto414026.jpeg',
		];

		foreach ($urls as $index => $url) {
			$images[$index] = Image::faker();
			$images[$index]['url'] = $url;
		}

		Image::insert($images);
	}
}
