<?php

use Illuminate\Database\Seeder;
use App\Payment;

class PaymentsTableSeeder extends Seeder
{
    public function run()
    {
        Payment::truncate();
        factory(Payment::class, 100)->create();
    }
}
