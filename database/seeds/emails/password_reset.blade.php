<subject>Reset Your Password</subject>
<body>
    <p>{{ $user['username'] }},</p>
    <p>
        A request was made to reset your password. If you did not make this request, then ignore this email. Otherwise, click on the following button to confirm your ownership of this email address and reset your password:
    </p>
    <p>
        <a class="btn btn-primary" href="{{ config('site.app_url') }}/password/confirm?email={{ $user['email'] }}&token={{ $data['token'] }}">
            Reset Your Password
        </a>
    </p>
</body>
