<subject>Thank you for supporting {{ $data['campaign']['title'] }}</subject>
<body>
    <p>
        For your records, following is the receipt for your contribution:
    </p>
    <table class="table table-striped">
        <tbody>
            <tr>
                <th>Contribution Amount:</th>
                <td>{{ curency($payment['amount']) }}</td>
            </tr>
            @if ($payment['reward'])
                <tr>
                    <th>Your Reward</th>
                    <td>
                        <strong>{{ $payment['reward']['title'] }}</strong>
                        <br>
                        <p>{{ $payment['reward']['description'] }}</p>
                    </td>
                </tr>
            @endif
            <tr>
                <th>Payment Method:</th>
                <td>{{ $payment['card_brand'] }} end with {{ $payment['card_last4'] }}</td>
            </tr>
            <tr>
                <th>Payment ID:</th>
                <td>{{ $payment['id'] }}</td>
            </tr>
        </tbody>
    </table>
    <p>Based on your communication preferences, either {{ $data['campaign']['user'] }} may periodically update you with the progress of this campaign or you can manually follow the progress by visiting <a href="{{ config('site.app_url') }}/c/{{ $data['campaign']['id'] }}"></a>.
</body>
