<subject>Product Accepted</subject>
<body>
    <p>{{ $user['username'] }},</p>
    <p>
        Good news, your product submission for "{{ $data['product']['name'] }}" has been accepted into the {{ config('site.app_name') }} marketplace! You can see it live at {{ config('site.shop_url') }}.
    </p>
    <p>
        You've helped connect local merchants with local consumers and are now a stock holder of this product. You'll receive {{ $data['advocate_commission_rate'] * 100 }}% of the sales. Automatic payouts will occur {{ $data['payout_schedule'] }} whenever your total share exceeds ${{ $data['minimum_payout_amount'] }}. Icrease your commissions and help your local economy by promoting this product.
    </p>
    <p>
        To get paid, you'll need to connect a Stripe account (if you haven't already). It's fast and easy. Click on the button below to get started:
    </p>
    <p>
        <a class="btn btn-primary" href="https://stripe.com">
            Add Your Payment Information
        </a>
    </p>
</body>
