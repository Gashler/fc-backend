<?php

use App\Subscription;
use App\Email;

class EmailsTableSeeder extends DatabaseSeeder
{
	public function run()
	{
		Schema::disableForeignKeyConstraints();

		// prepare data
		$query = Subscription::all();
		foreach ($query as $index => $emailSubscription) {
			$emailSubscriptions[$emailSubscription->key] = $emailSubscription;
		}
		foreach ($emailSubscriptions as $emailSubscription) {
			foreach ($emailSubscription->emails as $email) {
				$emailSubscription->emails()->detach($email->id);
			}
		}
		Email::truncate();

		// seed database from email files
		function scanEmailDirs($path, $parent = null, $grandparent = null, $emailSubscriptions) {
			$files = opendir($path);
			$emails = [];
			while ($file = readdir($files)) {
				if (is_dir($path . $file) && $file !== '.' && $file !== '..') {
					if (isset($parent)) {
						$grandparent = $parent;
					}
					scanEmailDirs($path . $file . "/", $file, $grandparent, $emailSubscriptions);
				} else {
					if ($file !== '.' && $file !== '..') {
						$content = file_get_contents($path . $file);
						$array = explode(".", $file);
						$key = $array[0];
						$interval = null;
						$interval_type = null;
						if ($grandparent == 'campaigns') {
							$array = explode('<interval-type>', $content);
							if (isset($array[1])) {
								$array = explode('</interval-type>', $array[1]);
								$interval_type = $array[0];
								$content = $array[1];
							}
							$array = explode('<interval>', $content);
							if (isset($array[1])) {
								$array = explode('</interval>', $array[1]);
								$interval = $array[0];
								$content = $array[1];
							}
						}
						$array = explode('<subject>', $content);
						$array = explode('</subject>', $array[1]);
						$subject = $array[0];
						$body = $array[1];
						$body = explode('<body>', $body);
						$body = explode('</body>', $body[1]);
						$body = $body[0];
						$email = Email::create([
							'key' => $key,
							'subject' => $subject,
							'body' => $body
						]);
						if (isset($interval)) {
							$email->subscriptions()->save($emailSubscriptions[$parent], [
								'interval_type' => $interval_type,
								'interval' => $interval
							]);
						}
					}
				}
			}
		}
		scanEmailDirs(database_path() . '/seeds/emails/', null, null, $emailSubscriptions);
		Schema::enableForeignKeyConstraints();
	}

}
