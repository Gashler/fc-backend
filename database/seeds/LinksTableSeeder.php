<?php

use App\Link;

class LinksTableSeeder extends DatabaseSeeder
{

	public function run()
	{
		Link::truncate();

		$urls = [
			'https://www.youtube.com/watch?v=khH5gNgtye0',
			'https://www.youtube.com/watch?v=3PpLISc44Rg',
			'https://www.youtube.com/watch?v=z1yuxJo8ksA',
			'https://www.youtube.com/watch?v=lDUax3llp0s',
			'https://www.youtube.com/watch?v=md_NX3gHL8M',
			'https://www.youtube.com/watch?v=9QBLxrzX3-g',
			'https://www.youtube.com/watch?v=yLxPmxjF_tQ',
			'https://www.youtube.com/watch?v=ftl4jyTy8nA',
			'https://www.youtube.com/watch?v=d5S7-PmXL3Y',
			'https://www.youtube.com/watch?v=jzoLB6VYmOs',
		];

		foreach ($urls as $index => $url) {
			$links[$index] = [
				'key' => 'youtube',
				'value' => $url
			];
		}

		Link::insert($links);
	}
}
