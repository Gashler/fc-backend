<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    public function run()
    {
        Role::truncate();
        Role::insert([
            ['name' => 'user'],
            ['name' => 'admin']
        ]);
    }
}
