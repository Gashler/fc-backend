<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        // seeders that must be run in order
        $this->call(SettingsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ImagesTableSeeder::class);
        $this->call(LinksTableSeeder::class);

        // other seeders
        $this->call(CampaignsTableSeeder::class);
        $this->call(CommentsTableSeeder::class);
        $this->call(SubscriptionsTableSeeder::class);
        $this->call(EmailTemplatesTableSeeder::class);
        $this->call(EmailsTableSeeder::class);
        $this->call(PaymentsTableSeeder::class);
        $this->call(RewardsTableSeeder::class);

        Schema::enableForeignKeyConstraints();
    }
}
