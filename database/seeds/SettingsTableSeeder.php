<?php

use App\Setting;
use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [
            [
                'key' => 'recommended_donation',
                'value' => 10
            ],
        ];
        Setting::insert($array);
    }
}
