<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailTemplatesTable extends Migration
{

	public function up()
	{
		Schema::create('email_templates', function(Blueprint $table) {
			$table->increments('id');
			$table->string('file_name');
			$table->text('css')->nullable();
			$table->text('html')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('email_templates');
	}

}
