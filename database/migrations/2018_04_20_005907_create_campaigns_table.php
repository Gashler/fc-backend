<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('days')->nullable();
            $table->integer('image_id')->nullable()->unsigned();
            $table->boolean('live')->default(0);
            $table->text('long_description');
            $table->integer('monetary_goal')->nullable();
            $table->integer('monetary_total')->default(0);
            $table->text('short_description');
            $table->string('title');
            $table->integer('user_id')->nullable()->unsigned();
            $table->integer('video_id')->nullable()->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
}
