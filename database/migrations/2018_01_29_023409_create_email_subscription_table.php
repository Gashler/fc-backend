<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailSubscriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::disableForeignKeyConstraints();
        Schema::create('email_subscription', function (Blueprint $table) {
            $table->integer('subscription_id')->unsigned();
            $table->integer('email_id')->unsigned();
            $table->primary(['subscription_id', 'email_id']);
            $table->integer('interval');
            $table->string('interval_type', 25)->nullable();
            // $table->foreign('subscription_id')->references('id')->on('subscriptions')->onDelete('cascade');
            // $table->foreign('email_id')->references('id')->on('emails')->onDelete('cascade');
        });
        // Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('email_subscription', function (Blueprint $table) {
            $table->dropPrimary();
        });
        Schema::drop('email_subscription');
    }
}
