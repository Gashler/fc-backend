<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_user', function (Blueprint $table)
        {
            $table->integer('email_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->primary(['email_id', 'user_id']);
            $table->integer('amount')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('email_user', function (Blueprint $table) {
            $table->dropPrimary();
        });
        Schema::drop('email_user');
    }
}
