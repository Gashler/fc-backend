<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::disableForeignKeyConstraints();
        Schema::create('subscription_user', function (Blueprint $table) {
            $table->integer('subscription_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->primary(['subscription_id', 'user_id']);
            // $table->foreign('subscription_id')->references('id')->on('subscriptions')->onDelete('cascade');
            // $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
        // Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscription_user', function (Blueprint $table) {
            $table->dropPrimary();
        });
        Schema::drop('subscription_user');
    }
}
