<?php

/**
* For site-wide variables
*/
return [
    'admin_user_id' => env('ADMIN_USER_ID'),
    'admin_url' => env('ADMIN_URL'),
    'app_env' => env('APP_ENV'),
    'app_name' => env('APP_NAME'),
    'app_url' => env('APP_URL'),
    'default_api_url' => env('DEFAULT_API_URL'),
    'frontend_url' => env('FRONTEND_URL'),
    'redirect_url' => env('REDIRECT_URL'),
    'shop_provider' => env('SHOP_PROVIDER', 'wc'),
    'shop_domain' => env('SHOP_DOMAIN'),
    'frontend_url' => env('FRONTEND_URL'),
    'testing_password' => env('TESTING_PASSWORD'),
    'testing_email' => env('TESTING_EMAIL'),
    'testing_market_user_id' => env('TESTING_MARKET_USER_ID'),
    'testing_first_name' => env('TESTING_FIRST_NAME'),
    'testing_last_name' => env('TESTING_LAST_NAME'),
    'wc-webhook-signature' => env('WOOCOMMERCE_WP_WEBHOOK_SIGNATURE')
];
