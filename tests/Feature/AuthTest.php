<?php

namespace Tests\Feature;

use App\User;
use League\OAuth2\Server\Grant\ClientCredentialsGrant;
use Tests\TestCase;

class AuthTest extends TestCase
{
    /**
     * Register
     *
     */
    public function testRegister()
    {

        $userData = (new User)->faker();
        $userData['password_c'] = $userData['password'];
        $response = $this->json(
            'POST',
            '/register',
            $userData
        );
        $response
            ->assertStatus(200)
            ->assertJsonStructure(['user']);
    }

    /**
     * Register
     *
     */
    public function testLogin()
    {
//        $response = $this->json(
//            'POST',
//            '/login',
//            [
//                'email' => config('site.testing_email'),
//                'password' => config('site.testing_password')
//            ]
//        );
//        $response
//            ->assertStatus(200);
    }

    /**
     * Login User by client
     * should receive auth token
     */
    public function testClientLogin()
    {
        $tokenData = $this->clientAuth();
        $this->json(
            'GET',
            'client/user-login/1'
        )->assertStatus(401);

        $this->json(
            'GET',
            'client/user-login/1',
            [],
            ['Authorization' => "Bearer $tokenData->access_token"]
        )->assertStatus(200);
    }

    /**
     * Login User by client
     * should receive auth token
     */
    public function testClientRegister()
    {
        $tokenData = $this->clientAuth();

        $existsUserParams = [
            'email' => config('site.testing_email'),
            'password' => config('site.testing_password'),
            'password_c' => config('site.testing_password'),
            'market_user_id' => config('site.testing_market_user_id'),
        ];

        $newUserParams = (new User)->faker();
        $newUserParams['password_c'] = $newUserParams['password'];
        $newUserParams['market_user_id'] = config('site.testing_market_user_id');

        $res = $this->json(
            'POST',
            'client/user-register',
            $existsUserParams,
            ['Authorization' => "Bearer $tokenData->access_token"]
        );

        $res->assertStatus(200)
            ->assertJsonStructure(['access_token', 'user']);

        $this->json(
            'POST',
            'client/user-register',
            $newUserParams,
            ['Authorization' => "Bearer $tokenData->access_token"]
        )->assertStatus(200)
            ->assertJsonStructure(['access_token', 'user']);
    }

    private function clientAuth()
    {
        $response = $this->post(
            'oauth/token',
            [
                'grant_type' => 'client_credentials',
                'client_id' => 1,
                'client_secret' => env('API_KEY'),
                'scope' => '*',
            ],
            [
                'Accept' => 'application/json'
            ]
        );
        return json_decode($response->getContent());
    }

}
