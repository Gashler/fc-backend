<?php

namespace Tests\Feature;

use App\User;
use Faker\Generator as Faker;

class UserTest extends BaseTest
{
    public $modelName = 'User';
    /**
    * Index
    */
    public function testIndex()
    {
        return $this->index();
    }

    /**
    * Store
    */
    public function testStore()
    {
        return $this->store();
    }

    /**
    * Show
    */
    public function testShow()
    {
        return $this->show();
    }

    /**
    * Update
    */
    public function testUpdate()
    {
        return $this->update();
    }

    /**
    * Delete
    */
    public function testDelete()
    {
        return $this->destroy();
    }

    /**
     * Attach Merchant
     */
    public function testAttachMerchant()
    {
        return $this->attach( 'Merchant');
    }

    /**
     * Detach Merchant
     */
    public function testDetachMerchant()
    {
        return $this->detach('Merchant');
    }
}
