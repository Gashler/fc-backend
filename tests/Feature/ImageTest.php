<?php

namespace Tests\Feature;

use App\Image;
use Faker\Generator as Faker;

class ImageTest extends BaseTest
{
    public $modelName = 'Image';
    /**
    * Index
    */
    public function testIndex()
    {
        return $this->index();
    }

    /**
    * Store
    */
    public function testStore()
    {
        return $this->store();
    }

    /**
    * Show
    */
    public function testShow()
    {
        return $this->show();
    }

    /**
    * Update
    */
    public function testUpdate()
    {
        return $this->update();
    }

    /**
    * Delete
    */
    public function testDelete()
    {
        return $this->destroy();
    }

    /**
     * Attach Product
     */
    public function testAttachProduct()
    {
        return $this->attach('Product');
    }

    /**
     * Detach Product
     */
    public function testDetachProduct()
    {
        return $this->detach('Product');
    }
}
