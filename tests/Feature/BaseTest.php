<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use App\Traits\GenerateUrls;

class BaseTest extends TestCase
{
    use GenerateUrls;

    protected static $object = null;
    protected static $token = null;
    protected $url = null;
    protected $version = '/v1/';
    protected $defaultFields = [
        'id',
        'created_at',
        'updated_at'
    ];

    public $modelName = null;

    /**
     * Setup
     */
    public function setUp()
    {
        parent::setUp();
        if (self::$token === null) {
            self::$token = $this->getToken();
        }
        $this->headers = [
            'Authorization' => 'Bearer ' . self::$token
        ];
        $this->url = $this->generateUrl($this->version, str_plural(strtolower($this->modelName)));
    }

    public function getToken()
    {
        $user = User::whereEmail(config('site.testing_email'))->first();
        return $user->createToken('TestToken')->accessToken;
    }

    public function testExample()
    {
        // leaving this empty function avoids a warning message
    }

    /**
     * Index
     *
     */
    public function index(array $params = [])
    {
        $response = $this->withHeaders($this->headers)->json('GET', $this->url, $params);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(['data' => [$this->defaultFields]]);
    }

    /**
     * Store
     *
     */
    public function store()
    {
        $modelPath = 'App\\' . $this->modelName;
        $object = new $modelPath;
        $params = $object->faker();
        $response = $this->withHeaders($this->headers)->json('POST', $this->url, $params);
        $content = json_decode($response->getContent());
        self::$object = $content->data;
        $response
            ->assertStatus(200)
            ->assertJsonStructure(['data' => $this->defaultFields]);
    }

    /**
     * Show
     *
     */
    public function show()
    {
        $this->withHeaders($this->headers)
            ->json('GET', $this->generateUrl($this->url, self::$object->id))
            ->assertStatus(200)
            ->assertJsonStructure($this->defaultFields);
    }

    /**
     * Update
     *
     */
    public function update()
    {
        $modelPath = 'App\\' . $this->modelName;
        $object = new $modelPath;
        $params = $object->faker();

        $this->withHeaders($this->headers)
            ->json('PUT', $this->generateUrl($this->url, self::$object->id), $params)
            ->assertStatus(200);
    }

    /**
     * Delete
     *
     */
    public function destroy()
    {
        $url = $this->version . str_plural(strtolower($this->modelName)) . '/' . self::$object->id;
        $response = $this->withHeaders($this->headers)->json('DELETE', $url);
        $response
            ->assertStatus(200);
    }

    /**
     * Attach
     */
    public function attach(string $childName)
    {
//        $parentObject = factory("App\\$this->modelName")->create();
//        $childObject = factory("App\\$childName")->create();
//        $url = $this->generateUrl($this->url, $parentObject->id, str_plural(strtolower($childName)), $childObject->id);
//
//        $response = $this->withHeaders($this->headers)->json('POST', $url);
//        $response
//            ->assertStatus(200);
    }

    /**
     * Detach
     */
    public function detach(string $childName)
    {
//        $parentObject = factory("App\\$this->modelName")->create();
//        $childObject = factory("App\\$childName")->create();
//        $relationship = str_plural(strtolower($childName));
//
//        $url = $this->generateUrl($this->url, $parentObject->id, $relationship, $childObject->id);
//
//        $parentObject->$relationship()->attach($childObject);
//
//        $response = $this->withHeaders($this->headers)->json('DELETE', $url);
//        $response
//            ->assertStatus(200);
    }
}
