<?php

namespace Tests\Feature;

use App\Console\Commands\GenerateDocsCommand;
use Faker\Generator as Faker;

class DocsTest extends BaseTest
{
    /**
     * Index
     *
     */
    public function testIndex()
    {
        \Artisan::call('si:generate_docs');
        $response = $this->withHeaders($this->headers)->json('GET', '/docs');
        $response
            ->assertStatus(200);
    }
}
