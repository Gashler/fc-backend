<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Traits\GenerateUrls;

class GenerateUrlsTraitTest extends TestCase
{
    use GenerateUrls;

    public function testAddingSlashes()
    {
        $this->assertEquals('/', $this->generateUrl());
        $this->assertEquals('1/2/3/', $this->generateUrl(1, 2, 3));
    }

    public function testWrongArgument()
    {
        $this->expectException(\Exception::class);
        $this->generateUrl(1);
        $this->generateUrl(1, []);
        $this->generateUrl('string', []);
        $this->generateUrl([], 'string');
    }

}
