-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: fc
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imageable_id` int(11) DEFAULT NULL,
  `imageable_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` VALUES (17,NULL,NULL,'2018/06/bells2.jpg',NULL,12,'App\\Campaign','2018-05-25 14:54:46','2018-05-25 14:54:46'),(20,NULL,NULL,'2018/06/marriedgames.png',NULL,17,'App\\Campaign','2018-06-06 20:43:44','2018-06-06 20:43:44'),(21,NULL,NULL,'2018/06/lovers-lane.jpg',NULL,17,'App\\Campaign','2018-06-06 20:46:11','2018-06-06 20:46:11'),(22,NULL,NULL,'2018/06/foreplay-generator.jpg',NULL,17,'App\\Campaign','2018-06-06 20:46:29','2018-06-06 20:46:29'),(23,NULL,NULL,'2018/06/romantic-ideas.jpg',NULL,17,'App\\Campaign','2018-06-06 20:47:30','2018-06-06 20:47:30'),(24,NULL,NULL,'2018/06/love-dice.jpg',NULL,17,'App\\Campaign','2018-06-06 20:48:45','2018-06-06 20:48:45'),(25,NULL,NULL,'2018/06/poker.png',NULL,17,'App\\Campaign','2018-06-06 20:49:07','2018-06-06 20:49:07'),(26,NULL,NULL,'2018/06/positions.jpg',NULL,17,'App\\Campaign','2018-06-06 20:49:55','2018-06-06 20:49:55'),(27,NULL,NULL,'2018/06/romantic-diary.jpg',NULL,17,'App\\Campaign','2018-06-06 20:51:27','2018-06-06 20:51:27'),(29,NULL,NULL,'2018/06/timer.jpg',NULL,17,'App\\Campaign','2018-06-06 20:52:34','2018-06-06 20:52:34'),(30,NULL,NULL,'2018/06/last-30-days.jpg',NULL,17,'App\\Campaign','2018-06-06 20:52:53','2018-06-06 20:52:53'),(32,NULL,NULL,'2018/06/school.png',NULL,18,'App\\Campaign','2018-06-07 15:39:41','2018-06-07 15:39:41'),(33,NULL,NULL,'2018/06/mom.jpg',NULL,19,'App\\Campaign','2018-06-07 21:43:01','2018-06-07 21:43:01'),(40,NULL,NULL,'2018/06/dog.jpg',NULL,20,'App\\Campaign','2018-06-09 04:48:38','2018-06-09 04:48:38'),(43,NULL,NULL,'2018/06/cards.jpg',NULL,24,'App\\Campaign','2018-06-13 22:01:49','2018-06-13 22:01:49'),(44,NULL,NULL,'2018/06/flowers.jpg',NULL,25,'App\\Campaign','2018-06-14 13:06:35','2018-06-14 13:06:35'),(45,NULL,NULL,'2018/06/animals.jpg',NULL,26,'App\\Campaign','2018-06-15 01:10:13','2018-06-15 01:10:13'),(47,NULL,NULL,'2018/06/manga.png',NULL,29,'App\\Campaign','2018-06-16 03:00:35','2018-06-16 03:00:35'),(48,NULL,NULL,'2018/06/veil.jpg',NULL,30,'App\\Campaign','2018-06-16 21:40:28','2018-06-16 21:40:28'),(50,NULL,NULL,'2018/06/bells.jpg',NULL,NULL,NULL,'2018-06-19 07:55:25','2018-06-19 07:55:25'),(51,NULL,NULL,'2018/06/windy.jpg',NULL,28,'App\\Campaign','2018-06-19 20:18:49','2018-06-19 20:47:27'),(52,NULL,NULL,'2018/06/girls.jpg',NULL,16,'App\\Campaign','2018-06-19 20:19:10','2018-06-19 20:47:44'),(53,NULL,NULL,'2018/06/lazurus.png',NULL,31,'App\\Campaign','2018-06-19 20:19:46','2018-06-19 20:46:22');
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-19  8:49:49
