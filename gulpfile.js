var elixir = require('laravel-elixir');

// sass
// elixir(function(mix) {
//     mix.sass('app.scss');
// });

// js
elixir(function(mix) {
    mix.scriptsIn('resources/assets/js');
    mix.scriptsIn('resources/assets/js/*/*.js');
    mix.scriptsIn('resources/assets/js/*/*/*.js');
});

elixir(function(mix) {
    mix.browserify('all.js');
});
